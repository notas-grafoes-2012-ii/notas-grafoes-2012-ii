\chapter{Menores, Árvores e Quase Boa Ordem}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%                                             %%
%%  \input{aulas/09-1-menores-e-qbo-relacoes}  %%
%%                                             %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%DONE: decidir se vamos manter o parágrafo de objetivo de capítulo
%\emph{Objetivo}: ``Provar'' (dar uma idéia da prova) o ``Minor
%Theorem'' [Robsertson \& Seymour]: ``Em todo conjunto infinito de
%grafos existem dois tais que um é menor do outro.''

\section{Relações}
Uma relação~\(R\) sobre um conjunto~\(X\) é um subconjunto
de~\(X\times X\). Para enfatizar que estamos tratando de relações e
não simplesmente de conjuntos, usamos a notação infixa~\(xRy\) para
denotar~\((x,y)\in R\).

Fixada uma relação~\(R\) sobre um conjunto~\(X\), dizemos que~\(R\) é
\begin{itemize}
\item \defi{Reflexiva}, se~\(xRx\) para todo~\(x\in X\);
\item \defi{Transitiva}, se~\(xRy\) e~\(yRz\) implicam~\(xRz\) para
  todos~\(x,y,z\in X\);
\item \defi{Antissimétrica}, se~\(xRy\) e~\(yRx\) implicam~\(x = y\),
  para todos~\(x,y\in X\);
\item \defi{Total}, se para todos~\(x,y\in X\), temos~\(xRy\)
  ou~\(yRx\);
\item \defi{Bem-fundada} (well-founded), se para todo subconjunto não-vazio~\(Y\)
  de~\(X\), existe um~\(y\in Y\) tal que nenhum~\(z\in Y\)
  satisfaz~\(zRy\) (\(Y\) tem um elemento minimal).
\end{itemize}

Em particular, uma relação é chamada de
\begin{itemize}
\item \defi{Quase-ordem}, se for reflexiva e transitiva;
\item \defi{Ordem parcial}, se for reflexiva, transitiva e
  antissimétrica;
\item \defi{Ordem total} (ou \defi{ordem}), se for reflexiva,
  transitiva, antissimétrica e total;
\item \defi{Quase-boa-ordem}, se for reflexiva, transitiva e
  bem-fundada;
\item \defi{Boa-ordem parcial}, se for reflexiva, transitiva,
  antissimétrica e bem-fundada;
\item \defi{Boa-ordem total} (ou \defi{boa-ordem}), se for reflexiva,
  transitiva, antissimétrica, total e bem-fundada.
\end{itemize}

Estamos particularmente interessados em estudar quase-boas-ordens e,
nesse contexto, é mais simples trabalhar com uma noção equivalente
(sob o axioma da escolha) de relações bem-fundadas:

\begin{proposicao}
Assumindo o axioma da escolha, uma relação~\(R\) sobre um
conjunto~\(X\) é bem-fundada se e somente se para toda sequência
infinita enumerável~\((x_i)_{i\in\NN}\), existem índices~\(i < j\)
tais que~\(x_i R x_j\).
\end{proposicao}

A partir deste ponto, assumiremos verdadeiro o axioma da escolha
implicitamente.

Dada uma relação~\(R\) sobre um conjunto~\(X\), um
subconjunto~\(A\subset X\) é dito uma~\defi{anticadeia} se não
existem~\(x,y\in A\) distintos tais que~\(xRy\).

Usaremos frequentemente o símbolo~\(\preceq\) no estudo de relações
(tipicamente quase-ordens) e assumiremos sempre que as
relações~\(\prec\), \(\succeq\) e~\(\succ\) estão definidas como:
\begin{itemize}
\item Para todos~\(x,y\), temos~\(x \prec y\) se e somente
  se~\(x\preceq y\) e~\(\neg(y\preceq x)\);
\item Para todos~\(x,y\), temos~\(x \succeq y\) se e somente
  se~\(y\preceq x\);
\item Para todos~\(x,y\), temos~\(x \succ y\) se e somente
  se~\(x\succeq y\) e~\(\neg(y\succeq x)\);
\end{itemize}

Supondo que~\(\preceq\) é uma quase-ordem, uma sequência~\((x_i)_{i\in
  U}\) indexada por~\(U\subset\NN\) é dita
\begin{itemize}
\item \defi{Crescente}, se para todos índices~\(i < j\), temos~\(x_i
  \preceq x_j\);
\item \defi{Decrescente}, se para todos índices~\(i < j\), temos~\(x_j
  \succeq x_i\);
\item \defi{Estritamente crescente}, se para todos índices~\(i < j\),
  temos~\(x_i \prec x_j\);
\item \defi{Estritamente decrescente}, se para todos índices~\(i <
  j\), temos~\(x_j \succ x_i\).
\end{itemize}



\begin{proposicao}\label{prop:9.1}
  Seja~\(\preceq\) uma quase-ordem sobre~\(X\). Então~\(\preceq\) é
  quase-boa-ordem se e somente se~\(X\) não contém nem uma anticadeia
  infinita nem uma sequência infinita estritamente decrescente~\(x_0
  \succ x_1 \succ \cdots\).
\end{proposicao}

\begin{prova}
  Observe que se~\(A\) é uma anticadeia infinita, então qualquer
  sequência~\((x_i)_{i\in\NN}\) em~\(A\) de elementos distintos é tal
  que não existem~\(i < j\) com~\(x_i \preceq x_j\).

  Por outro lado, se~\((x_i)_{i\in\NN}\) é uma sequência infinita
  estritamente decrescente, então também temos que não existem~\(i <
  j\) com~\(x_i \preceq x_j\).

  Portanto, se~\(\preceq\) é quase-boa-ordem, então~\(X\) não contém
  nem uma anticadeia infinita nem uma sequência infinita estritamente
  decrescente.

  \medskip

  Seja~\(x_0, x_1, \ldots, \) uma sequência qualquer de~\(X\) indexada
  pelos naturais. Considere o grafo completo (infinito)~\(K_{\NN}\)

  Faça uma coloração das arestas~\(ij\) de~\(K_{\NN}\), com~\(i < j\)
  com~\(3\) cores:
  \begin{itemize}
  \item Atribua a cor \emph{verde}, se~\(x_i \preceq x_j\);
  \item Atribua a cor \emph{amarela}, se~\(x_i\) e~\(x_j\) são
    incomparáveis;
  \item Atribua a cor \emph{vermelha}, se~\(x_i \succ x_j\).
  \end{itemize}
  Pelo Teorema de Ramsey, sabemos que~\(K_{\NN}\) tem um subgrafo
  completo~\(H\) infinito cujas arestas são todas da mesma cor. Pela
  hipótese da proposição, tais arestas não podem ser nem amarelas e
  nem vermelhas.

  Portanto, são verdes. Logo, quaisquer dois vértices~\(i,j\) (com~\(i
  < j\)) de~\(H\) são tais que~\(x_i \preceq x_j\) (bastava uma tal
  aresta).

  Concluímos que~\(\preceq\) é uma quase-boa-ordem.
\end{prova}
%DONE prova nas notas

\begin{corolario}\label{cor:9.2}
  Se~\(\preceq\) é quase-boa-ordem sobre $X$, então toda sequência infinita
  em~\(X\) tem uma subsequência infinita crescente.
\end{corolario}

Uma quase-ordem~\(\preceq\) sobre~\(X\) induz uma quase-ordem natural
sobre o conjunto dos subconjuntos finitos de~\(X\) (denotado
por~\([X]^{<\omega}\)). Essa quase-ordem é definida abaixo.

Para conjuntos finitos~\(A,B \subseteq X\), fazemos~\(A \preceq B\) se
existe uma função injetora~\(f\:A\rightarrow B\) tal que~\(a \preceq
f(a)\) para todo~\(a \in A\).

O seguinte lema e a ideia de sua prova têm um papel importante na
teoria da quase-boa-ordem.

%DONE escrever página 9.1,9.2

\begin{lema}\label{lem:9.3}
  Se~\(\preceq\) é quase-boa-ordem sobre~\(X\), então~\(\preceq\) é
  quase-boa-ordem sobre~\([X]^{<\omega}\).
\end{lema}

%DONE prova nas notas

\begin{prova}
  Suponha que~\(\preceq\) seja uma quase-boa-ordem sobre~\(X\) e
  que~\(\preceq\) não seja quase-boa-ordem sobre~\([X]^{<\omega}\).

  Chamaremos de sequências \emph{ruins} aquelas que violam a
  propriedade de serem bem-fundadas, isto é,
  sequências infinitas~\((x_i)_{i\in\NN}\) que são ou estritamente
  decrescentes ou tais que~\(\{x_i : i\in\NN\}\) é uma anticadeia.
  Naturalmente, chamaremos de sequências \emph{boas} aquelas que não
  são ruins.

  Vamos construir uma sequência ruim~\((A_n)_{n\in\NN}\) especial
  em~\([X]^{< \omega}\) recursivamente.

  Dado~\(n \in \NN\), suponha, indutivamente, que~\(A_i\) foi definido
  para todo natural~\(i < n\), e que exista uma sequência ruim
  em~\([X]^{<\omega}\) começando com~\(A_0, A_1, \ldots, A_{n-1}\)
  (para~\(n = 0\), temos que uma sequência ruim existe por~\(\preceq\)
  não ser quase-boa-ordem sobre~\([X]^{<\omega}\))

  Escolha então~\(A_n \in [X]^{< \omega}\) com~\(|A_n|\) mínimo e de
  forma que exista uma sequência ruim em~\([X]^{<\omega}\) começando
  com~\(A_0, A_1, \ldots, A_n\).

  Claramente, temos que ~\(A_n \neq \vazio\) para todo~\(n\in\NN\) (caso
  contrário teríamos~\(A_n \preceq A_{n+1}\)). Escolha então, para cada
  \(n \in \NN\), um elemento~\(a_n \in A_n\) e tome \(B_n =
  A_n\setminus\{a_n\}\).

  Pelo Corolário~\ref{cor:9.2}, a sequência \((a_n)_{n\in\NN}\) possui
  uma subsequência infinita crescente \((a_{n_i})_{i\in\NN}\).

  Considere a sequência de conjuntos
  \[
  (C_i)_{i\in\NN} = (A_0, A_1, \ldots A_{n_0 -1}, B_{n_0}, B_{n_1}, \ldots)
  \]
  Note que esta sequência é boa por construção (se a sequência fosse
  ruim, como~\(|B_{n_0}| < |A_{n_0}|\), teríamos uma contradição à escolha
  de~\(A_{n_0}\)).

  Portanto, existe um par de índices~\(i_0 < j_0\) tal que~\(C_{i_0} \preceq
  C_{j_0}\).

  Observe que não podemos ter~\(j_0 < n_0\), caso contrário,
  teríamos~\(A_{i_0} \preceq A_{j_0}\).

  Também não podemos ter~\(i_0 < n_0 \leq j_0\), caso contrário,
  teríamos~\(A_{i_0} \preceq B_{j_0} \preceq A_{j_0}\).

  Finalmente, se tivermos~\(n_0 \leq i_0\), então, como~\(a_{i_0}
  \preceq a_{j_0}\), temos que~\(B_{i_0}\preceq B_{j_0}\), o que
  implica que \(A_{i_0}\preceq A_{j_0}\) (basta estender a função
  de~\(B_{i_0}\) para~\(B_{j_0}\) fazendo a imagem de~\(a_{i_0}\)
  ser~\(a_{j_0}\)). Isso é uma contradição.

  Portanto~\(\preceq\) é quase-boa-ordem sobre~\([X]^{<\omega}\).
\end{prova}

% pagina 9.3

Seja~\(\mathcal{G}\) a classe de todos os grafos \emph{finitos} a
menos de isomorfismo e~\(\preceq\) a relação de `menor (de)' definida
sobre~\(\mathcal{G}\). É fácil ver que~\(\preceq\) é quase-ordem
sobre~\(\mathcal{G}\) (é inclusive uma ordem parcial).

O `Minor Theorem' afirma que
\begin{center}
	``A relação~\(\preceq\) é quase-boa-ordem sobre~\(\mathcal{G}\).''
\end{center}

Como obviamente uma sequência estritamente decrescente de menores não
pode ser infinita, a Proposição~\ref{prop:9.1} nos dá que o `Minor
Theorem' é equivalente a sequinte afirmação:
%DONE verificar se a referência acima está correta, foi corrigida de prop:91

\begin{center}
  ``Não existe uma anticadeia infinita em~\(\mathcal{G}\).''
\end{center}
Isto é, não existe um conjunto infinito de grafos em~\(\mathcal{G}\),
dois-a-dois incomparáveis quanto à relação menor~\(\preceq\).

Restringindo-nos à classe das árvores, existe uma versão mais forte do
`Minor Theorem':

\begin{teorema}\label{teo:minorarv}
  Seja~\(\mathcal{T}\) a classe das árvores \emph{finitas} a menos de
  isomorfismo e~\(\preceq\) a relação de menor topológico (\(\preceq
  \equiv \preceq_t\)). Então~\(\preceq\) é quase-boa-ordem
  sobre~\(\mathcal{T}\).
\end{teorema}

A prova será baseada numa relação mais forte definida sobre árvores
enraizadas.

Se~\(T\) é uma árvore e~\(r\) um vértice qualquer fixo, chamaremos o
par~\((T,r)\) de \emph{árvore enraizada}, e~\(r\) sua \emph{raiz}
(algumas vezes escrevemos simplesmente~\(T\), em vez de~\((T,r)\).

Dadas árvores enraizadas~\((T,r)\) e~\((T',r)\), escrevemos
\[
(T,r) \preceq (T',r')
\]
se existe um isomorfismo~\(\varphi\) entre alguma subdivisão de~\(T\)
e uma subárvore~\(T''\) de~\(T'\) tal que o caminho de~\(r'\)
a~\(\varphi(r)\) em~\(T'\) não contém nenhum outro vértice de~\(T''\)
a não ser~\(\varphi(r)\). Dizemos que um tal isomorfismo
\emph{respeita}~\(r'\).

Seja~\(\mathcal{T}^*\) o conjunto das árvores enraizadas finitas.

Provaremos então o teorema abaixo, do qual o
Teorema~\ref{teo:minorarv} segue como corolário.

%DONE: o teorema abaixo parece ser de Kruskal (verificar)
\begin{teorema}[Kruskal, 1960~\cite{Kruskal60}]\label{teo:minorarvenr}
  A relação~\(\preceq\) é quase-boa-ordem sobre~\(\mathcal{T}^*\).
\end{teorema}

\begin{prova}
  Novamente adotaremos a terminologia de sequências \emph{ruins} para
  as que violam a propriedade de boa-fundação das relações e de
  sequências \emph{boas} para as que não são ruins.

  Suponha que~\(\preceq\) não seja quase-boa-ordem
  sobre~\(\mathcal{T}^*\) e vamos construir uma sequência
  ruim~\((T_n,r_n)_{n\in\NN}\) especial em~\(\mathcal{T}^*\)
  recursivamente.

  Dado~\(n \in \NN\), suponha indutivamente que~\((T_i,r_i)\) já foi
  construído para todo~\(i < n\), e que exista uma sequência ruim
  começando com~\((T_0,r_0), (T_1,r_1), \ldots, (T_{n-1},r_{n-1})\).

  Escolha então~\((T_n,r_n)\) com~\(|V(T_n)|\) mínimo e de forma que
  exista uma sequência ruim começando com~\((T_0,r_0), (T_1,r_1),
  \ldots, (T_n,r_n)\) (para~\(n = 0\), temos que uma sequência ruim
  existe por~\(\preceq\) não ser quase-boa-ordem).

  Observe que~\(|V(T_n)| > 1\) para todo \(n\in\NN\)).

  Para cada~\(n\in\NN\), seja~\(A_n\) o conjunto das árvores
  enraizadas~\((T,r)\) tal que~\(T\) é um componente conexo
  de~\(T_n-r_n\) e~\(r\) é adjacente a~\(r_n\) em~\(T_n\).

  Seja~\(A = \bigcup_{n \in \NN} A_n\).

  Vamos provar que~\(\preceq\) é quase-boa-ordem sobre~\(A\).

  Seja~\(((U_k,s_k))_{k\in\NN}\) uma sequência qualquer de árvores
  enraizadas do conjunto~\(A\).

  Para cada~\(k\in \NN\) escolha~\(n = n(k)\) tal que \(U_k \in A_n\)
  e seja~\(k^* = \argmin\{n(k) : k\in \NN\}\) (\(n(k^*) \leq n(k)\)
  para todo~\(k \in \NN\)).

  Então a sequência
  \[
  (W_n,t_n)_{n\in\NN} = ((T_0,r_0),\ldots,
  (T_{n(k^*)-1},r_{n(k^*)-1}), (U_{k^*},s_{k^*}),
  (U_{k^*+1},s_{k^*+1}), \ldots)
  \]
  é boa, pois \(U_{k^*} \subsetneq T_{n(k^*)}\) (caso
  contrário, teríamos uma contradição com a minimalidade
  de~\(|V(T_{n(k^*)})|\)).

  Ou seja, existem índices~\(i < j\) tais que~\((W_i,t_i) \preceq
  (W_j,t_j)\).

  Observe que não podemos ter~\(j \leq n(k^*) -1\), caso contrário,
  teríamos~\((T_i,r_i)\preceq (T_j,r_j)\).

  Também não podemos ter~\(i < n(k^*) \leq j\), caso contrário,
  teríamos~\((T_i,r_i)\preceq (U_j,s_j)\preceq (T_{n(j)},r_{n(j)})\)
  (pois~\(U_j\in A_{n(j)}\)).

  Logo, temos~\(n(k^*) \leq i\), ou seja,
  temos~\((U_i,s_i)\preceq (U_j,s_j)\), o que significa que a
  sequência~\((U_n,s_n)_{n\in\NN}\) é boa.

  Portanto~\(\preceq\) é quase-boa-ordem sobre~\(A\).

  Pelo Lema~\ref{lem:9.3}, temos que~\(\preceq\) é quase-boa-ordem
  sobre~\([A]^{<\omega}\).

  Em particular, isso significa que existem índices~\(i_0 < j_0\) tais
  que~\(A_{i_0} \preceq A_{j_0}\).

  A partir da função~\(f\: A_{i_0}\to A_{j_0}\), das subdivisões e
  isomorfismos que respeitam raiz que testemunham~\(A_{i_0} \preceq
  A_{j_0}\), segue que~\((T_{i_0},r_{i_0})\preceq (T_{j_0},r_{j_0})\).

  Portanto~\(\preceq\) é quase-boa-ordem sobre~\(\mathcal{T}^*\).
\end{prova}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%                                                           %%
%%  \input{aulas/09-2-menores-e-qbo-minor-thm-larg-arb-lim}  %%
%%                                                           %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% página 9.4

\section{O `Minor Theorem' para grafos com largura arbórea limitada}

Grafos com largura arbórea (\(\la\)) limitada são suficientemente
`semelhantes' a árvores tornando possível adaptar a prova do Teorema
de Kruskal (Teorema~\ref{teo:minorarvenr}) para a classe desses
grafos. Grosseiramente, a ideia é iterar o argumento da `sequência
minimal ruim'~\(\la(G)\) vezes. A prova desse resultado encontra-se no
artigo ``graph minors I'', de Robertson~\&~Seymour, 1990.

\begin{teorema}\label{teo:9.5}[Robertson~\&~Seymour, 1990]
  Para todo natural~\(k\), os grafos com largura arbórea menor
  que~\(k\) são quase-bem-ordenados pela relação de \emph{menor}.
\end{teorema}

Este teorema nos leva um passo adiante em relação à prova do `Minor
Theorem' (para grafos arbitrários). Para chegar a esse resultado
geral, a seguinte questão é de interesse.

\emph{Pergunta}: Como são os grafos com \emph{largura arbórea
  limitada}?

Mais precisamente, há algum fenômeno estrutural que ocorre num grafo
se e somente se ele tem~\(\la\) grande?

\subsection{Obstruções canônicas à largura arbórea pequena}

Dizemos que~\(X \subseteq V(G)\) é um \emph{subconjunto conexo}
se~\(G[X]\) é conexo.

Seja~\(\mathcal{C}\) um conjunto de subconjuntos conexos de~\(V(G)\).

Dizemos que um conjunto~\(U \subseteq V(G)\) cobre~\(\mathcal{C}\)
se~\(U\) intersecta cada conjunto em~\(\mathcal{C}\).

Dizemos que dois elementos de~\(\mathcal{C}\) se \emph{tocam}
(`touch') se eles se intersectam ou \(G\) contém uma aresta entre
eles.

\begin{exercicio}
  Se~\(\mathcal{C}\) é um conjunto de subconjuntos conexos de~\(V(G)\)
  e quaisquer dois elementos de~\(\mathcal{C}\) se tocam, então em
  toda decomposição arbórea~\((T,\{W_t : t\in V(T)\})\) de~\(G\)
  existe~\(t\in V(T)\) tal que~\(W_t\)
  cobre~\(\mathcal{C}\). [Sugestão: imitar a prova da
    Proposição~\ref{prop:8.4}]
\end{exercicio}

Esse resultado prova o lado fácil (\(\Leftarrow\)) da seguinte
caracterização de grafos com largura arbórea maior ou igual a~\(k\).

\begin{teorema}\label{teo:9.6}[Seymour~\&~Thomas, 1993]
  Seja~\(k\) um inteiro. Um grafo~\(G\) tem~\(\la(G) \geq k\) se e
  somente se~\(G\) contém uma coleção de conjuntos de vértices conexos
  que se tocam dois a dois e que não podem ser cobertos por até~\(k\)
  vértices.
\end{teorema}

Outro teorema interessante faz uso de uma generalização de árvores:

Dizemos que um grafo é uma~\emph{\(k\)-árvore} se é um grafo cordal e
tal que todas suas cliques maximais tem cardinalidade~\(k+1\) e todos
os minimal clique separators tem cardinalidade~$k$. %%%%%%%%%%%%%%%%%%%%

Dizemos que um grafo é uma~\emph{\(k\)-árvore-parcial} se é um
subgrafo de uma~\(k\)-árvore.

\begin{teorema}
Se~\(G\) é uma~\(k\)-árvore-parcial, então~\(\la(G) \leq k\).
\end{teorema}


%página 9.6

\emph{Interesse}: Decomposição arbórea que dê mais informações sobre a
estrutura arbórea do grafo (quão semelhante de uma árvore é o
grafo). Exigir apenas largura arbórea pequena não captura como bolsas
da decomposição se relacionam ao longo da árvore (da decomposição).

%DONE - desenho e diagrama da página 9.6
%nota: o desenho é inútil


Dizemos que uma decomposição arbórea~\((T, \{W_t : t\in V(T)\})\)
de~\(G\) é \emph{interligada} (`linked') se, para todo~\(s\in\NN\) e
todos~\(t_1,t_2\in V(T)\), existem~\(s\) caminhos disjuntos
de~\(W_{t_1}\) a~\(W_{t_2}\).

Dizemos que uma decomposição arbórea~\((T, \{W_t : t\in V(T)\})\)
de~\(G\) é \emph{enxuta} (`lean') se, para todo~\(s \in \mathbb{N}\) e
e todos~\(t_1,t_2 \in V(T)\), existe~\(t\) no caminho de~\(t_1\)
para~\(t_2\) em~\(T\) tal que~\(|W_t| < s\).


%DONE - fazer desenhos página 9.6
%nota: o desenho é inútil

O teorema abaixo afirma que é possível encontrar decomposições
arbóreas com largura mínima e que são também enxutas:

\begin{teorema}\label{teo:9.7}[Thomas,1990]
  Todo grafo~\(G\) possui uma decomposição arbórea enxuta com
  largura~\(\la(G)\).
\end{teorema}

% página 9.7

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%                                                      %%
%%  \input{aulas/09-3-menores-e-qbo-dec-arb-men-proib}  %%
%%                                                      %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Decomposições arbóreas e menores proibidos}

Seja~\(\mathcal{X}\) uma classe de grafos. Considere a classe dos
grafos sem um menor em \(\mathcal{X}\)
\[
\Forb_\preceq (\mathcal{X})
\quad = \quad
\{ G \text{ grafo } : G \not\succeq X, \text{ para todo } X \in \mathcal{X}\}.
\]

É fácil ver que~\(\Forb_\preceq(\mathcal{X})\) é uma \emph{propriedade
  de grafos}, isto é, é fechada sob isomorfismo
(\(H\in\Forb_\preceq(\mathcal{X})\) e~\(H \cong H'\) implicam~\(H' \in
\Forb_\preceq(\mathcal{X})\)).

Quando se tratar apenas de um grafo, escreveremos~\(\Forb_\preceq(G)\)
em vez de~\(\Forb_\preceq(\{G\})\).

A classe~\(\Forb_\preceq(\mathcal{X})\) é expressa especificando-se os
grafos~\(X \in \mathcal{X}\) como \emph{menores proibidos}
(`forbidden' ou `excluded minors').

\begin{exemplo}
  Se~\(\mathcal{X} = \{K_5,K_{3,3}\}\),
  então~\(\Forb_\preceq(\mathcal{X})\) é a classe dos grafos planares.

  Se~\(\mathcal{X} = \{K_4\}\), então~\(\Forb_\preceq (\mathcal{X})\)
  é a classe dos grafos série-paralelos.

  Se~\(\mathcal{X} = \{K_{2,3},K_4\}\),
  então~\(\Forb_\preceq(\mathcal{X})\) é a classe dos grafos
  exoplanares.
\end{exemplo}

Certamente~\(\Forb_\preceq(\mathcal{X})\) é fechada sob a operação de
tomar menores, isto é, se~\(G\in \Forb_\preceq (\mathcal{X})\) e~\(H
\preceq G\), então~\(H \in \Forb_\preceq (\mathcal{X})\).

% página 9.8

Propriedades de grafos que são fechadas sob tomada de menores são
chamadas~\emph{minor-hereditary}.

A proposição abaixo afirma que as propriedades hereditárias coincidem
exatamente com as classes de menores proibidos.

\begin{proposicao}\label{prop:9.8}
  Seja \(\mathcal{P}\) uma propriedade de grafos. A
  propriedade~\(\mathcal{P}\) pode ser expressa através de menores
  proibidos se e somente se~\(\mathcal{P}\) é minor-hereditary.
\end{proposicao}

\begin{prova}
Já vimos que~\(\Forb_\preceq(\mathcal{X})\) é uma propriedade hereditária
para toda~\(\mathcal{X}\).

\medskip
Por outro lado, sabemos que \(\mathcal{P} =
\Forb_\preceq(\overline{\mathcal{P}})\), onde
\(\overline{\mathcal{P}}\) é o complemento de \(\mathcal{P}\).
\end{prova}

\emph{Pergunta}: Como representar melhor uma propriedade hereditária
através de menores proibidos?

Resposta mais adiante.

Por ora, considere a propriedade hereditária: largura arbórea limitada
(`bounded tree width').

\begin{proposicao}\label{prop:9.9}
  Para todo~\(k > 0\), a propriedade de ter~\(\la < k\) é hereditária.
\end{proposicao}

\begin{prova}
  É suficiente provar que a largura arbórea de um grafo não aumenta
  quando contraímos uma aresta. O resultado segue da
  Proposição~\ref{prop:8.0} e da propriedade de menores.
\end{prova}

% página 9.9

Pelas proposições anteriores, a propriedade ``ter largura arbórea
menor que~\(k\)'' pode ser expressa por menores proibidos.

Seja~\(\mathcal{G}_{<k} =\) classe dos grafos com largura arbórea
menor que~\(k\).

Para alguns valores fixos de~\(k\), já conhecemos essa classe:

\begin{itemize}
\item A classe~\(\mathcal{G}_{< 2}\) é a classe das florestas (e
  coincide com~\(\Forb_\preceq (K_3)\));
\item A classe~\(\mathcal{G}_{< 3}\) é a classe dos grafos
  série-paralelos (e coincide com~\(\Forb_\preceq (K_4)\)).
\end{itemize}

É natural perguntar se há outros grafos~\(H\) para os quais os grafos
em~\(\Forb_\preceq(H)\) têm largura arbórea limitada e como são tais
grafos.

\begin{proposicao}
Se~\(H\) é um grafo tal que os grafos de~\(\Forb_\preceq(H)\) possuem
largura arbórea limitada, então~\(H\) é planar.
\end{proposicao}

\begin{prova}
Sabemos que o grafo~\(G_{n\times n}\) da grade~\(n \times n\) possui
largura arbórea~\(\la(G_{n\times n}) = n\), ou seja, existem grafos
planares com largura arbórea arbitrariamente grande.

Como todo menor de um grafo planar é planar, se~\(H\) não for planar,
então~\(G_{n\times n}\) não o terá como menor.
\end{prova}

% página 9.10

\begin{teorema}\label{teo:9.A}[Robertson,~Seymour~\&~Thomas, 94]
  Para todo grafo planar~\(H\) existe um inteiro~\(n\) tal
  que~\(H\preceq G_{n \times n}\).
\end{teorema}

\begin{teorema}\label{teo:9.B}
  Para todo grafo planar~\(H\) existe um inteiro~\(k\) tal
  que se um grafo~\(G\) tem largura arbórea maior ou igual a~\(k\),
  então~\(H\preceq G\).
\end{teorema}

\begin{corolario}\label{cor:9.C}
  Se~\(H\) é um grafo planar, então os grafos de~\(\Forb_\preceq(H)\)
  têm largura arbórea limitada.
\end{corolario}

\begin{corolario}\label{teo:9.10}[Robertson~\&~Seymour]
  Grafos em~\(\Forb_\preceq (H)\) têm largura arbórea limitada se e
  somente se~\(H\) é planar.
\end{corolario}

%DONE: colocar definição de pw no C8
Existe um resultado análogo: troca-se~\(\la\) por~\(\pw\) e planar por
floresta.

\begin{teorema}\label{teo:9.11}[Robertson~\&~Seymour,83]
  Grafos em~\(\Forb_\preceq(H)\) têm~\(\pw\) limitada se e somente
  se~\(H\) é uma floresta.
\end{teorema}

\begin{prova}
  Se~\(H\) não é uma floresta, isto é, se~\(H\) tem um circuito,
  então~\(\Forb_\preceq(H)\) contém todas as árvores.

  Vamos provar então que árvores podem ter~\(\pw\) arbitrariamente
  grande.

  Seja~\(G\) um grafo conexo e~\((P,\{W_t : t\in V(P)\})\) uma
  decomposição em caminho de~\(G\) com largura~\(\pw(G)\). Suponha que
  os vértices de~\(P\) são~\(1, 2,\ldots, k\) e~\(E(P) = \{ij :
  i-j=1\}\).

  Tome~\(v_1 \in W_1\) e~\(v_k \in W_k\) e seja~\(Q\) um caminho
  de~\(v_1\) a~\(v_k\) em~\(G\).

  Então a decomposição em caminho~\((P,\{W_t \setminus V(Q) : t\in
  V(P))\}\) de~\(G-Q\) possui largura menor ou igual a \(\pw(G)-1\), pois~\(Q\) deve
  possuir pelo menos um vértice em cada bolsa (segue da
  Proposição~\ref{prop:8.3}).

  Portanto todo grafo conexo~\(G\) contém um caminho~\(Q\) tal
  que~\(\pw(G-Q) \leq \pw(G) - 1\). Assim, se pudermos assumir (por alguma
  hipótese de indução apropriada) que~\(\pw(G-Q)\) é grande para todo
  caminho~\(Q \subseteq G\), então~\(\pw(G)\) também será grande.

  Usando esse fato, vamos provar que árvores podem ter~\(\pw\)
  arbitrariamente grande.

  Para todo~\(k \geq 1\), seja \(T^k_3 \equiv\) a árvore ternária com
  raiz~\(r\) e todas suas folhas à distância~\(k\) de~\(r\) (a raiz
  possui grau~\(3\), as folhas possuem grau~\(1\) e todos os demais
  vértices possuem grau~\(4\)).

  %DONE	-	desenho das árvores: T^2_3 é desenhada como caso base e T^{k+1}_3 é construído
  %			colocando um novo vértice e colando este vértice com 3 cópias de T^k_3.

  \begin{figure}[ht]
  \begin{center}
  \PoeFigura{arvoresternarias}
  \caption{Árvores ternárias~\(T^0_3\), \(T^1_3\) e~\(T^2_3\).}
  \end{center}
  \end{figure}

  Observe que, se~\(Q\) é um caminho em~\(T^{k+1}_3\), então~\(Q\)
  contém no máximo duas das arestas incidentes a~\(r\). Logo
  temos~\(T^{k+1}_3 -Q \supseteq T^k_3\), donde segue
  que~\(\pw(T^{k+1}_3) \geq \pw(T^k_3) + 1\) para todo~\(k \geq 1\).

  Como~\(\pw(T^2_3) \geq 2\), segue por indução que~\(\pw(T^k_3) \geq
  k\) para todo~\(k\geq 2\).

  Portanto existem árvores com~\(\pw\) arbitrariamente grande,
  logo grafos de~\(\Forb_\preceq(H)\) têm~\(\pw\) limitada implica
  que~\(H\) é uma floresta.

%TODO: prova da volta do Teorema~\ref{teo:9.11}
\end{prova}


%\begin{teorema}\label{teo:12.4.5}[Roberton \& Seymour, 1983]
%  The path-width of the graphs in \(\Forb_\preceq (H)\) is bounded if
%  and only if \(H\) is a forest.
%\end{teorema}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%                                                  %%
%%  \input{aulas/09-4-menores-e-qbo-minor-theorem}  %%
%%                                                  %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% página 9.11

\section{O `Minor Theorem'}

Teoremas que caracterizam uma propriedade hereditária~\(\mathcal{P}\)
através de um conjunto~\(\mathcal{X}\) de menores proibidos estão
entre os resultados mais atraentes na teoria dos grafos.

Tais teoremas dão um sabor de~\(\operatorname{coNP}\) à
propriedade~\(\mathcal{P}\), pois para certificar que um grafo~\(G\)
não satisfaz~\(\mathcal{P}\), basta apresentar o menor proibido
que~\(G\) possui.

% página 9.12

Claramente, podemos caracterizar~\(\mathcal{P}\)
como~\(\Forb_\preceq(\overline{\mathcal{P}})\), mas o interessante é
caracterizar~\(\mathcal{P}\) como~\(\Forb_\preceq(\mathcal{X})\)
com~\(\mathcal{X}\) menor possível.


\begin{proposicao}
  Uma classe~\(\mathcal{X} \subseteq \overline{\mathcal{P}}\) é minimal
  à propriedade~\(\mathcal{P} = \Forb_\preceq(\mathcal{X})\) se e
  somente se~\(\mathcal{X}\) contém exatamente uma cópia de cada grafo
  \(\preceq\)-minimal em~\(\overline{\mathcal{P}}\).
\end{proposicao}

\begin{prova}
  Claramente, se~\(\mathcal{X}\) possuir um grafo~\(H\) que não
  é~\(\preceq\)-minimal em~\(\overline{\mathcal{P}}\) porque
  digamos~\(H\succeq K \in \overline{\mathcal{P}}\) e~\(H\neq K\),
  então teremos~\(\Forb_\preceq(\mathcal{X}) =
  \Forb_\preceq(\mathcal{X}\setminus \{H\}\), pois todos os grafos que
  possuem~\(H\) como menor também possuem~\(K\) como menor.

  Por outro lado, se um grafo~\(\preceq\)-minimal~\(H\)
  de~\(\overline{\mathcal{P}}\) não estiver em~\(\mathcal{X}\), então
  teremos~\(H \in \Forb_\preceq(\mathcal{X})\).

  Logo~\(\mathcal{X}\) é minimal implica que~\(\mathcal{X}\) contém
  exatamente os grafos que são~\(\preceq\)-minimais
  em~\(\overline{\mathcal{P}}\).

  \medskip

  Por outro lado, se~\(\mathcal{X}\) contém exatamente os grafos que
  são~\(\preceq\)-minimais em~\(\overline{\mathcal{P}}\), então todo
  grafo~\(H\in\Forb_\preceq(\mathcal{X})\) está em~\(\mathcal{P}\)
  (caso contrário~\(H\) possuiria um menor~\(\preceq\)-minimal
  de~\(\overline{\mathcal{P}}\)).

  A outra inclusão segue da hereditariedade de~\(\mathcal{P}\).
\end{prova}

A proposição acima implica que existe um único conjunto minimal de
menores proibidos (a menos de isomorfismo) para uma propriedade
hereditária.

Ademais, os elementos desse conjunto são incomparáveis
sob~\(\preceq\).  O `Minor Theorem' (enunciado a seguir) implica que
qualquer conjunto de grafos~\(\preceq\)-incomparáveis é finito.

\begin{teorema}\label{teo:9.13}[Minor Theorem; Robertson \& Seymour, 1986-97]
  Os grafos finitos são quase-bem-ordenados pela relação de menor
  \(\preceq\).
\end{teorema}

\begin{corolario}\label{cor:9.14}
  Toda propriedade de grafos que é fechada sob tomada de menores pode
  ser expressa como~\(\Forb_\preceq(\mathcal{X})\) com~\(\mathcal{X}\)
  finito.
\end{corolario}

Como a propriedade de ``ser imersível numa superfície'' é fechada sob
tomada de menores, o seguinte corolario segue.

\begin{corolario}\label{cor:9.15}
  Para toda superfície~\(S\) existe um conjunto finito de
  grafos~\(\mathcal{X}\) tal que~\(\Forb_\preceq(\mathcal{X})\) contém
  precisamente os grafos não-imersíveis em~\(S\).
\end{corolario}

A título de curiosidade, a propriedade de imersibilidade no plano
projetivo equivale a um total~\(35\) menores proibidos.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%                                          %%
%%  \input{aulas/09-5-menores-e-qbo-lista}  %%
%%                                          %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Exercícios}

\begin{exercicio}% exercício 29
Sejam~\(\preceq_1\) e~\(\preceq_2\) quase-boas-ordens sobre~\(Q_1\)
e~\(Q_2\) respectivamente. Seja também~\(Q=Q_1\times Q_2\) e
defina a relação~\(\preceq\) sobre~\(Q\) da seguinte forma:
\[
(q_1,q_2)\preceq (q_1',q_2')\quad \text{se}
\quad q_1\preceq_1 q_1'\quad \text{e}
\quad q_2\preceq_2 q_2'.
\]
Prove que \(\preceq\) é uma quase-boa-ordem sobre~\(Q\).
\end{exercicio}


\begin{exercicio}% exercício 30
Mostre que não vale um resultado análogo ao Lema~\ref{lem:9.3}
para subconjuntos infinitos de~\(X\), denotado por \(X^\omega\).

Para isso, seja~\(Q\) o conjunto de todos os pares~\((i,j)\) de
inteiros~\(1\leq i<j\) e considere a relação~\(\preceq\) sobre~\(Q\)
definida por
\[
(i,j)\preceq (i',j')\quad\text{se}\quad
(i=i'\quad\text{e}\quad j\leq j')\quad
\text{ou}\quad (j<i'),
\]
e defina~\(Q^\omega = \{A\subset Q : A \text{ é infinito}\}\) e a
relação~\(\preceq_\omega\) sobre~\(Q^\omega\) definida por
\begin{center}
\begin{tabular}{rp{7cm}}
\(A \preceq_\omega B\), & se existe uma função injetora~\(f\:A\to
B\) tal que~\(a\preceq f(a)\) para todo~\(a\in A\).
\end{tabular}
\end{center}

Prove que~\(\preceq\) é quase-boa-ordem sobre~\(Q\)
mas~\(\preceq_\omega\) não é quase-boa-ordem sobre~\(Q^\omega\).
\end{exercicio}


\begin{exercicio}% exercício 31
Prove que a relação de subgrafo não é uma quase-boa-ordem sobre o
conjunto das árvores finitas. (Veja Ex 7 do Cap. 12 do Diestel.)
\end{exercicio}


\begin{exercicio}% exercício 32
Prove que a relação de menor topológico não é uma quase-boa-ordem
sobre o conjunto dos grafos finitos. (Veja sugestão no Ex do Cap 12 do
Diestel.)
\end{exercicio}



%\printpagenotes* % notas de fim de capítulo
\nocite{*} % remover depois, é só para facilitar a compilação
\bibliographystyle{alpha}
\bibliography{bibliografia}

%% Local Variables:
%% mode: latex
%% coding: utf-8
%% fill-column: 70
%% tab-width: 2
%% End:
