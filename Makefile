#pdflatex options
override PLOPT+=-shell-escape

#bibtex options
override BOPT+=

#call-bibtex.sh options
override CBOPT+=

#program caller (for OS compatibility)
#Unix: `./'
#Windows: `call '
CALLER=./

#TeX files needed for compilation
TEXFILES=$(wildcard *.tex */*.tex)
#(change this later)

#Bib files needed for compilation
BIBFILES=$(wildcard *.bib)
#(change this later)

default: notas-grafoes.pdf

all: main fast

main: notas-grafoes.pdf notas-grafoes-print.pdf

fast: notas-grafoes-fast.pdf notas-grafoes-print-fast.pdf

available: notas-grafoes-print.pdf
	scp notas-grafoes-print.pdf tassio@ime.usp.br:~tassio/www/arquivo/2012-ii/grafoes/notas-grafoes.pdf
	scp tassio@ime.usp.br:~tassio/www/arquivo/2012-ii/grafoes/index.html index.html.original
	git log --date=short --pretty=format:"<tr><td>%ad</td><td>%s</td><td>%an</td></tr>" | grep -v "Merge branch" | head -15 > git.log.table
	$(CALLER)insert.sh index.html.original git.log.table
	scp index.html.original.updated tassio@ime.usp.br:~tassio/www/arquivo/2012-ii/grafoes/index.html 
	rm -f -- $(wildcard index.html.original*) $(wildcard git.log.table)

clean: cleanaux cleantildes

cleanall: clean cleanpdf

cleanaux: cleanfigurasaux
	rm -f -- $(wildcard *.aux *.log *.toc *.lot *.lof *.out *.bbl *.blg *.ent *.auxlock)

cleantildes:
	rm -f -- $(wildcard *~ */*~ */*/*~ */*/*/*~)

cleanfigurasaux:
	rm -f -- $(wildcard figuras/compilacoes/*.log figuras/compilacoes/*.ent figuras/compilacoes/*.dpth)

cleanfiguraspdf:
	rm -f -- $(wildcard figuras/compilacoes/*.pdf)

cleanpdf: cleanfiguraspdf
	rm -f -- $(wildcard *.pdf)

notas-grafoes.pdf: $(TEXFILES) $(BIBFILES)
	pdflatex $(PLOPT) notas-grafoes
	$(CALLER)call-bibtex.sh $(CBOPT) -b $(BOPT)
	pdflatex $(PLOPT) notas-grafoes
	pdflatex $(PLOPT) notas-grafoes

notas-grafoes-print.pdf: $(TEXFILES) $(BIBFILES)
	pdflatex $(PLOPT) notas-grafoes-print
	$(CALLER)call-bibtex.sh $(CBOPT) -b $(BOPT)
	pdflatex $(PLOPT) notas-grafoes-print
	pdflatex $(PLOPT) notas-grafoes-print

notas-grafoes-fast.pdf: $(TEXFILES) $(BIBFILES)
	pdflatex $(PLOPT) notas-grafoes-fast

notas-grafoes-print-fast.pdf: $(TEXFILES) $(BIBFILES)
	pdflatex $(PLOPT) notas-grafoes-print-fast
