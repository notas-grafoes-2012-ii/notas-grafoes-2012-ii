#!/bin/bash

# Este script imprime todas as ocorencias do comando \label
# em arquivos `tex`, e o numero de ocorrencias de cada 
# label.
#
# Eu assumo que nenhum arquivo tem o caractere ':' no nome.

find . -name '*.tex' -exec ./labels-in-file.sh {} \;
find . -name '*.tex' -exec ./refs-in-file.sh {} \;
