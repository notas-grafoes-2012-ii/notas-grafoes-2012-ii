04-planaridade.tex:%DONE fica a lista aqui?
aulas/planaridade-1-grafos-planos.tex:%DONE decidir se usa "mapa de G" ou "mapa plano de G".
% Decidimos usar "mapa plano de G"

aulas/planaridade-1-grafos-planos.tex:%DONE decidir se entrarão esses termos em inglês
% Decidimos usar termos em inglês

aulas/planaridade-1-grafos-planos.tex:%TODO tn Consertar rótulos das subfiguras
% Quando olhamos no pdf, os rótulos precisam de duas linhas para aparecer. Não consegui resolver isso sozinho, acho que eles poderiam ser centralizados ou um pouco mais largos.

aulas/planaridade-1-grafos-planos.tex:%TODO pra que serve isso?
% Quando encontrarmos o local onde isso é usado/ou não, resolvemos isso daqui

aulas/planaridade-1-grafos-planos.tex:%READ fazer referência ao teorema de Kuratowski
% Resolvido já

aulas/planaridade-1-grafos-planos.tex:%READ ver o espaçamento antes do teorema
% Aquele probleminha com espaçamento acima dos enunciados dos teoremas. Aparentemente está funcionando.

aulas/planaridade-1-grafos-planos.tex:%READ tirar a prova e colocar a referência aqui dentro do teorema Nishizeki \& Chiba
% Logo após o comentário, tem dizendo que a prova está num material distribuído em sala de Nishizeki & Chiba, então podemos somente colocar as referências lá.
% Coloquei a referência e uma frase indicando que a demonstração de tal resultado pode ser encontrada no artigo referente.

aulas/planaridade-1-grafos-planos.tex:%READ ver essa questão de ter nome, data e "nome do teorema"
% Neste caso, o teorema é devido a Euler, tem data, e ao mesmo tempo se chama "Fórmula de Euler".
% É interessante ter todas estas informações? ou pode ser "Teorema 5. (Fórmula de euler, 1759)?
% Talvez seja interessante criar o nosso ambiente teorema com duas variáveis (não necessárias): referência (autor ou nome do teorema) e data.

aulas/planaridade-1-grafos-planos.tex:%READ como apresentar esta lista de corolários de uma forma melhor?
% O que fiz foi juntar os resultados dois a dois, já vi que vocês transformaram um em exercício.

%%% Fim do arquivo planaridade-1-grafos-planos.tex


aulas/planaridade-2-menores.tex:%READ as definições de planaridade foram mudadas para o arquivo planaridade-2.tex que contém os conceitos e resultados básicos
% No final das contas, as definições ficaram no arquivo planaridade-1-grafos-planos.tex

aulas/planaridade-2-menores.tex:%READ é pra ter o ambiente OBSERVAÇÃO?
% Foi abolido o ambiente observação. O que vai no lugar? deixei um \textbf{Observação:}

aulas/planaridade-2-menores.tex:%READ está definido isomorfismo?
% Não vi onde foi definido isomorfismo. Neste parágrafo é usado o conceito de dois grafos isomorfos.
% A definição é simples, pode ir lá na introdução.

aulas/planaridade-2-menores.tex:%READ como está esse negócio de colocar exercício no meio do texto?
% A gente pára o texto pra botar um exercício quando é conveniente ou coloca todos os exercícios no final? ou tanto faz?

aulas/planaridade-2-menores.tex:%READ essa questão da partição é delicada
% É delicada, mas é isso aí mesmo.

aulas/planaridade-2-menores.tex:%READ as próximas duas proposições não possuem prova no texto. Podemos deixar como exercício.
% São as seguintes proposições
% 1. Todo menor topológico de um grafo~\G é um menor de~\G
% 2. Todo menor de grau máximo não maior que 3 de um grafo G é também um
%    menor topológico de~\G.
% 3. Mostre que o grafo de petersen tem~\(K_5\) como menor mas não possui~\(K_5\)
%    como menor topológico.

aulas/planaridade-2-menores.tex:  %READ ver como é que faz para dar label aos ítens de uma proposição, aqui ocorre um problema por causa disso. RESPOSTA: use enumerate
% Eu gostaria de dar rótulos diferentes para ítens diferentes do itemize.

aulas/planaridade-2-menores.tex:  %READ colocar aquela separação da idaXvolta
% Qual o comando que estamos usando para separar o "se" do "somente se" na demonstração de um teorema?

aulas/planaridade-2-menores.tex:%READ Ver a numeração das figuras. Tem algo de errado acontecendo.
% Não sei por que motivo, mas este label não tá funcionando.

aulas/planaridade-2-menores.tex:%READ ver se a seguinte frase é necessária ou não.
% "O teorema X e o teorema Y serão usados na primeira demonstração do teorema de Kuratowski
% Só tem uma demonstração no texto.
% Não é meio óbvio que o teorema será usado na demonstração?

aulas/planaridade-2-menores.tex:%READ ver se aqui não é melhor citar o teo:5.6
% Devo ter julgado que não poderia decidir isso sozinho. Temos que ler o texto.
% É um pouco complicado. Neste caso falamos dos teoremas 4.15 e 4.16 no pdf.
% Um diz um pouco mais que o outro. Mas esse parágrafo é meio confuso.
% >>>> Olhar teoremas de conexidade (remover provas redundantes)

aulas/planaridade-2-menores.tex:%READ botei a citação dentro do exercício.
% É só pra saber se vocês não tem problema com isso.
% É um exercício (exercício 4.5) que está resolvido ou proposto num paper do thomassen. Aí botei um "(Veja o paper tal)" no final do exercício.
>>> como abreviar página

%%% Fim do arquivo planaridade-2-menores.tex


aulas/planaridade-3-caracterizacoes-Kuratowski.tex:%READ Foi para o arquivo planaridade-1-grafos-planaes.tex como exercício após o resultado que mostra que K_5 e K_{3,3} não são planares
% O exercício que diz pra provar que qualquer subgrafo de K_5 ou K_{3,3} é planar foi pra outro capítulo. 

aulas/planaridade-3-caracterizacoes-Kuratowski.tex:%READ aqui temos várias provas. Será que não existe outra forma de numerar estas provas?
% No caso se termos várias provas para um mesmo teorema, não podemos ter um contador pra cada prova de um mesmo teorema?

aulas/planaridade-3-caracterizacoes-Kuratowski.tex:%READ aparentemente temos somente uma prova para este teorema.
% Até onde eu vi, só tinha uma prova, não sei onde foi parar a outra
% >>

aulas/planaridade-3-caracterizacoes-Kuratowski.tex:	%TODO o que ela quer dizer com 'split graphs"? as componentes de G-{x,y}?
aulas/planaridade-3-caracterizacoes-Kuratowski.tex:	%TODO provavelmente G_1 é definido da seguinte forma: seja H_2 uma componente de G-{x,y}. Então G_1 = G-H_2.
% Ela não diz em lugar nenhum o que é um split graph com respeito a alguma coisa. Acredito que devemos definir aqui.
% Eu acredito que entendi o que é pelo contexto seguinte, mas preciso de confirmação de vocês.

aulas/planaridade-3-caracterizacoes-Kuratowski.tex:	%TODO fb: deixei uma demonstração abaixo baseada na da YW, mas com os detalhes escritos.
% Acabei não deixando :P

aulas/planaridade-3-caracterizacoes-Kuratowski.tex:	%TODO botar a divisão entre casos/ida+volta de teoremas aqui.
% Mesmo problema que já citei anteriormente.

aulas/planaridade-3-caracterizacoes-Kuratowski.tex:	%TODO verificar o caption
% Era uma figura com várias subfiguras, eu queria que o sub-índice de cada uma fosse igual ao índice no intemize que usei no teorema.
% Resolvi de outra forma: no itemize do teorema eu botei referência à subfigura específica.

aulas/planaridade-3-caracterizacoes-Kuratowski.tex:%READ apaguei este teorema e deixei apenas o teorema de Wagner
% Só tem que ver se vocês concordam. 
% O teorema que tinha aqui era uma revisão que apresentava o teorema de Kuratowski de novo e o teorema de Wagner.
% Achei desnecessário e só deixei o de Wagner

aulas/planaridade-3-caracterizacoes-Kuratowski.tex:%READ parti o arquivo aqui.

%%% Fim do arquivo planaridade-3-caracterizacoes-Kuratowski.tex


aulas/planaridade-3.tex:%TODO ciclo foi definido?
% Circuito foi definido, mas ciclo foi? uma definição boa é que um ciclo é um grafo/subgrafo euleriano.
% >> Definir ciclo, cociclo

aulas/planaridade-3.tex:%TODO é bom definir ortogonalidade, né?
% é!

aulas/planaridade-3.tex:%TODO tem que rever essa definição.
% Definição de dual geométrico. Escrevi de uma forma que dá pra entender, mas não deve ser a melhor.

aulas/planaridade-3.tex:%TODO continuar daqui.
aulas/planaridade-4-caracterizacoes-Maclane.tex:%TODO parti o arquivo aqui.
aulas/planaridade-4-caracterizacoes-Maclane.tex:%TODO falta muita coisa, vou organizar pra fazer sentido.
aulas/planaridade-4-caracterizacoes-Maclane.tex:%READ ciclo foi definido?
aulas/planaridade-4-caracterizacoes-Maclane.tex:%TODO falta uma prova aqui
aulas/planaridade-exercicios.tex:%TODO: qual teorema afinal é para provar no exercício 17 (numeração yw)? os enunciados não batem!!
