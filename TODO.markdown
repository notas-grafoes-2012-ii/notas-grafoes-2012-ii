# Agendadas
- sugestão: definir "milestones"
  - versão 0.5: capítulos revisados por uma pessoa
  - versão 0.6: revisão verificada por alguma dupla
  - versão 0.7: unificação da terminologia
  - versão 0.8: algum prof. olhou
  - versão 0.9: revisão para publicação
  - versão 1.0: primeira errata
- avisar em https://groups.google.com/d/forum/grafoes-2012
  quando novas versões estiverem prontas.
- checklist de buscas a fazer no texto
  - procurar por abreviações em maiúsculas ex: árvore BP
  - procurar por ":=" e substituir por \deq
  - substituir "\,:\," por \colon
  - definição de conjuntos {x| primo} vira {x : primo} (usamos \colon?)
  
# Tarefas sem dono
- uniformizar rótulos, e remover espaços!
- qual a abreviação correta de "página"? E de "páginas?" (está pp em planaridade)
- remover acentos dos labels
- definir split graph, ciclo, cociclo
- inserir na introdução a caracterização de digrafo euleriano
- criar ambiente de provaNumerada
- definir isomorfismo na introdução
- adicionar capítulo final sobre grafos aleatórios
  - resultado: concentração do número cromático
  - transição de fase para propriedades "crescentes"
- definir hipergrafos, seção de caminho, isomorfismo
- seção sobre notação: incluir teoremas
- adicionar referência para a apostila do minicurso de Grafos (yoshi,
  yoshiko, pf), e do livro de exercícios do pf
- arrumar o espaço antes do teorema da dec. de Edmonds--Gallai (se puder!)
- se há mais concatenações e seções de caminho, acrescentar uma definição
  dessas coisas na introdução
- que resultados de coloração introduzir nas notas?
- resolver se vamos usar "natural" ou "inteiro positivo".

# Tarefas de Todos
- todos lerem demonstração dos teoremas
  - a => d da caracterização de grafo 2-aresta-conexo
  - Lovász sobre emparelhamentos
- Revisar as notas do Eric que foram incluídas no texto por Fábio.
- assistir o destino de miguel
- ler apologia de Sócrates
- estudar provas da equivalência entre rotulação do algoritmo e classes do
  Teo. de Edmonds--Gallai

# Eric Ossami Endo (eo)
- capítulo sobre Ramsey

# Fábio Botler (fb)
- teorema da roda (wheel) definido em conexidade, referenciar em vez de demonstrar de novo
- capítulo sobre planaridade
- perguntar à Yoshiko se ela sugere alguma bibliografia sobre
  emparelhamentos
- falar à Yoshiko da ideia de publicar as notas de aula
- artigos sobre fatores
- verificar se a prova do T. de Menger usando submodularidade é de 
  András Frank e foi extraída do Handbook of Combinatorics.
- bibtex: acrescentar: O. Ore, Graphs and matching theorems. Duke Math. J.
- confirmar se C. Berge, Sur le couplage maximum d’un graphe. (French) C. R. Acad. Sci. Paris é a fórmula de Berge, e adicionar ao bibtex
- Escrever a prova do Teorema 4.4 (ou teo:5.8)
 
# Leonardo Nagami Coregliano (lc)
- capítulo conexidade

# Tássio Naia (tn)
- script para extrair todos multilinha
  %TODO> título do todo
  % corpo da tarefa (primeira linha)
  % corpo da tarefa (segunda linha)
  % ...
  % última linha
  %TODO<
- diferença entre \comma e a vírgula, \colon e ":"
- controlar largura das legendas em subfloats
- meta-informações do pdf
- fechar capítulo sobre emparelhamento
- formatação de exercícios: como no bondy--murty
- fazer desenhos que estão marcados com %TODO em "notas-do-eric---menger.tex"
- propriedades da decomposição de Edmonds--Gallai
- publicar _On the number of witnesses for the Tutte--Berge formula_
- assistir O Homem da Terra

# Baixa prioridade
- inserir na introdução um bla sobre os assuntos debatidos
- links para endnotes (notas de fim de capítulo)
- backlinks em footnotes e endnotes (ligando ao ponto do texto que os cita)
- pensar se colocamos a referência do livro "Combinatorial
  Optimization", de William J. Cook, William H. Cunningham, William
  R. Pulleyblank, Alexander Schrijver
- prova de West da decomposição de Edmonds--Gallai

# Baixas...

## Antonio Josefran de Oliveira Bastos (aj)
## Rafael Santos Coelho (rc)

