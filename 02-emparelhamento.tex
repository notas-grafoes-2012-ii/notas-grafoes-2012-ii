\chapter{Emparelhamentos}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%                                %%
%%  \input{aulas/02-1-emp-intro}  %%
%%                                %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Um conjunto de arestas~\M\ de um grafo~\G\ é chamado de
\defi{emparelhamento} se para cada vértice \(v \in V(G)\) existe no
máximo uma aresta de \(M\) incidente a \(v\). A teoria de
emparelhamentos em grafos é uma área bastante estabelecida, com
resultados em várias
direções~\cite{lovplu86matching,cook2011combinatorial}
e~generalizações\pagenote{\input{notas/fatores}}. Nestas notas
apresentamos alguns resultados clássicos com o objetivo de compreender
a decomposição de \Edmonds--\Gallai\ e o algoritmo de \Edmonds\ para
obter emparelhamentos máximos em grafos quaisquer.

Dado um emparelhamento~\M\ e um vértice~\(v\), dizemos
que~\M\ \defi{cobre} (ou \defi{satura})~\(v\) se alguma aresta
de~\M\ incide em~\(v\). Caso contrário, dizemos que~\(v\) é um vértice
\defi{não-coberto} por~\M\ ou \defi{livre} (de~\M).  Dizemos que um
emparelhamento~\(M\) em~\G\ é~\defi{maximal} se não existe
emparelhamento~\(M'\) que contém~\(M\) propriamente. Um
emparelhamento~\(M\) é dito~\defi{máximo} se não existe em $G$ nenhum
emparelhamento de cardinalidade maior que a de $M$.

Em geral, estamos interessados em determinar condições para a
existência de emparelhamentos que cobrem todos os vértices do
grafo. Tais emparelhamentos são ditos \defi{perfeitos}. Apresentamos
ainda fórmulas explícitas para o cálculo da cardinalidade~\(\emp(G)\)
de um emparelhamento máximo de~\G, e um algoritmo para obter um
emparelhamento máximo.

Dado um emparelhamento~\M\ de um grafo~\G, um \defi{caminho
  \(M\)-alternante} em~\G\ é um caminho cujas arestas estão
alternadamente em~\M\ e e em \(E(G)\setminus M\). Um tal caminho com
ambos os extremos não-cobertos por~\M\ é chamado \defi{caminho
  aumentador} (nome motivado pelo Teorema de Berge, enunciado a
seguir).


\begin{teorema}[\Berge, 1957]\label{teo:berge_aumentador}
\textsl{Seja~\G\ um grafo e~\M\ um emparelhamento em~\G. Então~\M\ é um
emparelhamento máximo se e só se~\G\ não tem nenhum caminho
\(M\)-alternante com ambos os extremos livres.}
\end{teorema}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%                                   %%
%%  \input{aulas/02-2-emp-perfeito}  %%
%%                                   %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Emparelhamentos perfeitos}

\begin{teorema}[Hall, 1935]
  \textsl{Seja \G\ um grafo \((A,B)\)-bipartido. Então \G\ tem um
  emparelhamento que cobre \(A\) se e somente se \(\bigl|\adj(X)\bigr|
  \geq \bigl|X\bigr|\) para todo \(X\subseteq A\).}
\end{teorema}

\begin{prova}
  Demonstramos apenas uma das implicações. Suponha que não exista
  emparelhamento que cobre~\(A\), e seja~\M\ um emparelhamento máximo
  de~\G. Então existe vértice~\(v\in A\) que não é coberto por~\M.
  Considere \(A' \cup B'\) (\(A' \subseteq A\), \(B' \subseteq B\)) o
  conjunto de vértices de~\G\ que são atingíveis a partir de \(v\) por
  caminhos \M-alternantes. Como~\G\ é bipartido, todo caminho
  de~\(v\) até~\(a \in A'\) tem comprimento par. Logo, ~\(a\) é
  emparelhado com o vértice que vem exatamente antes dele em todos
  esses caminhos que o atingiram. Assim, todo vizinho de~\(a\) está
  em~\(B'\) e, portanto, \(\adj(A') \subseteq B'\).  Por outro lado,
\(B'\subseteq \adj(A')\), e portanto, \(\adj(A') =  B'\).   Por hipótese, temos
  que \(|\adj(A')| \geq |A'|\); logo, ~\(|B'| \geq |A'|\) e, assim, como~\(v\in
  A'\) não está coberto por~\M, existe pelo menos um vértice~\(b\)
  em~\(B'\) que não está coberto por~\M. O caminho \M-alternante de
  \(v\) a \(b\) tem comprimento ímpar e, portanto, é aumentador. Pelo
  Teorema~\ref{teo:berge_aumentador}, $M$ não é um emparelhamento máximo, uma contradição.
\end{prova}

\begin{corolario}\label{cor:emparelhamento_|A|-k}
 \textsl{Seja \G\ um grafo \((A,B)\)-bipartido. Se \(|\adj(X)|\geq|X|-k\)
  para todo \(X\subseteq A\) e algum inteiro fixo \(k\), então
  \G\ possui um emparelhamento de cardinalidade~\(|A|-k\).}
\end{corolario}

\begin{prova}[Demonstração (sugestão)]
Adicione~\(k\) vértices a~\(B\), conectados cada um a todos os vértices
de~\(A\).
\end{prova}

Um conjunto~\(S\) de vértices de um grafo~\G\ é dito uma
\defi{cobertura} (por vértices) se toda aresta de~\G\ é incidente a
pelo menos um vértice de~\(S\). O tamanho de uma menor cobertura
de~\G\ é denotado por~\(\cob (G)\). É fácil ver que~\(|S|\geq |M|\),
para qualquer emparelhamento~\(M\). O seguinte teorema estabelece a
célebre relação min--max entre coberturas e
emparelhamentos~\cite{diestel05,bondy2008graph}.

\begin{teorema}[\Konig, 1931]
\textsl{Seja \G\ um grafo bipartido. A cardinalidade de um emparelhamento
máximo de~\G\ é igual à cardinalidade de uma cobertura mínima de~\G.}
\end{teorema}


%\begin{prova}
%\((\emp G\leq \cob (G))\)
%Sejam~\M\ um emparelhamento e~\C\ uma cobertura de~\G. É fácil ver
%que~\(|M| \leq |C|\), pois as arestas do emparelhamento são
%independentes, e cada uma incide em algum vértice da cobertura. Em
%particular a desigualdade vale para o emparelhamento máximo e para a
%cardinalidade mínima.
%\end{prova}

%\begin{prova}[Prova alternativa de ~\(\emp G \leq \cob (G)\).]
%Considere o grafo~\(H=(V(G),M)\) e observe que~\(|M|\leq \sum_{v\in C}d_H(v)\leq |C|\), onde a primeira desigualdade decorre de~\(C\) ser uma  cobertura, e a segunda de~\(M\) ser um emparelhamento.
%\end{prova}

\begin{prova}
Demonstraremos que~\(\emp G \geq \cob (G)\). A outra desigualdade é um
exercício. Seja~\G\ um grafo~\((A,B)\)-bipartido e~\C\ uma cobertura
mínimade~\G. Definimos os conjuntos
\begin{equation*}
A_C=A\cap C,\quad
B_C=B\cap C,\quad
A_{\overline{C}}=A\setminus C,\quad\text{e}\quad
B_{\overline{C}}=B\setminus C.
\end{equation*}
%
Seja~\aga\ o subgrafo de~\G\ induzido por~\(A_C\cup
B_{\overline{C}}\). É claro
que~\aga\ é~\((A_C,B_{\overline{C}})\)-bipartido. Mostramos a sequir
que a minimalidade de~\C\ garante que~\aga\ satisfaz a hipótese do
Teorema de Hall.

Seja~\X\ um subconjunto qualquer de~\(A_C\). Note que o conjunto~\(C\setminus
X)\cup \adj_H(X)\) é uma cobertura de~\G (observe que toda aresta que tem uma
ponta em~\X\ tem a outra ponta em~\(\adj_H(X)\)). Como a
cardinalidade dessa cobertura é~\(|C| -|X| + |\adj_H(X)|\), a
minimalidade de~\C\ implica que~\(|\adj_H(X)|\geq |X|\).

Pelo Teorema de Hall, existe um emparelhamento~\F\ em~\aga\ que
cobre~\(A_C\). De maneira análoga, podemos concluir que o
subgrafo~\(H'\) de~\G\ induzido por~\(B_C\cup A_{\overline{C}}\)
possui um emparelhamento~\(F'\) que cobre~\(B_C\). Ademais, \(F\cup
F'\) é um emparelhamento em~\G\ e
%
\[
\emp(G)\geq |F\cup F'|=|F| + |F'| =|A_C| + |B_C| = |C|=  \cob (G). \qedhere
\]
\end{prova}

Dado um grafo~\aga, denotamos por~\defi{\(c_o(H)\)} o número de
componentes (conexas) de~\aga\ que têm um número ímpar de vértices. O
teorema a seguir fornece uma condição para a existência de
emparelhamentos perfeitos em grafos arbitrários em termos do número de
tais componentes.

\begin{exercicio}
  Demonstre o Teorema de Hall de duas formas:
  \begin{enumerate}[i)]
  \item Por indução em $|A|$, dividindo o estudo em dois casos:
    (1).~existe conjunto de vértices~\(S\subseteq A\) com
    \(\bigl|\adj(S)\bigr|=\bigl|S\bigr|\), e (2).~para todo \(S
    \subseteq A\)
    vale \(\bigl|\adj(S)\bigr|>\bigl|S\bigr|\).
  \item Usando o Teorema de \Konig.
  \end{enumerate}
\end{exercicio}

\begin{teorema}[Tutte, 1947]\label{teo:tutte-emp}
 \textsl{ Um grafo \(G=(V,E)\) tem um emparelhamento perfeito se e só se
  \(c_o(G-S)\leq|S|\) para todo \(S\subseteq V\).}
\end{teorema}

\begin{prova}
Em aula. [A ser incluído aqui.]
\end{prova}


\begin{exercicio}
  Deduza o Teorema de Hall do Teorema de Tutte.
\end{exercicio}

\begin{exercicio}
Um \defi{grafo cúbico} é um grafo em que todo vértice possui grau
\(3\). Prove que todo grafo cúbico sem arestas-de-corte tem um
emparelhamento perfeito (\Petersen, 1891). \hint{mostre que tal grafo
  satisfaz a condição do Teorema~\ref{teo:tutte-emp}.}
\end{exercicio}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%                                           %%
%%  \input{aulas/02-3-emp-maximo-e-defeito}  %%
%%                                           %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Emparelhamentos máximos e a deficiência de um grafo}

Dado um grafo~\((A,B)\)-bipartido~\G\ e um conjunto~\(X\subseteq A\),
dizemos que~\(|X|-|\adj(X)|\) é a~\defi{deficiência} de~\(X\) (com
relação a~\(A\)), e a denotamos
por~\(\defi{\(\defic_A(X)\)}\). Definimos a ~\defi{$A$-Deficiência
  de~$G$} por~\(\deficiencia_A(G)=\max_{X\subseteq
  A}\defic_A(X)\). Note que a $A$-Deficiência de~\G\ é não-negativa,
uma vez que, para~\(X=\vazio\), temos~\(\defic_A(X)=0\).

%DONE a referência \cite{Ore55} não existe!
Em 1955,~\Ore~\cite{Ore55} publicou uma ``versão defectiva''
do~\mencao{Teorema de Hall} para grafos bipartidos. Em 1958, Berge
obteve uma versão generalizada para grafos arbitrários, tendo como
base o Teorema de Tutte.

\begin{teorema}[\Ore, 1955]\label{teo:Ore_deficiencia}\label{teo:Ore55}
  Se~\G\ é um grafo~\((A,B)\)-bipartido, então~\[\emp
  (G)=|A|-\deficiencia_A(G).\]
\end{teorema}

\begin{prova}
A desigualdade \(\emp(G)\leq|A|-\deficiencia_A(G)\) segue diretamente
da definição de~\(\deficiencia_A(G)\); a desigualdade
\(\emp(G)\geq|A|-\deficiencia_A(G)\) segue do
Corolário~\ref{cor:emparelhamento_|A|-k}.
\end{prova}


O Teorema~\ref{teo:Ore_deficiencia} afirma que se $G$ é um
grafo~\((A,B)\)-bipartido
então a sua $A$-Deficiência (resp. $B$-Deficiência) é exatamente o
número de vértices em $A$ (resp. $B$) que não são cobertos por um
emparelhamento máximo.


Definindo-se a \defi{Deficiência de~$G$}, $\deficiencia(G)$, onde $G$
é \((A,B)\)-bipartido, como sendo $\defic_A(G) + \defic_B(G)$, temos
que
 $\deficiencia(G) = \defic_A(G) + \defic_B(G) =
(|A| - \emp(G)) + (|B| - \emp(G)) = |A\cup B| - 2\emp(G) = |V(G)| - 2\emp(G)$.


Esta igualdade motiva a definição de \textbf{Deficiência} de um
grafo arbitrário $G$, que mencionamos na próxima subseção.

% como sendo \textit{o número de vértices não cobertos
% por um emparelhamento máximo em $G$}.  isto é,

% \begin{align*}
%   \deficiencia(G)
%   &\igual |V| -2\emp (G), \\
%   &\igual \min_{M\in \famm}\bigl\{|V|-2|M|\bigr\},
% \end{align*}
% onde~\famm\ é o conjunto dos emparelhamentos de~\G.

% Em 1958, Berge~\cite{} provou o teorema a seguir, conhecido
% como~\defi{fórmula de Berge}.


% conclui que a deficiência de um
% grafo bipartido~\G\ é o número de vértices descobertos por um
% emparelhamento máximo, isto é, \(\deficiencia (G)=|V|-2\emp
% (G)\). Esta última igualdade motiva uma definição de defeito para
% grafos arbitrários.

\section{Deficiência em grafos arbitrários}

Seja~\M\ um emparelhamento em~\G. Definimos o~\defi{defeito de~\M}
como sendo o número de vértices não cobertos por~\M. Definimos a
\defi{Deficiência de~\G}, $\deficiencia(G)$,  como sendo o número de vértices não cobertos
por um emparelhamento máximo, isto é,
\begin{align*}
  \deficiencia(G)
  &\igual |V| -2\emp (G), \\
  &\igual \min_{M\in \famm}\bigl\{|V|-2|M|\bigr\},
\end{align*}
onde~\famm\ é o conjunto dos emparelhamentos de~\G. Em 1958,
Berge~\cite{} provou o teorema a seguir, conhecido como~\defi{Fórmula
  de Berge}.

\begin{teorema}[Fórmula de Berge]\label{teo:formula_Berge}
  Para todo grafo~$G=(V,E)$, tem-se que
\begin{equation}\label{eq:formula_Berge}
\deficiencia (G) = \max_{S\subseteq V}\bigl\{c_o(G-S) -|S|\bigr\}.
\end{equation}
\end{teorema}

O Teorema~\ref{teo:formula_Berge} é consequência do teorema a
seguir, que quantifica o tamanho de um emparelhamento máximo em im
grafo arbitrário.

\begin{teorema}[Fórmula de Tutte-Berge]\label{teo:formula_Tutte-Berge}
  Para todo grafo~\G=(V,E)\, temos que
\begin{equation}\label{eq:formula_Tutte-Berge}
\emp (G) = \min_{S\subseteq V} \Bigg\{
\frac{|V| +|S| -c_o(G -S)}{2}
\Bigg\}.
\end{equation}
\end{teorema}

\begin{prova}
  É suficiente provarmos para o caso em que $G$ é
  conexo. Suponha então que $G$ seja conexo.  Primeiramente, demonstramos
  que~\(\emp (G) \leq \frac{1}{2}\min_{S\subseteq V} \bigl\{ |V|
  +|S| -c_o(G -S)\bigr\}\).
  Se  para todo $S\subseteq V$ tem-se que $|S| \geq c_o(G-S)$, pelo
  Teorema de Tutte, $G$ tem um emparelhamento perfeito, e portanto a
  desigualdade vale. Se existe~\(S \subseteq V\)
  tal que~\(c_o(G-S) > |S|\),
  então pelo menos~\(c_o(G-S) - |S|\)
  vértices devem ficar livres em qualquer
  emparelhamento de $G$. Assim, $G$ tem no máximo $|V| - c_o(G-S) -
  |S|$ vértices cobertos, ou seja, temos no máximo
  \(\frac{1}{2}\bigl(|V| - c_o(G-S) + |S|\bigr)\)
  arestas em um emparelhamento, donde segue
  que~\(\emp(G) \leq \frac{1}{2}\min_{S \subseteq V} \bigl\{ |V| + |S|
  - c_o(G-S)\bigr\}\).

A demonstração de~\(\emp (G) \geq \frac{1}{2}\min_{S\subseteq V}
\bigl\{ |V| +|S| -c_o(G -S)\bigr\}\) segue por indução no número de
vértices~\(|V|\). Se~\(|V| = 1\) a fórmula é óbvia, e ambos os
lados resultam~\(0\). Suponha então que~\(|V| > 1\) e que o teorema
valha para todo grafo com menos vértices do que $G$.  Consideramos
dois casos.

\emph{Caso 1.} Existe um vértice~\vv\ que é coberto por todo
emparelhamento máximo de~\G. Seja~\M\ um tal emparelhamento. Considere
o grafo~\(G' \deq G - v\) obtido de~\G\ pela remoção do vértice \vv.
Seja $V'= V \setminus\{v\}$. Seja~\(e\) a aresta de~\M\ que
cobre~\vv\ e tome o emparelhamento~\(M' = M-e\) de~\glinha. Se existe
emparelhamento de~\(G'\) de tamanho~\(|M'| + 1 = |M| = \emp(G)\), então
existe emparelhamento máximo em~\G\ que não emparelha~\vv.
Logo, ~\(\emp(G') = \emp(G) -1\). Pela hipótese de indução, existe~\(S'
\subseteq V'\) tal que \(|V'| + |S'| - c_o(G'-S') = 2|M'|\).
Considere o conjunto~\(S\deq S'\cup \{\vv\} \subseteq V\) e observe
que, uma vez que~\(G' = G -v\), temos que~\(G' - S' = G - v - S' = G -
S\). Logo, temos que~\(c_o(G'-S') = c_o(G-S)\) e, portanto,
\begin{align*}
|M|
\igual
|M'| + 1
&\igual
\frac{|V'| + |S'| - c_o(G'-S')}{2} + 1\\
&\igual
\frac{(|V'| +1) + (|S'|+1) - c_o(G'-S')}{2}\\
&\igual
\frac{|V| + |S| - c_o(G-S)}{2}\\
&\gigual
\min_{S \subseteq V} \left\{ \frac{|V| + |S| - c_o(G-S)}{2}\right\}.
\end{align*}

\emph{Caso 2.} Todo vértice de~\G\ não é coberto por algum
emparelhamento máximo. Vamos provar que \emph{exatamente} um vértice
fica livre em cada emparelhamento máximo.

Suponha que para todo emparelhamento máximo existam dois vértices livres. Tome então um emparelhamento máximo~\M\ tal que a
distância~\(\mathrm{d}(u,v)\) entre dois vértices
livres~\vu\ e~\vv\ seja mínima. A distância
entre~\vu\ e~\vv\ não pode ser~\(1\), caso contrário podemos adicionar
a aresta~\(uv\) a~\M\ e obter um emparelhamento maior. Além disso,
todo vértice interior num caminho de menor comprimento
entre~\vu\ e~\vv\ deve ser coberto por~\M, caso contrário existiria um
par de vértices livres com distância menor que \(\mathrm{d}(\vu,
\vv)\). Tome~\(s\) um tal vértice e tome~\(N\) um emparelhamento
máximo de \G\ que não cobre~\(s\) e tal que~\(M\cap N\) seja o maior
possível. Note, em particular, que~\vu\ e~\vv\ são cobertos
por~\N\ (e~\emph{não} são cobertos por~\M). Ora, como a cardinalidade
de~\M\ e~\N\ é a mesma, existe um vértice~\(x\neq s\) que é coberto
por~\M, mas não é coberto por~\(N\).

Seja~\(y\in V(G)\) o vértice emparelhado com~\(x\) em~\M. Se~\(y\)
não for coberto por~\(N\) podemos adicionar a aresta~\(xy\) a \(N\),
entrando em contradição com a maximalidade de~\(N\). Se~\(y\) for
coberto por~\(N\), podemos retirar de~\(N\) a aresta que o cobre e
adicionar~\(xy\) em seu lugar, obtendo um emparelhamento \(N'\) com
uma interseção maior com~\M.

Concluímos que todo emparelhamento máximo deixa livre exatamente um
vértice. Portanto, $\emp(G) = (|V|-1)/2$. A Fórmula de Tutte-Berge
segue tomando $S=\emptyset$.
\end{prova}

\medskip

No Caso~2 da demonstração do Teorema~\ref{teo:formula_Tutte-Berge},
encontramos um grafo~\G\ tal que~\(G -v\) possui um emparelhamento
perfeito para todo~\(v\in V(G)\). Neste caso, dizemos que~\G\ é
\defi{hipoemparelhável}. Mais geralmente, se~\famb\ é uma propriedade
sobre grafos, dizemos que um
grafo~\G\ é~\defi{hipo}-\famb\ se~\(G\) não tem a propriedade $\famb$,
mas \(G -v\) tem a propriedade \(\famb\) para todo vértice~\vv\ de~\G.

Na busca de um emparelhamento com cardinalidade máxima, nos deparamos
com a questão de saber, dado um emparelhamento~\M, se é possível
encontrar um emparelhamento maior. O Teorema~\ref{teo:formula_Berge}
nos dá uma ideia de como proceder. Note que, para todo
conjunto~\S, a fórmula fornece um limitante inferior para o número de
vértices não-cobertos por um emparelhamento máximo. Sabemos que não é
possível encontrar emparelhamento que deixa menos do que~\(c_o(G -S)
-|S|\) vértices livres. Portanto, se~\M\ deixa exatamente~\(c_o(G
-S) -|S|\) vértices livres, para algum~\S, então~\M\ é máximo.

Estruturas como o conjunto~\S, que definem condições necessárias para
alguma propriedade, são chamados de~\defi{certificados}. O conjunto~S,
por exemplo, é um certificado de que \emph{ao menos} certa quantidade
de vértices é deixada livre por um emparelhamento em~\G.

Outro conceito que será útil adiante é o de \emph{testemunha}. Dado um
emparelhamento máximo~\M\ em um grafo~\G, o
%
 Teorema~\ref{teo:formula_Tutte-Berge}
%
garante a existência de um conjunto~\(S\) tal
que~\(|M|=\frac{1}{2}(|V| +|S| -c_o(G -S))\). Um tal conjunto~\(S\) é
dito \defi{testemunha} de~\G.


\begin{figure}[ht]
\begin{center}
 \PoeFigura{ex-decomposicao-edmonds-gallai}
  \caption{(Decomposição de \Edmonds--\Gallai\ de um grafo~$G$. Note que
    \(\deficiencia(G)=\max\bigl\{0,c_o(D_G)-|A_G|\bigr\}\), e \(\emp (G) = \bigl(|V| + |A_G| -
    c_o(D_G)\bigr)\!\!\bigm/\! 2\). Figura obtida de Plummer e Lovász~\cite{lovplu86matching}.}\label{fig:Edmonds-Gallai}
\end{center}
\end{figure}


\begin{teorema}[Decomposição de \Edmonds--\Gallai]\label{teo:decomposicao-edmonds-gallai}
  Dado um grafo~\gve, sejam
  \begin{align*}
    D_G &\deq \{v\in V : \text{existe emparelhamento máx.\ em
      \G\ que não cobre \vv} \},\\
    A_G &\deq \{v\in V\setminus D_G: \text{\vv\ é adjacente a algum
      vértice de \(D_G\)}\},\\
    C_G &\deq V\setminus\bigl(D_G \cup A_G\bigr).
  \end{align*}
Então
\begin{enumerate}[a)]
\item \(S=A_G\) é testemunha de~\G;
% isto é, atinge o mínimo no lado direito da Fórmula de
% Tutte-Berge~\ref{eq:formula_Tutte-Berge};
\item \(C_G\) é a união de componentes pares de~\(G-A_G\);
\item \(D_G\) é a união de componentes ímpares de~\(G-A_G\);
\item Todo componente ímpar de~\(G-A_G\) é hipoemparelhável.
\end{enumerate}
\end{teorema}

% O exemplo dado na Figura~\ref{fig:Edmonds-Gallai} foi obtida
% de Plummer e Lovász\cite{lovplu86matching}.

% \begin{figure}[ht]
% \begin{center}
%  \PoeFigura{ex-decomposicao-edmonds-gallai}
%   \caption{(Decomposição de \Edmonds--\Gallai\ de um grafo~$G$. Note que
%     \(\deficiencia(G)=\max\bigl\{0,c_o(D_G)-|A_G|\bigr\}\), e \(\emp (G) = \bigl(|V| + |A_G| -
%     c_o(D_G)\bigr)\!\!\bigm/\! 2\). Figura obtida de Plummer e Lovász~\cite{lovplu86matching}.}\label{fig:Edmonds-Gallai}
% \end{center}
% \end{figure}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%                                    %%
%%  \input{aulas/02-4-emp-algoritmo}  %%
%%                                    %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Um algoritmo para achar um emparelhamento máximo}

\begin{figure}[ht]
\begin{center}
\PoeFigura{emparelhamento-exemplo-de-flor}
\caption{Um
  emparelhamento~\(M=\bigl\{\{v_1,v_2\},\{v_3,v_4\},\{v_5,v_6\}\bigr\}\)
  e uma flor~\(v_0v_1\cdots v_7\).}\label{fig:passeio_com_circuito_impar}
\end{center}
\end{figure}

O Teorema~\ref{teo:berge_aumentador} sugere que podemos construir um
emparelhamento máximo iterativamente. Partimos de um emparelhamento
qualquer e enquanto houver um caminho~\M-aumentador~\P, substituímos o
emparelhamento corrente~\M\ por~\(M\sdiff P\). Descrevemos nessa seção
algumas estratégias que podem ser usadas para encontrar um tal caminho.

Seja~\(G=(V,E)\) um grafo, \M\ um emparelhamento em~\G, e~\X\ o
conjunto dos vértices livres (de~\M). Um~\emph{passeio
  \hbox{\M-alternante}}~\( v_0,v_1,\ldots,v_t \) é chamado
de~\defi{\M-flor} se satisfaz
(na Figura~\ref{fig:passeio_com_circuito_impar}
considere o passeio $v_0,v_1,\ldots,v_7$):
\begin{enumerate}[a)]
\item \(v_0\in X\);
\item \(v_0,\ldots, v_{t -1}\) são distintos;
\item \(t\) é ímpar, e \(v_t=v_i\) para algum \(i\) par, $i<t$.
\end{enumerate}

%Podemos construir o
%digrafo~\(\Gchapeu=(V,A)\) onde~\( A = \{ uw \,:\, \exists\, v \in V
%\text{ tal que } uv \in E \setminus M \text{, e } vw \in M \}\). Um
%caminho em~\Gchapeu\ é um passeio \M-alternante em~\G.


Seja \(T=v_0v_1\cdots v_k\) um passeio~\M-alternante em~\G\ entre
vértices distintos de~\X. Se~\T\ é um caminho, então \T\ é um
caminho \hbox{\M-aumentador.} Caso contrário, seja~\(j\) o menor inteiro tal
que~\(v_i=v_j\), para~\(i < j\). É fácil ver que~\(v_0v_1\cdots v_j\)
é uma~\M-flor. A parte da~\M-flor de~\vindice0\ a~\vindice i é
chamada~\defi{caule} (``\defi{stem}'') e a parte de~\vindice i
a~\vindice t é chamada de \defi{botão} (``\defi{blossom}'') ou
\defi{$M$-botão}. Dizemos
que~\vindice 0 é a~\defi{raiz} da flor ou do botão. Se~\B\ é
um~\M-botão, definimos o grafo~\defi{\(G/B\)}
(chamado~\G\ \defi{contraído} de~\B), com
emparelhamento~\mencao{\(M/B\)}, como o grafo que resulta da
substituição do botão~$B$ por um vértice. Mais formalmente, temos
\begin{itemize}
\item \(V(G/B)=(V\setminus B)\cup\{b\}\), onde~\(b\notin V\) é um
  vértice novo;
\item
  \(E(G/B)= \bigl(E\setminus\{e\in E: \text{\(e\) incide em
    \B}\}\bigr) \,\cup\, \bigl\{vb:v\in V(G/B), vz \in E(G), z\in B
  \bigr\}\);
\item \(M/B = \bigl(M \setminus \{e\in E:\text{\(e\) incide em
  \B}\}\bigr) \,\cup\, \bigl\{vb:v\in V(G/B), vz \in M, z\in B
  \bigr\}\).
\end{itemize}
Note que~\(\bigl\{vb:v\in V(G/B), vz \in M, z\in B \bigr\}\) possui
apenas uma aresta.

\medskip

O Teorema~\ref{teo:berge_aumentador} diz que se não houver caminho
\M-aumentador, então~\M\ é um emparelhamento máximo, e todo passeio
\M-alternante entre vértices distintos de~\X\ possui uma flor. A
utilidade  da operação de \defi{contração}, definida acima, fica
aparente pelo enunciado do teorema a seguir.





%% Se existe um caminho \M-aumentador~\(P=v_0v_1\cdots v_k\) para~\G, então podemos
%% construir um caminho orientado \(\hat P=v_0v_3\cdots
%% v_{k -1}\) em~\Gchapeu, entre~\(v_0\in X\) e~\(v_{k -1} \in N(X
%% -v_0)\). A recíproca, contudo, não é verdade --- tome, por exemplo, um
%% caminho em~\Gchapeu\ correspondente a um passeio em~\G\ que contenha
%% um circuito ímpar, como na
%% figura~\ref{fig:passeio_com_circuito_impar}.


\begin{teorema}[\Edmonds, 1965]\label{teo:2.11}
Seja~\gve\ um grafo, \M\ um emparelhamento em~\G\ e~\B\ um
\M-botão. Então~\M\ é um emparelhamento máximo em~\G\ se e somente se
\(M/B\) é um emparelhamento máximo em~\(G/B\).
\end{teorema}

\begin{prova}
  Vamos provar que existe um caminho~\M-aumentador em~\G\ se e somente
  se existe um caminho~\(M/B\)-aumentador em~\(G/B\).

  Seja~\F\ a flor de botão~\B. Para todo vértice~\(v\) de~\B, denote
  por~\(P_v\) o caminho de comprimento par em~\F\ que vai de~\(v\) até
  a raiz de~\B.

  Note que se existe um caminho~\M-aumentador~\P\ em~\G\ que não
  possui aresta de~\B, então~\P\ também é um caminho aumentador
  em~\(G/B\).

  Por outro lado, se existe um
  caminho~\M-aumentador~\P\ em~\G\ contendo arestas de~\B, então
  existe um caminho~\M-aumentador com extremo na raiz de~\B. De fato,
  tome~\vu\ um extremo de~\(P\) diferente da raiz de~\B\ e~\vv\ o
  primeiro vértice do botão em~\(P\) quando seguimos~\(P\) partindo
  de~\vu. Seja~\(Q\) o subcaminho de~\(P\) de~\vu\ até~\vv. O
  caminho procurado é dado pela união de~\(Q\) com~\(P_v\).

  Observe que a aresta de~\vv\ em~\(Q\) não é aresta de~\M\ e,
  portanto,~\((Q\cup P)/B\) é um caminho~\(M/B\)-aumentador
  em~\(G/B\).

  \medskip

  Suponha então que exista um caminho~\(M/B\)-aumentador~\(P\)
  em~\(G/B\). Se~\(P\) não contém~\(b\) então~\(P\) é um
  caminho~\M-aumentador em~\G. Se~\(P\) contém~\(b\) então
  seja~\vu\ um extremo de~\(P\) diferente da raiz de~\B\ e~\(v \in
  V(G)\) o primeiro vértice do botão em~\(P\). Como antes, tome~\(Q\)
  o caminho em~\(P\) de~\vu\ a~\vv\ e então~\(Q \cup P_v\) é um
  caminho~\M-aumentador em~\G.
\end{prova}

%TODO encontrar uma boa prova da outra implicação
%DONE revisar o formalismo
\begin{prova}[(Prova alternativa de uma das implicações.)]
  Suponhamos que~\(M/ B\) não seja um emparelhamento máximo em~\(G/
  B\). Seja~\(N\) um emparelhamento máximo em~\(G/ B\),
  temos~\(|N|>|M/ B|\).

  Considere o emparelhamento~\(N^{+}=\tilde N\cup \tilde M\),
  onde~\(\tilde N\) é um emparelhamento em~\(G\) que não possui
  arestas de~\(B\) e tal que~\(N/ B = \tilde N\) e~\(\tilde M\) é um
  emparelhamento quase-perfeito em~\(B\) compatível com~\(\tilde N\),
  isto é, existe exatamente um vértice em~\(B\) livre de~\(\tilde M\)
  e~\(N^{+}\) é um emparelhamento em~\(G\). Temos

  \[ |N^{+}|=|N|+|\tilde M|>|M/ B|+ |\tilde M|=|M|. \]

  Portanto~\(|N^{+}|>|M|\), uma contradição.
\end{prova}

%%   \medskip

%%   Mostremos agora a recíproca. Seja~\(M'=M\Delta P\), onde~\(P\) é o
%%   caule do botão~\(B\). Claramente~\(M'\) é um emparelhamento
%%   e~\(|M'|=|M|\). Notemos que~\(M'/ B\) é um emparelhamento
%%   máximo em~\(G/ B\).

%%   Mostremos que~\(M'\) é um emparelhamento máximo em~\(G/ B\).

%%   Suponha que não seja, então, pelo teorema de Berge, existe um
%%   caminho~\(M'\)-aumentador, digamos~\(Q\) com ambos os extremos
%%   livres, digamos~\(u\) e~\(v\).

%%   Claramente~\(Q\) intersecta~\(B\), de fato, se~\(Q\) não
%%   intersecta~\(B\), então~\(Q\subset (G/ B)\). Se trocar as arestas
%%   de~\(Q\), isto é, \(Q\Delta (M'/ B)\), então~\(Q\Delta M'/ B\) é um
%%   emparelhamento e~\(|Q\Delta M'/ B|> |M'/ B|\), o que contradiz a
%%   maximalidade de~\(M'/B\).

%%   Notemos que um dos vértices livres de~\(Q\) não pertence a~\(B\),
%%   pois~\(B\) é um botão. Suponhamos~\(u\notin B\), onde~\(u\) é um dos
%%   vértices livres de~\(Q\).

%%   Seja~\(w\) o primeiro vértice de~\(Q\) que intersecta~\(B\), e
%%   seja~\(Q'\) a secção de~\(Q\) que vai de~\(u\) a~\(w\). Então~\(Q'\)
%%   é um caminho~\(M'/ B\)-aumentador em~\(G/ B\), uma contradição.

%%   Portanto~\(M'\) é um emparelhamento máximo em~\(G/ B\), donde segue
%%   que~\(M\) é um emparelhamento em~\(G\).
%\end{prova}


\begin{observacao}
  Note que nem todo circuito ímpar hipoemparelhado é um botão. Além
  disso, se~\(C\) é um circuito ímpar, e~\(M/C\) é um emparelhamento
  máximo em~\(G/C\), então \emph{não}
  necessariamente~\M\ emparelhamento máximo em~\G; onde a contração é
  definida analogamente.
\end{observacao}

%O teorema~\ref{} sugere que uma possível estratégia para a construção de um algoritmo para encontrar emparelhamento máximos é
%Um algoritmo para encontrar um emparelhamento máximo pode buscar

O Teorema~\ref{teo:2.11} motiva o seguinte algoritmo para encontrar um
emparelhamento máximo partindo de um grafo~\G\ com emparelhamento~\M.
Buscamos passeios \M-alternantes entre vértices distintos de~\X
(conjunto dos vértices livres). Se
não existe tal passeio, o emparelhamento é máximo, pelo
Teorema~\ref{teo:berge_aumentador}.
%
Se encontramos um tal passeio~\P\ \emph{sem} flor, aplicamos o algoritmo
a~\G\ com emparelhamento~\(M'=M\sdiff P\).
%
Se encontramos um passeio \emph{com} uma flor de botão~\B, aplicamos o
algoritmo a~\(G/B\) com emparelhamento~\(M'=M/B\).

O procedimento acima atinge um grafo~\G\ com emparelhamento máximo.
%Note que~\G\ pode conter vértices que resultaram de contrações. O
%teorema~\ref{teo:2.11} garante que podemos desfazer as contrações,
%mantendo a maximalidade do emparelhamento.
Uma vez que \G\ pode conter vértices resultantes de contrações, podemos
usar o Teorema~\ref{teo:2.11} para desfazê-las preservando a maximalidade do
emparelhamento. Na próxima seção descrevemos o algoritmo que esboçamos.

\section{Algoritmo de~\Edmonds--\Gallai}\label{sec:AlgoritmoEdmonds}

Dado um emparelhamento~\M\ em~\G, desejamos encontrar um emparelhameno
maior do que~\M, ou constatar que~\M\ é máximo. No processo, rotulamos
os vértices do grafo, de modo a obter a decomposição de
\Edmonds--\Gallai.

Para encontrar o emparelhamento, fazemos uso de caminhos
alternantes. A grosso modo, partimos de um emparelhamento~\M, e
construímos uma floresta \M-alternante, a partir de algum vértice não
coberto (raiz). A árvore~``cresce'' por meio da adição de arestas do
emparelhamento. Nesse processo, rotulamos os vértices da
árvore. Quando não pudermos prosseguir, o algoritmo termina, e as
classes de vértices definidas pelos rótulos (ou sua ausência) definem
as componentes da~\mencao{decomposição
  de~\Edmonds--\Gallai}~\ref{teo:decomposicao-edmonds-gallai}.


Existem outros algoritmos para encontrar emparelhamentos
máximos.\pagenote{\input{notas/algoritmos_para_emparelhamento}}


Nosso objetivo é construir uma floresta \M-alternante \F, e rotulamos
seus vértices \vpar\ ou \vimpar. Seja \X\ o conjunto de vértices não
cobertos por \M. Inicialmente rotulamos de \vpar\ os vértices de
\X. Cada vértice de \X\ é raiz de uma das árvores de~\F. O crescimento
de \F\ é sempre feito a partir de um vértice \vpar, digamos~\vu. Temos
os seguintes casos.

\emph{Caso 1.}\quad Existe uma aresta \(uv\) onde \vv\ não está
rotulado. Rotulamos \vv\ de \vimpar\ e o seu companheiro \vw\ (\(vw\in
M\)) de~\vpar.

\emph{Caso 2.}\quad Existe uma aresta \(uv\) com \vv\ rotulado
\vpar\ tal que \vv\ pertence a uma árvore distinta da que
\vu\ pertence. Neste caso, encontramos um caminho \M-aumentador
\(R\deq P(uv)Q\), onde \(P\) é o caminho em \F\ de~\(r_u\), raiz da
árvore que contém \vu, até \vu, e \(Q\) é o caminho em \F\ de~\vv\
até~\(r_v\), raiz da árvore que contém \vv. Fazemos \(M\deq M\sdiff R\) e
repetimos o processo da construção de \F\ (do início!).

\emph{Caso 3.}\quad Existe uma aresta~\(uv\)
com~\vv\ rotulado~\vpar\ e~\vv\ pertencente à mesma árvore à qual
pertence \vu. Neste caso, temos uma uma \M-flor em~\G, com
um~\M-botão, digamos~\B\ (circuito ímpar que existe em
\(F+uv\)). Rotulamos de~\vpar\ os vértices do botão~\B,
contraímos~\B\ e consideramos o grafo~\(G/B\) com o emparelhamento
\(M/B\) nesse grafo. Continuamos o processo de expansão da floresta
\F\ resultante. (Usamos então o teorema provado anteriormente.)
Recursivamente, continuamos\ldots

\begin{fato}
  Se nenhum dos três casos ocorre, então afirmamos que encontramos um
  emparelhamento máximo~\(M'\) no grafo corrente \(G'=(V',E')\) que
  foi obtido do grafo original após zero ou mais contrações.
\end{fato}

\begin{prova}
  Considere a rotulação \vpar/\vimpar\ feita conforme a
  \hbox{\(M'\)-floresta} foi construída. Seja \(X'\deq\{\mbox{\(x\in
    V'\):\ \(x\) não é coberto por \(M'\)}\}\), e sejam
\begin{align*}
  \vpar &\digual\{v\in V':\text{ rótulo de \vv\ é \vpar}\},\\ \vimpar
  &\digual\{v\in V':\text{ rótulo de \vv\ é \vimpar}\}.
\end{align*}

Note que não há flores em~\glinha\negthinspace, e
portanto~\(|X'|=|\vpar| - |\vimpar|\). Para para todo
subconjunto~\(S\subseteq V'\) vale \(\deficiencia(G')\geq c_o (G -S)-
|S|\) e em particular, tomando \(S=\vimpar\), temos que
\[
\deficiencia(G')\geq c_o (G -\vimpar)-|\vimpar| =|\vpar|-|\vimpar|
=|X'|.
\]
Como \(M'\) \emph{não} cobre exatamente \(|X|\) vértices, segue que
\(M'\) é máximo.
\end{prova}

\begin{prova}[(Prova alternativa.)] Sabemos, pela Fórmula de Tutte--Berge,
\[
\emp(G')\le \frac{1}{2}\bigl(|V'|+|S|-c_o (G'-S)\bigr)
\]
para todo \(S\subset V'\). Tomando \(S=\vimpar\), temos
\[
\emp(G')\le \frac{1}{2}(|V'|+|\vimpar|-|\vpar|).
\]

O algoritmo descrito constrói uma floresta, composta pelo conjunto de
vértices rotulados. Como na outra demonstração, considere a rotulação
\vpar/\vimpar\ feita conforme a \(M'\)-floresta foi construída. Seja
\(X'\deq\{\mbox{\(x\in V'\):\ \(x\) não é coberto por \(M'\)}\}\), e
sejam
\begin{align*}
  \vpar &\digual\{v\in V':\text{ rótulo de \vv\ é \vpar}\},\\ \vimpar
  &\digual\{v\in V':\text{ rótulo de \vv\ é \vimpar}\}.
\end{align*}
Pela rotulação feita pelo algoritmo, o número de arestas do
emparelhamento~\(M'\) que estão fora da floresta
é~\((|V'|-(|\vimpar|+|\vpar|))/2 \), e o das que estão na floresta
é~\(|\vimpar|\). De fato, o conjunto de vértices \(X'\subseteq V'\)
expostos por \(M'\) está na floresta, e assim os vértices não
rotulados (isto é, fora da floresta) devem estar cobertos pelo
emparelhamento~\(M'\).

A quantidade de vértices fora da floresta
é~\(|V'|-(|\vimpar|+|\vpar|)\), e o número de arestas do
emparelhamento \(M'\) na floresta é igual ao número de vértices
rotulados \(\vimpar\), pois cada \(\vimpar\) é ponta de exatamente uma
aresta de \(M'\). Portanto
\[
\begin{aligned}
|M'|&\igual
\frac{1}{2}\bigl(|V'|-(|\vimpar|+|\vpar|)\bigr)+|\vimpar| \\
&\igual
\frac{1}{2}(|V'|+|\vimpar|-|\vpar|).
\end{aligned}
\]
Logo, o emparelhamento \(M'\) é máximo.
\end{prova}

Já vimos que o emparelhamento máximo em~\glinha\ corresponde a um
emparelhamento máximo no grafo~\G\ inicial, obtido segundo o
Teorema~\ref{teo:2.11} (os botões são descontraídos na ordem inversa
de sua contração).

Observamos agora como identificar a decomposição de
\Edmonds-\Gallai\ do Teorema~\ref{teo:decomposicao-edmonds-gallai}
partindo da rotulação de~\G\ fornecida pelo algoritmo. Lembramos
que~\(D_G\) é o conjunto dos vértices que são descobertos por algum
emparelhamento máximo, e~\(A_G\) é o conjunto de vizinhos de~\(D_G\)
em~\(V\setminus D_G\). Note que~\vpar\ é o conjunto dos
vértices~\vv\ tais que existe em~\G\ um caminho~\M-alternante de
comprimento par de~\X\ até~\vv. Analogamente, \vimpar\ é o conjunto de
vértices~\(v\in V(G)\setminus\vpar\) alcançáveis por um
caminho~\M-alternante de comprimento ímpar.

\begin{proposicao}
Seja~\M\ um emparelhamento máximo em~\gve, \X\ o conjunto dos vértices
não cobertos por~\M\ e sejam~\vpar\ e~\vimpar\ como acima.
Então~\(\vpar = D_G\) e~\(\vimpar=A_G\).
\end{proposicao}

Consideramos que um caminho de comprimento zero é um
caminho \M-alternante. Ou seja, \vpar\ contém o conjunto~\X.

\begin{prova}
Demonstraremos a igualdade~\(\vpar=D_G\); o fato~\(\vimpar=A_G\) é um
exercício.

\emph{(Prova de \(\vpar \subseteq D_G\).)}\quad Seja~\vv\ um vértice
em~\vpar, e seja~\P\ um caminho \M-alternante
de~\X\ para~\vv. Considere~\(M'\deq M \sdiff P\). Então~\(M'\) é um emparelhamento máximo que não
cobre~\vv. Portanto, \vv\ pertence a~\(D_G\).

\emph{(Prova de \(D_G \subseteq \vpar\).)}\quad Seja~\vv\ um vértice
em~\(D_G\). Se~\(v\) pertence a~\X\ então~\vv\ pertence
a~\vpar. Suponha que~\vv\ é coberto por~\M. Seja~\(M'\) um
emparelhamento máximo que não cobre~\vv\ (\(M'\) existe
pois~\vv\ pertence a~\(D_G\)). Os componentes de~\(M' \sdiff M\) são
circuitos ou caminhos pares com arestas alternadamente em~\(M'\) e
em~\M. Como~\vv\ é coberto por~\M, existe um caminho alternante par
que começa em~\vv, com uma aresta de~\M, e termina num vértice~\vw,
chegando por uma aresta de~\(M'\). Como~\vw\ não é coberto por~\M,
então~\vw\ pertence a~\X. Neste caso, temos um caminho \M-alternante
par que começa em~\X e termina em~\vv. Logo,~\vv\ pertence a~\vpar.
\end{prova}

Um corolário que segue imediatamente do Algoritmo
de~\Edmonds--\Gallai\ é o seguinte.

\begin{corolario}
Se~\(M\) é um emparelhamento máximo em um grafo~\(G\), então para todo
vértice~\(w\) em~\(D_G\), existe um vértice~\(v\) não-coberto por~\(M\)
e um caminho~\(M\)-alternante par de~\(v\) a~\(w\).
\end{corolario}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%                                     %%
%%  \input{aulas/02-5-emp-exercicios}  %%
%%                                     %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Exercícios}
\begin{exercicio}% exercício 1
  Seja~\(k\) um inteiro positivo, e seja~\G\ um grafo simples
  com~\(|V(G)|\geq 2k\), e tal que~\(d(v)\geq k\) para todo~\(v\in
  G\). Mostre que~\G\ tem um emparelhamento com pelo menos~\(k\)
  arestas.
\end{exercicio}

\begin{exercicio}% exercício 2
  Prove que todo grafo bipartido com pelo menos uma aresta tem um
  emparelhamento que cobre todos os vértices de grau máximo.
\end{exercicio}

\begin{observacao}
  Note que uma consequência imediata desse resultado é o fato de que o
  conjunto das arestas e um grafo bipartido pode ser particionado
  em~\(\Delta(G)\) emparelhamentos. (Ou seja, o índice cromático de um
  grafo bipartido é precisamente~\(\Delta(G)\)).
\end{observacao}

\begin{exercicio}\label{ex:emparelhamento-maximo-e-maximal}% exercício 3
  Seja~\(E\) um emparelhamento maximal e~\(E^\star\) um emparelhamento
  máximo num grafo. Mostre que~\(|E|\geq |E^\star|/2\).
\end{exercicio}

\begin{exercicio}\label{ex:hall-from-tutte}% exercício 4
  Prove o Teorema de~Hall usando o Teorema
  de Tutte~(Teorema~\ref{teo:tutte-emp}).
\end{exercicio}

\begin{exercicio}% exercício 5
  Prove que se~\(G=(V,E)\) é um grafo com~\(|V|\) par e tal que
  \[
  |\adj(X)| \geq \min \left\{ |V|, \frac{4}{3}|X| -\frac{2}{3}
  \right\}\quad \text{para todo~\(X\subseteq V,\)}
  \]
então~\G\ tem um emparelhamento perfeito.
\end{exercicio}

\printpagenotes*
\bibliographystyle{alpha}
\bibliography{bibliografia}

%% Local Variables:
%% mode: latex
%% coding: utf-8
%% fill-column: 70
%% tab-width: 2
%% End:
