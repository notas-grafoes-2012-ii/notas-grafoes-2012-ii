\section{Introdução}

\emph{Teoria de Ramsey}: ramo da Combinatória que trata do estudo de estruturas que são preservadas sob partição.

\subsection{Exemplos de resultados nesta teoria}

\begin{teorema}[Teorema de van der Waerden, 1927]
	Para qualquer partição do conjunto dos números inteiros em um número finito de classes,
	sempre existe uma classe que contém uma progressão aritmética de comprimento arbitrariamente longo.
\end{teorema}

\begin{teorema}[Teorema A]\label{teorema_A}
	Se \(G\) é um grafo completo com \(6\) vértices, 
	então para qualquer partição do conjunto de suas arestas em duas classes, 
	sempre existe uma classe contendo \(3\) arestas que determinam um triângulo.
\end{teorema}

Teoria de Ramsey: assim chamada em homenagem a Frank Ramsey [1903 - 1930], 
lógico inglês, cujo trabalho em Teoria dos Conjuntos inspirou esta linha de investigação.

\underline{Outras formulações (equivalentes) do Teorema \ref{teorema_A}}:
\begin{enumerate}
	\item	"Numa festa com \(6\) pessoas sempre existem \(3\) pessoas que se conhecem 
			duas a duas ou existem \(3\) que se desconhecem duas a duas."
	
	\item	Para qualquer coloração, em vermelho e azul, das arestas de um grafo completo \(G\)
			com \(6\) vértices, este contém um triÂngulo vermelho ou um triângulo azul.
	
	\item	\underline{Teorema}\label{teo:7.1} Todo grafo com \(6\) vértices contém uma cópia de \(K_3\)
			ou uma cópia de \(\overline{K_3}\).
\end{enumerate}

\begin{observacao}\label{obs:7.1}
	O teorema \ref{teo:7.1} não vale para grafos com menos do que \(6\) vértices 
	(tomar, por exemplo, o \(C_5\)).
	
	Teorema \ref{teo:7.1}: caso especial de um teorema provado por Ramsey
	(será visto mais adiante).
\end{observacao}

\section{Alguns resultados sobre generalização do teorema~\ref{teo:7.1}}

\begin{observacao}
	Todos os naturais aqui considerados são positivos.
\end{observacao}

Dados naturais \(m,n\) denote por 
\(r(m,n)\) o menor natural \(p\) tal que qualquer grafo de ordem \(p\)
contém uma cópia de \(K_m\) ou uma cópia de \(\overline{K_n}\).

\begin{enumerate}
	\item	\(r(3,3) = 6\)	\qquad (Teorema \ref{teo:7.1} + Observação \ref{obs:7.1}
	\item	\(r(1,n) = 1\) \qquad Trivial
	\item	\(r(2,n) = n\) \qquad Trivial
\end{enumerate}

\textit{Pergunta}: "\(r(m,n)\) está bem definida?"

Resposta:

\begin{teorema}[\Erdos\ \& \Szekeres, 35] \label{teo:7.2}
	Para quaisquer naturais \(m\) e \(n\), \(r(m,n)\) existe e 
	\[
		r(m,n) \leq \binom{m+n-2}{m-1}
	\]
\end{teorema}

\begin{corolario}
	Para naturais \(m \geq 2\) e \(n \geq 2\),
	\[
		r(m,n) \leq r(m-1,n) + r(m,n-1).
	\]
\end{corolario}

\begin{center}
	\begin{tabular}{|c|c|c|c|c|c|c|c|c|c|}
		\hline
			&	3	&	4	&	5	&	6		&	7		&	8		&	9		\\ \hline
		3	&	6	&	9	&	14	&	18		&	23		&	28		&	36		\\ \hline
		4	&	9	&	18	&	25	&	35--41	&	42--61	&	53--84	&	69--115	\\ \hline
	\end{tabular}
	Valores de \(r(m,n)\).
\end{center}

Exemplo de um método para se estabelecer limites inferiores.

\begin{teorema}[\Erdos, 47]
	Para todo natural \(n \geq 3\), \(r(n,n) > \left\lfloor 2^{n/2} \right\rfloor\).
\end{teorema}


\subsection*{Variações do problema}

Substituir subgrafos completos por outros tipos de subgrafos (Árvores, circuitos, caminhos, bipartidos completos,...)

\(r(G_1,G_2)\) denota o menor natural \(p\) tal que qualquer grafo \(G\)
de ordem \(p\) contém \(G_1\) ou o seu complemento \(G\) contém \(G_2\).

Assim, \(r(m,n) = r(K_m,K_n)\).

Um dos resultados mais conhecidos deste tipo é o seguinte

\begin{teorema}[\Chvatal,77]\label{teo:7.5}
	Seja \(T_m\) uma árvore qualquer de ordem \(m\geq 1\) e seja
	\(n\) um natural não nulo.
	Então
	\[
		r(T_m,K_n) = 1 + (m-1)(n-1)
	\]
\end{teorema}

\begin{exercicio}
	Fazer uma outra prova do Teorema \ref{teo:7.5}, 
	usando a linguagem de \(2\)-coloração de \(K_{(m-1)(n-1)+1}\)
	e fazendo indução em \(m+n\).
\end{exercicio}

Outros resultados:

\begin{teorema}[Lawrence,73]\label{teo:7.6}
	\[
		r(C_m,K_{1,n}) = 
		\begin{cases}
			2n+1	&	\text{se } m \text{ é ímpar e }m \leq 2m+1		\\
			m	&	\text{se } m \geq 2n
		\end{cases}
	\]
\end{teorema}

\begin{teorema}[\Chvatal\ \& \Harary, 72]\label{teo:7.7}
	Para qualquer grafo \(G\) de ordem \(m\) e sem vértices isolados,
	\[
		r(G,P_3) = 
		\begin{cases}
			m+1	&	\text{se }	\overline{G}	 \text{ tem um emparelhamento perfeito},	\\
			m	&	\text{caso contrário}
		\end{cases}
	\]
\end{teorema}

\begin{teorema}[\Chvatal\ \& \Gyarfas,67]\label{teo:7.8}
	Para naturais \(m,n\) com \(2 \leq m \leq n\)
	\[
		r(P_m,P_n) = n + \left\lfloor\frac{m}{2} \right\rfloor -1
	\]
\end{teorema}

\begin{teorema}[\Faudree\ \& \Schelp, 74] \label{teo:7.9}
	Sejam \(m,n\) naturais tais que \(3 \leq m \leq n\).
	\begin{itemize}
		\item[(a)]	Se \(m\) é ímpar e \((m,n) \neq (3,3)\), então
					\[
						r(C_m,C_n) = 2n -1.
					\]
		\item[(b)]	Se \(m\) e \(n\) são pares e \((m,n) \neq (4,4)\), então
					\[
						r(C_m,C_n) = n + \frac{m}{2} - 1
					\]
		\item[(c)]	Se \(n\) é ímpar e \(m\) é par, então
					\[
						r(C_m,C_n) = \max\{n+\frac{m}{2}-1,2m-1\}
					\]
		\item[(d)]	\(r(C_3,C_3) = r(C_4,C_4) = 6\).
	\end{itemize}
\end{teorema}
