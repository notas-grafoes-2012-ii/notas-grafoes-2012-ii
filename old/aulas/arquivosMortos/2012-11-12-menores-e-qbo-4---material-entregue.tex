% página 9.4

\section{O 'minor theorem' para grafos com largura arbórea limitada}

Grafos com largura arbórea (la) limitada são suficientemente 'semelhantes' a árvores
tornando possível adaptar a prova do Teorema de Kruskal (Teorema \ref{teo:9.4})
%TODO a label teo:9.4 não existe
para a classe desses grafos.
Grosseiramente, a ideia é iterar o argumento da 'sequência minimal ruim' \(la(G)\) vezes.
A prova desse resultado encontra-se no artigo 'graph minors I", de Robertson \& Seymour, 1990.

\begin{teorema}\label{teo:9.5}[Robertson \& Seymour, 1990]
	Para todo \(k > 0\) (inteiro), os grafos com largura arbórea menor que \(k\)
	são quase-bem-ordenados pela relação de \textit{menor}.
\end{teorema}

Este teorema nos leva um passo adiante em relação à prova do 'minor theorem' (para grafos arbitrários).
Para chegar a esse resultado geral, a seguinte questão é de interesse.

\textit{Pergunta}: Como são os grafos com \textit{largura arbórea limitada}?

Mais precisamente, há algum fenômeno estrutural que ocorre num grafo se e somente se
ele tem \(la\) grande?
O próximo teorema responde essa questão.

\subsection{Obstruções canônicas à largura arbórea pequena}

\begin{definicao}
	Dizemos que \(X \subseteq V(G)\) é um \textit{subconjunto conexo} se \(G[X]\) é conexo.
\end{definicao}

%página 9.5

\begin{definicao}
	Seja \(\mathcal{C}\) um conjunto de subconjuntos conexos de \(V(G)\).
	
	Dizemos que um conjunto \(U \subseteq V(G)\) cobre \(\mathcal{C}\) se \(U\)
	intersecta cada conjunto em \(\mathcal{C}\).
	
	Dizemos que dois elementos de \(\mathcal{C}\) se \textit{tocam} ('touch') se 
	eles se intersectam ou \(G\) contém uma aresta entre eles.
\end{definicao}

\begin{exercicio}
	Se \(\mathcal{C}\) é um conjunto de subconjuntos conexos de \(V(G)\)
	e quaisquer dois elementos de \(\mathcal{C}\) se tocam, então em toda decomposição
	arbórea \((T,\mathcal{W})\) de \(G\) existe \(t\in T\) tal que \(W_t\) cobre \(\mathcal{C}\).
	[Sugestão: imitar a prova da proposição \ref{prop:9.4}]
%TODO a label prop:9.4 não existe
\end{exercicio}

Esse resultado prova o lado fácil (\(\Leftarrow\)) da seguinte caracterização de grafos com largura arbórea
maior ou igual a \(k\).

\begin{teorema}\label{teo:9.6}[Seymour \& Thomas, 1993]
	Seja \(k\) um inteiro.
	Um grafo \(G\) tem \(la(G) \geq k\) se e somente se \(g\) contém uma coleção de conjuntos de vértices 
	conexos que se tocam dois a dois e que não podem ser cobertos por até \(k\) vértices.
\end{teorema}

%página 9.6

\textit{Interesse}: 
Decomposição arbórea que dê mais informações sobre a estrutura arbórea do grafo
(quão semelhante de uma árvore é o grafo).
Exigir apenas largura arbórea pequena não captura como bolsas da decomposição
se relacionam ao longo da árvore (da decomposição).

% todo - desenho e diagrama da página 9.6

\begin{definicao}
	Dizemos que uma decomposição arbórea \((T, \mathcal{W})\) de \(G\) é \textit{'linked'} ou \textit{'lean'}
	(interligada ou enxuta) se satisfaz a seguinte condição:
	Dado qualquer \(s \in \mathbb{N}\) e \(t_1,t_2 \in T\), \textit{ou} \(G\) contém \(s\) caminhos disjuntos de \(W_{t_1}\) a \(W_{t_2}\);
	\textit{ou} existe \(t\) no caminho de \(t_1\) para \(t_2\) em \(T\) tal que \(|W_t| < s\).
\end{definicao}

% todo - fazer desenhos página 9.6

É possível encontrar decomposições arbóreas com largura \(la\) e que são também lean:

\begin{teorema}\label{teo:9.7}[Thomas,1990]
	Todo grafo \(G\) tem uma decomposição arbórea lean (enxuta) com largura \(la(G)\).
\end{teorema}

% página 9.7

\section{Decomposições arbóreas e menores proibidos}

Seja \(\mathcal{X}\) um conjunto ou uma classe de grafos.
Considere a classe
\[
	\Forb_\leq (\mathcal{X}) 
	\quad := \quad
	\{ G \, :\, G \not\geq X \text{ para todo } X \in \mathcal{X}\}
\]
classe dos grafos sem um menor em \(\mathcal{X}\).

\(\Forb_\leq(\mathcal{X})\) é uma \textit{propriedade de grafos}, isto é, é fechada sob isomorfismo.
[ \(H \in \mathcal{F}\) e \(H \equiv H' \Rightarrow H' \in \mathcal{F}\)]

Escrevemos \(\Forb_\leq(X)\) em vez de \(\Forb_\leq(\{X\})\).

A classe \(\Forb_\leq(\mathcal{X})\) é expressa especificando-se os grafos \(X \in \mathcal{X}\) como \textit{menores proibidos}
('forbidden' ou 'excluded minors').

\begin{exemplo}
	Se \(\mathcal{X} = \{K_5,K_{3,3}\}\), então \(\Forb_\leq (\mathcal{X}) =\) classe dos \textit{grafos planares}.
	
	Se \(\mathcal{X} = \{K_4\}\), então \(\Forb_\leq (\mathcal{X}) =\) classe dos grafos \textit{série-paralelos}.
	
	Se \(\mathcal{X} = \{K_{2,3},K_4\}\), então \(\Forb_\leq(\mathcal{X}) = \) classe dos grafos \textit{exoplanares}.
\end{exemplo}

\(\Forb_\leq(\mathcal{X})\) é \textit{fechada} sob a operação de tomar menores, isto é, se \(G\in \Forb_\leq (\mathcal{X})\)
e \(H \leq G\), então \(H \in \Forb_\leq (\mathcal{X})\).

% página 9.8

Propriedades de grafos que são \textit{fechadas sob tomada de menores} serão chamadas \textit{hereditárias}.

Toda propriedade hereditária, por sua vez, pode ser expressa através de menores proibidos.

\begin{proposicao}\label{prop:9.8}
	Seja \(\mathcal{P}\) uma propriedade de grafos.
	\(\mathcal{P}\) pode ser expressa através de menores proibidos se e somente se
	\(\mathcal{P}\) é hereditária.
\end{proposicao}

\((\Leftarrow) \mathcal{P} = \Forb_\leq(\overline{\mathcal{P}})\), onde \(\overline{\mathcal{P}}\)
é o complemento de \(\mathcal{P}\).

\textit{Pergunta}:
Como representar melhor uma propriedade hereditária através de menores proibidos?

Resposta mais adiante.

Por ora, considere a propriedade hereditária: largura arbórea limitada ('bounded tree width').

\begin{proposicao}\label{prop:9.9}
	Para todo \(k > 0\), a propriedade de ter \(la < k\) é hereditária.
\end{proposicao}

\begin{prova}
	É suficiente provar que a largura arbórea de um grafo não aumenta quando contraímos uma aresta.
	O resultado segue da proposição \ref{prop:8.0} e da propriedade de menores.
\end{prova}

% página 9.9

Pelas proposições anteriores, a propriedade "ter largura arbórea menor que \(k\)" pode 
ser expressa por menores proibidos.

Seja \(\mathcal{G}_{<k} =\) classe dos grafos com largura arbórea menor que \(k\).

\(\mathcal{G}_{<k} = \Forb_\leq (\mathcal{X})\) \(\leftarrow\) quem é?

\(k = 2\) \(\mathcal{G}_{< 2} = \) grafos com largura arbórea menor que \(2\), florestas \(= \Forb_\leq (K_3)\).

\(k = 3\) \(\mathcal{G}_{< 3} = \) grafos com largura arbórea menor que \(3\) = séries-paralelos \(= \Forb_\leq (K_4)\).

Se \(X = K_3\) ou \(X = K_4\), grafos em \(\Forb_\leq (X)\) tem largura arbórea menor que \(3\).

\textit{Pergunta}:
Há outros grafos \(X\) para os quais os grafos em \(\Forb_\leq(X)\) têm largura arbórea limitada?
Como são tais grafos?

\textit{Resposta}:
O grafo \(X\) deve ser \textit{planar}.

\textit{Justificativa}:
(Porque os planares devem ser excluídos)

\(G_{n\times n} = \) grade \(n \times n\).

\(G_{n\times n}\) é planar.
Vimos (exercício) que \(la(G_{n\times n}) =  n\).
\(la_{n\rightarrow \infty}(G_{n\times n}) \rightarrow \infty\).

\(X\) não planar \(\Rightarrow\) \(\Forb_\leq (X)\) inclui os planares.
\(G_{n\times n} \in \Forb_\leq (X)\)
A largura dos grafos em \(\Forb_\leq (X)\) não é limitada.

% página 9.10

\begin{teorema}\label{teo:9.A}[Robertson, Seymour \& Thomas, 94]
	Para todo grafo \textit{planar} \(H\) existe um inteiro \(n\) tal que a grade \(G_{n \times n}\) tem 
	um menor isomorfo a \(H\) (\(H\)-menor).
\end{teorema}

\begin{teorema}\label{teo:9.B}
	Para todo grafo \textit{planar} \(H\) existe um inteiro \(k\) tal que se um grafo \(G\) 
	tem largura arbórea maior ou igual a \(k\), então \(G\) tem um \(H\)-menor.
\end{teorema}

\begin{corolario}\label{cor:9.C}
	\(X\) é planar \(\Rightarrow\) grafos em \(\Forb_\leq(X)\) tem largura arbórea limitada.
\end{corolario}

\begin{teorema}\label{teo:9.10}[Robertson \& Seymour]
	Grafos em \(\Forb_\leq (X)\) tem largura arbórea limitada \(\Leftrightarrow\) \(X\) é \textit{planar}.
\end{teorema}

Tem um resultado anárlogo:
troca-se \(la\) por \(pw\) (path-width) e \textit{planaridade} por \textit{aciclicidade}.

\begin{teorema}\label{teo:9.11}[Robertson \& Seymour,83]
	Grafos em \(\Forb_\leq (X)\) tem \(pw\) limitada
	\(\Leftrightarrow\)
	\(X\) é uma \textit{floresta}.
\end{teorema}
