%TODO: decidir se vamos manter o parágrafo de objetivo de capítulo
\emph{Objetivo}: ``Provar'' (dar uma idéia da prova) o ``minor theorem'' [Robsertson
  \& Seymour]: ``Em todo conjunto infinito de grafos existem dois tais que um é menor
do outro.''

\section{Relações}
Uma relação~\(R\) sobre um conjunto~\(X\) é um subconjunto de~\(A\times A\). Para
enfatizar que estamos tratando de relações e não simplesmente de conjuntos, usamos a
notação infixa~\(xRy\) para denotar~\((x,y)\in R\).

Fixada uma relação~\(R\) sobre um conjunto~\(X\), dizemos que~\(R\) é
\begin{itemize}
\item \defi{Reflexiva}, se~\(xRx\) para todo~\(x\in X\);
\item \defi{Transitiva}, se~\(xRy\) e~\(yRz\) implicam~\(xRz\) para todos~\(x,y,z\in
  X\);
\item \defi{Antissimétrica}, se~\(xRy\) e~\(yRx\) implicam~\(x = y\), para
  todos~\(x,y\in X\);
\item \defi{Total}, se para todos~\(x,y\in X\), temos~\(xRy\) ou~\(yRx\);
\item \defi{Bem-fundada}, se para todo subconjunto não-vazio~\(Y\) de~\(X\), existe
  um~\(y\in Y\) tal que nenhum~\(z\in Y\) satisfaz~\(zRy\) e~\(\neg(yRz)\).
\end{itemize}

Em particular, uma relação é chamada de
\begin{itemize}
\item \defi{Quase-ordem}, se for reflexiva e transitiva;
\item \defi{Ordem parcial}, se for reflexiva, transitiva e antissimétrica;
\item \defi{Ordem total} (ou \defi{ordem}), se for reflexiva, transitiva,
  antissimétrica e total;
\item \defi{Quase-boa-ordem}, se for reflexiva, transitiva e bem-fundadada;
\item \defi{Boa-ordem parcial}, se for reflexiva, transitiva, antissimétrica e
  bem-fundada;
\item \defi{Boa-ordem total} (ou \defi{boa-ordem}), se for reflexiva, transitiva,
  antissimétrica, total e bem-fundada.
\end{itemize}

Estamos particularmente interessados em estudar quase-boas-ordens e, nesse contexto,
é mais simples trabalhar com uma noção equivalente (sob o axioma da escolha) de
relações bem-fundadas:

\begin{proposicao}
Assumindo o axioma da escolha, uma relação~\(R\) sobre um conjunto~\(X\) é
bem-fundada se e somente se para toda sequência infinita
enumerável~\((x_i)_{i\in\NN}\), existem indíces~\(i < j\) tais que~\(x_i R x_j\).
\end{proposicao}

Dada uma relação~\(R\) sobre um conjunto~\(X\), um subconjunto~\(A\subset X\) é dito
uma~\defi{anticadeia} se não existem~\(x,y\in A\) distintos tais que~\(xRy\).


%\section{Quase-boa-ordem (q.b.o) - `well-quasi-ordering'}
%
%\begin{definicao}
%	Uma relação \emph{reflexiva} e \emph{transitiva} é chamada uma
%    \emph{quase-ordem}.
%\end{definicao}
%
%\begin{definicao}
%	Uma quase ordem~\(\leq\) que é \emph{anti-simétrica} (\(x \leq y \leq x
%    \Rightarrow x = y\) é uma \emph{ordem parcial}.
%\end{definicao}
%
%\begin{definicao}
%	Uma quase ordem~\(\leq\) sobre um conjunto infinito~\(X\) é uma
%    \emph{quase-boa-ordem} se para \emph{toda} sequência infinita~\(x_0, x_1,
%    \ldots\) em~\(X\) existem índices~\(i < j\) tais que~\(x_i \leq x_j\).  Dizemos
%    que os elementos de~\(X\) são \emph{quase-bem-ordenados}, e que~\((X,\leq)\) é
%    \emph{quase-bem-ordenado}.
%	
%	Um par~\((x_i,x_j)\) onde~\(i<j\) e~\(x_i \leq x_j\) é chamado um \emph{par bom}.
%	Uma sequência que contém um par bom é chamada uma \emph{sequência boa}.
%	
%	Assim, uma quase ordem sobre~\(X\) é uma quase-boa-ordem se e somente se toda
%    sequência infinita em~\(X\) é boa.
%	
%	Uma sequência infinita é \emph{ruim} se não é boa.
%\end{definicao}
%
%\begin{definicao}
%	Uma \emph{anticadeia} (`antichain') em~\(X\) é um conjunto~\(Y \subseteq X\)
%    tal que quais quer dois de seus elementos são incomparáveis.
%\end{definicao}

Usaremos frequentemente o símbolo~\(\leq\) no estudo de relações (tipicamente
quase-ordens) e assumiremos sempre que as relações~\(<\), \(\geq\) e~\(>\) estão
definidas como:
\begin{itemize}
\item Para todos~\(x,y\), temos~\(x < y\) se e somente se~\(x \leq y\) e~\(\neg(y\leq
  x)\);
\item Para todos~\(x,y\), temos~\(x \geq y\) se e somente se~\(y \leq x\);
\item Para todos~\(x,y\), temos~\(x > y\) se e somente se~\(x \geq y\) e~\(\neg(y\geq
  x)\);
\end{itemize}

Supondo que~\(\leq\) é uma quase-ordem, uma sequência~\((x_i)_{i\in U}\) indexada
por~\(U\subset\NN\) é dita
\begin{itemize}
\item \defi{Crescente}, se para todos índices~\(i < j\), temos~\(x_i \leq x_j\);
\item \defi{Decrescente}, se para todos índices~\(i < j\), temos~\(x_j \geq x_i\);
\item \defi{Estritamente crescente}, se para todos índices~\(i < j\), temos~\(x_i <
  x_j\);
\item \defi{Estritamente decrescente}, se para todos índices~\(i < j\), temos~\(x_j >
  x_i\).
\end{itemize}



\begin{proposicao}\label{prop:9.1}
  Seja~\(\leq\) uma quase-ordem sobre~\(X\). Então~\(\leq\) é quase-boa-ordem se e
  somente se~\(X\) não contém nem uma anticadeia infinita nem uma sequência
  infinita estritamente decrescente~\(x_0 > x_1 > \cdots\).
\end{proposicao}

\begin{prova}
  Observe que se~\(A\) é uma anticadeia infinita, então qualquer
  sequência~\((x_i)_{i\in\NN}\) em~\(A\) de elementos distintos é tal que não
  existem~\(i < j\) com~\(x_i \leq x_j\).

  Por outro lado, se~\((x_i)_{i\in\NN}\) é uma sequência infinita estritamente
  decrescente, então também temos que não existem~\(i < j\) com~\(x_i \leq x_j\).

  Portanto, se~\(\leq\) é quase-boa-ordem, então~\(X\) não contém nem uma anticadeia
  infinita nem uma sequência infinita estritamente decrescente.

  \medskip
	
  Seja~\(x_0, x_1, \ldots, \) uma sequência qualquer de~\(X\) indexada pelos naturais.
  Considere o grafo completo (infinito)~\(K_{\NN}\)
	
	Faça uma coloração das arestas~\(ij\) de~\(K_{\NN}\), com~\(i < j\) com~\(3\) cores:
	\begin{itemize}
		\item Atribua a cor \emph{verde} se~\(x_i \leq x_j\);
		\item Atribua a cor \emph{amarela} se~\(x_i\) e~\(x_j\) são incomparáveis;
		\item Atribua a cor \emph{vermelha} se~\(x_i > x_j\).
	\end{itemize}
	Pelo teorema de Ramsey, sabemos que~\(K_{\NN}\) tem um subgrafo completo~\(H\)
    infinito cujas arestas são todas da mesma cor.  Pela hipótese da proposição, tais
    arestas não podem ser nem amarelas e nem vermelhas.

    Portanto, são verdes. Logo, quaisquer dois vértices~\(i,j\) (com~\(i < j\))
    de~\(H\) são tais que~\(x_i \leq x_j\) (bastava uma tal aresta).

    Concluímos que~\((X,\leq)\) é uma quase-boa-ordem.
\end{prova}
%DONE prova nas notas

\begin{corolario}
  Se~\(\leq\) é quase-boa-ordem, então toda sequência infinita em~\(X\) tem uma
  subsequência infinita crescente.
\end{corolario}

Uma quase-ordem~\(\leq\) sobre~\(X\) induz uma quase-ordem natural sobre o conjunto
dos subconjuntos finitos de~\(X\) (denotado por~\([X]^{<\omega}\)). Essa quase-ordem é
definida abaixo.

Para conjuntos finitos~\(A,B \subseteq X\), fazemos~\(A \leq B\) se existe uma função
injetora~\(f\:A\rightarrow B\) tal que~\(a \leq f(a)\) para todo~\(a \in A\).

O seguinte lema e a ideia de sua prova têm um papel importante na teoria da
quase-boa-ordem.

\begin{lema}\label{lema:9.3}
  Se~\(\leq\) é quase-boa-ordem sobre~\(X\), então~\(\leq\) é quase-boa-ordem
  sobre~\([X]^{<\omega}\).
\end{lema}

%TODO prova nas notas
%TODO escrever página 9.1,9.2


% pagina 9.3

Seja~\(\mathcal{G}\) o conjunto de todos os grafos \emph{finitos}
e~\(\leq\) a relação de `menor (de)' definita sobre~\(\mathcal{G}\).

O `minor theorem' afirma que 
\[
	``(\mathcal{G},\leq) \emph{ é quase-boa-ordem}''
\]
Como obviamente uma sequência estritamente decrescente de menores não pode ser
infinita, pela proposição \ref{prop:9.1}, o `minor theorem' é equivalente a sequinte
afirmação:
%DONE verificar se a referência acima está correta, foi corrigida de prop:91

\begin{center}
  ``não existe uma anticadeia infinita em~\(\mathcal{G}\)''
\end{center}
(isto é, não existe um conjunto infinito de grafos em~\(\mathcal{G}\), 
dois-a-dois incomparáveis quanto à relação menor~\(\leq\).)

Provaremos uma versão mais forte do `minor theorem' para árvores:

\begin{teorema}
	Seja~\(\mathcal{T}\) o conjunto de todas as árvores \emph{finitas}
	e~\(\leq\) a relação de menor topológico (\(\leq \equiv \leq_t\)).
	Então~\((\mathcal{T},\leq)\) é quase-boa-ordem.
\end{teorema}

A prova será baseada numa relação mais forte definita sobre árvores enraizadas.
\begin{itemize}
	\item Se~\(T\) é uma árvore e~\(r\) um vértice qualquer fixo,
	chamaremos o par~\((T,r)\) de \emph{árvore enraizada}, e~\(r\) sua \emph{raiz}.
	(algumas vezes escrevemos simplesmente~\(T\), em vez de~\((T,r)\).)
\end{itemize}
