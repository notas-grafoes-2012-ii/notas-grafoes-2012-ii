%\begin{resumo-aula}{3}{10}{2012}
%Introdução a problemas extremais, duas provas do teorema de \Turan (uma de \Turan, outra de \Zykov).
%\end{resumo-aula}

\section{Introdução}
Por problemas extremais, entendemos perguntas como as dos exemplos abaixo.
	
\begin{itemize}
\item[(P1)] Determine o número mínimo de arestas~\(\phi(n)\), tal que
  todo grafo~\(G\) de ordem~\(n\) e com pelo menos~\(\phi(n)\) arestas
  tem um circuito.
					
\item[(P2)] Determine o menor natural~\(\delta(n)\) tal que todo grafo
  de ordem~\(n\) e grau mínimo pelo menos~\(\delta(n)\) tem um
  circuito hamiltoniano.
					
\item[(P3)] Determine o menor natural~\(n\) tal que todo grafo de
  ordem pelo menos~\(n\) tem um~\(K_3\) ou~\(\overline{K_3}\) como
  subgrafo induzido.
					
\item[(P4)] Determine o número máximo de arestas
  em um grafo de ordem~\(n\) que não contém~\(K_3\).
\end{itemize}
	
Em particular, as respostas para as perguntas acima são:
	
\begin{itemize}
\item[(P1)]	Temos~\(\phi(n) = n\).
		
\item[(P2)]	O Teorema de Dirac afirma que
\begin{align*}
\delta(n) = \left\lceil\frac{n}{2}\right\rceil.
\end{align*}
		
\item[(P3)]	O Teorema de Ramsey (para o caso~\((3,3)\)) afirma que~\(n = 6\).
		
\item[(P4)] Veremos mais adiante que tal número
  é~\(\left\lceil\frac{n}{2}\right\rceil\left\lfloor\frac{n}{2}\right\rfloor\).
\end{itemize}

Tipicamente, dada uma propriedade~\(P\) de grafos, e um
invariante~\(I\) e uma classe~\(\mathcal{G}\) de grafos, procura-se
determinar o menor valor~\(m\) tal que todo grafo~\(G \in
\mathcal{G}\) com~\(I(G) > m\) tem a propriedade~\(P\).
	

\section{Problema da proibição de um dado grafo}
%\section{Problema da proibição de um grafo~\texorpdfstring{$G$}{G}}
%O comando acima coloca $G$ no título da seção, mas G no PDF bookmark
Dado um grafo~\(G\), definimos, para todo natural~\(n\) o
número
\begin{align*}
\ex(n,G) = \sup\{|E(H)| : H \text{ é um grafo de ordem } n \text{ com }
H \not\supset G\}.
\end{align*}
Neste caso,~\(G\) é chamado \defi{grafo proibido}.

Dizemos também que um grafo~\(H\) de ordem~\(n\) é um \defi{grafo
  extremal} (relativo a~\(G\)) se~\(H\) tem~\(\ex(n,G)\) arestas e~\(H
\not\supseteq G\) e denotamos o conjunto de todos os grafos extremais
de ordem~\(n\) por~\(\EX(n,G)\). 

Por exemplo, \(\ex(n,K_3) =
\left\lceil\frac{n}{2}\right\rceil\left\lfloor\frac{n}{2}\right\rfloor\).
O grafo $H=
K_{\left\lceil\frac{n}{2}\right\rceil\left\lfloor\frac{n}{2}\right\rfloor}$
é um grafo extremal para \(\mathcal{P}(K_3)\).

Chamamos de~\(\mathcal{P}(G)\) o problema de determinar o
número~~\(\ex(n,G)\).

Para resolver \(\mathcal{P}(G)\), precisamos:

\begin{itemize}
\item[(a)] Exibir um grafo extremal;
\item[(b)] Provar que todo grafo com $n$ vértices e pelo menos
  \(\ex(n,G) + 1\) arestas contém $G$ como subgrafo.
\end{itemize}


	
\subsection{Proibindo grafos completos}
\Turan, em~1941, investigou o problema~\(\mathcal{P}(K_p)\).

Para simplificar, considere~\(G \equiv K_{p+1}\). Claramente, os
grafos~\(p\)-partidos completos~\(K_{n_1,\cdots,n_p}\) de ordem~\(n\)
(ou seja, temos~\(\sum_i n_i = n\)) não contêm~\(K_{p+1}\). Dentre
todos os grafos desse tipo, o que tem o maior número possível de
arestas é aquele que tem os blocos das partições o mais balanceado
possível.

Ou seja, tomando~\(r = n\bmod p\) e~\(k = \lfloor n/p \rfloor\),
o conjunto~\(\EX(n,K_{p+1})\) contém o grafo~\(p\)-partido completo com~\(p-r\)
blocos com~\(k\) vértices e~\(r\) blocos com~\(k+1\) vértices.
	
Chamamos tal grafo de Grafo de~\Turan\ e o denotamos por~\(T_{n,p}\)
(extremal que não contém~\(K_{p+1}\)). Definimos também~\(t_{n,p} =
|E(T_{n,p})|\).
	
Observando que 
\[
|E(\overline{T_{n,p}})| = (p-r)\binom{k}{2} + r\binom{k+1}{2},
\]
obtemos a seguinte fórmula
\[
|E(T_{n,p})| = \binom{n}{2} - \left( (p-r) \binom{k}{2} +
r\binom{k+1}{2} \right) = \binom{n}{2} - \frac{k(n-p+r)}{2}.
\]


\bigskip
\bigskip
\bigskip
\bigskip
\bigskip
\bigskip

O seguinte teorema é um resultado clássico sobre problemas
extremais. É interessante conhecer pelo menos uma prova deste
resultado.

\bigskip

%DONE teo:6.1 também é definido em extremais-2.tex
\begin{teorema}[\Turan, 1941]\label{teo:6.1}
	Dentre todo os grafos de ordem~\(n\) que não contêm~\(K_{p+1}\),
  existe exatamente um com o número máximo de arestas, sendo esse
  o~\(T_{n,p}\).
\end{teorema}
	
\begin{prova} (Técnica de ``chopping'')
	Provaremos por indução em~\(n\).
		
  Observe que para~\(n \leq p\) o resultado é trivial.
		
	Suponha então que~\(n > p\) e que o resultado é válido
  para~\(n-1\).

  Seja~\(G\) um grafo extremal de ordem~\(n\) (em relação
  a~\(K_{p+1}\)). Como a adição de qualquer aresta a~\(G\) cria uma cópia
  de~\(K_{p+1}\), sabemos que existe uma cópia~\(H\) de~\(K_p\)
  em~\(G\).

  Sejam~\(q_1 = |E(H)| = \binom{p}{2}\), \(q_2 = |\{vw \in E(G) :
  v\in V(G)\setminus V(H), w\in V(H)\}|\) e~\(q_3 = |E(G-V(H))|\).

  Como cada vértice de~\(V(G)\setminus V(H)\) é adjacente a no
  máximo~\(p-1\) vértices de~\(H\), temos~\(q_2 \leq (n-p)(p-1)\).

  Além disso, pela hipótese de indução, sabemos que~\(q_3 \leq
  t_{n-p,p}\), pois~\(G - V(H)\) não contém uma cópia de~\(K_{p+1}\).

  Tomando~\(r = n \bmod p\) e~\(k = \lfloor n/p \rfloor\), temos que
  \begin{align*}
    |E(G)| & = q_1 + q_2 + q_3 \leq \binom{p}{2} + (n-p)(p-1) +
    t_{n-p,p} \\
    & = \binom{p}{2} + (n-p)(p-1) + \left(\binom{n-p}{2} -
    \left\lfloor \frac{n-p}{p} \right\rfloor
    \frac{(n-p-p+r)}{2}\right) \\
    & = \binom{p}{2} + (n-p)(p-1) + \frac{(n-p)(n-p-1)}{2} -
    \frac{(k-1)(n-2p+r)}{2} \\
    & = \binom{p}{2} + \frac{(n-p)(n+p-3)}{2} - \frac{k(n-p+r)}{2}
    + \frac{kp}{2} + \frac{n-2p+r}{2} \\
    & = \frac{n^2-3n+2p}{2} - \frac{k(n-p+r)}{2} + (n-p) \\
    & = \binom{n}{2} - \frac{k(n-p+r)}{2} = t_{n,p}.
  \end{align*}

  Ou seja, temos~\(|E(G)| \leq t_{n,p}\). Como~\(T_{n,p}\) não possui
  nenhuma cópia de~\(K_{p+1}\) e~\(G\) é extremal, segue a outra
  desigualdade e, portanto, temos~\(|E(G)| = t_{n,p}\).

  Isso significa também que temos~\(q_2 = (n-p)(p-1)\) e~\(q_3 =
  t_{n-p,p}\). Pela hipótese de indução, temos que~\(G-V(H) \cong
  T_{n-p,p}\). Ademais, o valor de~\(q_2\) nos garante que cada
  vértice~\(v\in V(G)\setminus V(H)\) é adjacente a todo~\(w\in
  V(H)\), com exceção de um deles. Isso determina uma partição de
  $V(G)$ em $p$ classes. De fato, se $V(H)=\{v_1, \ldots, v_p\}$, 
então temos a partição $V_1, \ldots, V_p$, onde $V_i=\{v\in V(H): 
 v \text{ não é adjacente a }  v_i\}$.

  Portanto~\(G \cong T_{n,p}\) (\(G\) é balanceado pois~\(T_{n-p,p}\) é
  balanceado por hipótese de indução).
\end{prova}
	

Uma outra prova pode ser obtida usando-se a \textbf{técnica da simetrização},
definida por \Zykov (1949). Omitiremos essa prova, mas apresentaremos
a definição desse conceito.

	Defina a \textbf{operação de simetrização} de um vértice~\(u\) em relação a
  um vértice~\(v \neq u\), $v$ não adjacente a $u$,  como a remoção das arestas incidentes
  a~\(u\), adição das arestas~\(uw\) para todo~\(w\in
  N_G(v)\setminus\{u\}\). 

  Observe que se~\(u\)
  e~\(v\)  são dois vértices distintos e não-adjacentes de um grafo~\(G\),
  temos que:

	\begin{itemize}
	\item Se~\(G\) não possui uma cópia de~\(K_{p+1}\), então após a
    operação de simetrização de~\(u\) em relação a~\(v\), o novo grafo
    também não possui uma cópia de~\(K_{p+1}\).
			
	\item Se~\(G'\) é o grafo obtido após a operação de simetrização
    de~\(u\) em relação a~\(v\), então~\(|E(G')| = |E(G)| +
    |N_G(v)\setminus\{u\}| - d_G(u)\).
	\end{itemize}

Usando a técnica da simetrização, Erd\H os (1970) provou o seguinte 
resultado (exercício):

\medskip

\noindent Teorema: Seja $G$ é um grafo livre de $K_{p+1}$ sobre $n$ vértices.
Então existe um grafo $p$-partido $H$ sobre o mesmo conjunto de
vértices, com $d_H(x) \geq d_G(x)$ para todo $x\in V(G)$. 
 

\begin{comment} %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
\begin{prova} (Técnica da simetrização) \Zykov~(1949)
  Seja~\(H\) um grafo extremal com~\(n\) vértices, então
  temos~\(\delta(H)+1\geq\Delta(H)\), caso contrário, poderíamos
  simetrizar um vértice de grau mínimo em relação a um de grau máximo
  e obter um grafo livre de~\(K_{p+1}\) com mais arestas.

  Ademais, também sabemos que se~\(\delta(H) < \Delta(H)\), então
  todos os vértices~\(v\) de grau~\(\delta(H)\) são adjacentes a todos
  os vértices~\(w\) de grau~\(\Delta(H)\), caso contrário, a
  simetrização de~\(v\) em relação a~\(w\) produziria um grafo livre
  de~\(K_{p+1}\) com mais arestas.

  Vamos provar agora que, se~\(v\) e~\(w\) são vértices de~\(H\) de
  mesmo grau, mas vizinhanças distintas, então~\(v\) é adjacente
  a~\(w\).

  Suponha que não. Como~\(v\) e~\(w\) possuem vizinhanças distintas e
  mesmo grau, sabemos que existem~\(v_0\in N_H(v)\setminus N_H(w)\)
  e~\(w_0\in N_H(w)\setminus N_H(v)\), sem perda de generalidade,
  supomos~\(d_H(v_0) \leq d_H(w_0)\) (caso contrário, basta trocar os
  nomes~\(v\) com~\(w\) e~\(v_0\) com~\(w_0\)).

  Seja~\(H'\) o grafo obtido a partir da simetrização de~\(v\) em
  relação a~\(w\) e observe que~\(H'\) é livre de~\(K_{p+1}\)
  e~\(|E(H')| = |E(H)|\), logo~\(H'\) também é extremal.

  Observe também que
  \begin{align*}
    d_{H'}(v_0) = d_H(v_0)-1 \leq d_H(w_0)-1 = d_{H'}(w_0)-2.
  \end{align*}
  Mas isso significa que a simetrização de~\(v_0\) em relação
  a~\(w_0\) no grafo~\(H'\) produz um grafo livre de~\(K_{p+1}\) com
  mais arestas que~\(H'\), o que é um absurdo.

  Portanto, juntando à adjacência dos vértices de graus distintos,
  sabemos que, se~\(v\) e~\(w\) são vértices de~\(H\) de vizinhanças
  distintas, então~\(v\) é adjacente a~\(w\).

  Considere a partição~\(P\) do conjunto dos vértices de~\(H\) de
  acordo com suas vizinhanças, isto é, se~\(N = \{N_H(v) : v\in
  V(H)\}\), então~\(P = \{ \{w \in V(H) : N_H(w) = A\} : A\in N\}\).

  A propriedade que acabamos de provar nada mais é do que o fato
  que~\(H\) é~\(|P|\)-partido completo com partição~\(P\).
  
  Certamente~\(|P| \leq p\), caso contrario, o grafo~\(H\) não seria
  livre de~\(K_{p+1}\).

  Ademais, como~\(\delta(H)+1\geq\Delta(H)\), segue que~\(H\) é
  balanceado, ou seja, é isomorfo a~\(T_{n,k}\), para algum~\(k\leq
  p\).

  A igualdade de~\(k\) e~\(p\) segue da maximalidade do valor
  de~\(|E(H)|\).
%DONE: terminar a prova de Zykov do Teorema de Turán
\end{prova}

\end{comment} %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




\begin{corolario}\label{cor:6.2:graph}
  Para todo natural~\(n \geq 3\), temos que~\(\EX(n,K_3) =
  \left\{K_{\left\lfloor\frac{n}{2}\right\rfloor\left\lceil\frac{n}{2}\right\rceil}\right\}\)
  e~\(\ex(n,K_3) \leq \frac{n^2}{4}\).
  \end{corolario}
  
\begin{corolario}\label{cor:6.3}
  Se \(n \geq p+1\), então todo grafo com $n$ vértices e \(t_{n,p} + 1\) arestas
  contém um \(K_{p+1}-e\) (onde~\(e\) é uma aresta qualquer de~\(K_{p+1}\)).
\end{corolario}
  
\begin{prova}
  Provaremos por indução em~\(n\).
    
  Observe que para~\(n = p+1\) o resultado é trivial (pois
  \(t_{p+1,p-1} + 1 = \binom{p+1}{2} - 1\)).

  Suponha então que~\(n > p+1\) e que o resultado é válido
  para grafos de ordem menor que~\(n\).

  Seja~\(G\) um grafo de ordem~\(n\) com~\(t_{n,p-1}+1\) arestas.

  Suponha que~\(\delta(G) > \delta(T_{n,p-1})\), então, como~\(|E(G)|
  = |E(T_{n,p-1})| + 1\), sabemos que~\(G\) é um grafo~\(p-1\)-partido
  completo, logo~\(G\) contém um~\(K_{p+1}-e\).

  Suponha então que~\(\delta(G) \leq \delta(T_{n,p-1})\), seja~\(x\in
  V(G)\) tal que~\(d_G(x) = \delta(G)\) e observe que
 % \begin{align*}
   $|E(G-x)| = t_{n,p-1}+1-\delta(G) \geq |E(T_{n,p-1})|+1 -
    \delta(T_{n,p-1}) = |E(T_{n-1,p-1})|+1 = t_{n-1,p-1}.$
  %\end{align*}

  Portanto, pela hipótese de indução temos~\(G \supseteq G-x \supseteq
  K_{p+1}-e\).
\end{prova}
  
\begin{exercicio}\label{ex:6.4}
  Temos que 
  \[
  t_{n,p-1} \leq \frac{1}{2}n^2 \frac{p-2}{p-1},
  \]
  e vale a igualdade quando~\(p-1\) divide~\(n\).
\end{exercicio}
  
\begin{corolario}\label{cor:6.5}
  Temos
  \[
  \lim_{n\rightarrow \infty} \frac{t_{n,p-1}}{\binom{n}{2}} =
  \frac{p-2}{p-1}.
  \]
\end{corolario}
  
Na verdade, a proposição acima pode ser generalizada para um resultado
que depende do Teorema de~\Erdos~\&~\Stone\ enunciado a seguir.

Informalmente, esse teorema diz que com apenas mais~\(\epsilon n^2\)
arestas adicionais temos não apenas o~\(K_p\), mas um~\(K_s^p\), isto
é, um~\(p\)-partido completo com classes de ordem~\(s\).
  
\begin{teorema}[\Erdos~\&~\Stone, 1946]\label{teo:6.6}
  Para todo~\(p \geq 2\) e~\(s \geq 1\) inteiros, e para
  todo~\(\epsilon > 0\), existe um inteiro~\(n_0\) tal que todo grafo
  com~\(n \geq n_0\) vértices e pelo menos~\(t_{n,p-1} + \epsilon
  n^2\) arestas contém um~\(K^p_s\) como subgrafo.
\end{teorema}

O seguinte corolário generaliza o Corolário~\ref{cor:6.5}.
  
\begin{corolario}[\Erdos\ \& \Stone]\label{cor:6.7}
  Para todo grafo~\(G\),
  \[
  \lim_{n\rightarrow \infty} \frac{\ex(n,G)}{\binom{n}{2}} =
  \frac{\chi(G) - 2}{\chi(G) - 1}.
  \]
\end{corolario}
  
\begin{prova}(Corolário~\ref{cor:6.7})
  Seja~\(p=\chi(G)\). Como~\(G\) não pode ser~\((p-1)\)-colorido,
  temos que~\(G \not\subseteq T_{n,p-1}\) para todo~\(n \in \NN\), e,
  portanto, temos~\(t_{n,p-1} \leq \ex(n,G)\).

  Por outro lado, para todo~\(s\) suficientemente grande temos~\(G
  \subseteq K^p_s\) (\(s\geq\Delta(G)\) é suficiente). Logo,
  temos~\(\ex(n,G) \leq \ex(n,K^p_s)\).
  
  Vamos fixar um tal~\(s\). Para todo~\(\epsilon > 0\), o
  Teorema~\ref{teo:6.6} implica que para~\(n\) suficientemente grande
  temos~\(\ex(n,K^p_s) < t_{n,p-1} + \epsilon n^2\).
  
  Portanto, para~\(n\) suficientemente grande temos que
  \begin{align*}
    \frac{t_{n,p-1}}{\binom{n}{2}} & \leq \frac{\ex(n,G)}{\binom{n}{2}}
    \leq \frac{(n,K^p_s)}{\binom{n}{2}} < \frac{t_{n,p-1} +\epsilon
      n^2}{\binom{n}{2}} \\
    & = \frac{t_{n,p-1}}{\binom{n}{2}} +
    \frac{2\epsilon}{1-\frac{1}{n}} \leq
    \frac{t_{n,p-1}}{\binom{n}{2}} + 4\epsilon \\
  \end{align*}

  Usando o Corolário~\ref{cor:6.5}, concluímos que 
  \[
  \lim_{n\rightarrow \infty} \frac{\ex(n,G)}{\binom{n}{2}} 
  = \frac{p-2}{p-1} = \frac{\chi(G) -2}{\chi(G) -1}.
  \]    
\end{prova}

%% Local Variables:
%% mode: latex
%% coding: utf-8
%% fill-column: 70
%% tab-width: 2
%% End:
