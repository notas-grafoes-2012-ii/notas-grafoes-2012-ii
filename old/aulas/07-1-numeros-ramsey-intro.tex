%\begin{resumo-aula}{17}{10}{2012}

%\end{resumo-aula}

\section{Teoria de Ramsey}

Em~1927, Frank~Plumpton~Ramsey [1903 - 1930], lógico inglês, provou no
seu trabalho de teoria dos conjuntos o que se chama hoje de
\emph{Teorema de~Ramsey}, um teorema que abriu novas portas para o
estudo de combinatória. Atualmente, devido a vastas pesquisas sobre o
assunto, a área conhecida como \emph{Teoria de~Ramsey} é bem
estabelecida na matemática. Essa teoria procura encontrar
regularidades dentro de uma estrutura larga e caótica. Segundo as
palavras de Theodore~S.~Motzkin: ``A completa desordem é impossível.''

Neste capítulo, apresentaremos o Teorema~de~Ramsey e vários corolários.

\section{Teorema~de~Ramsey}

Vamos lembrar o \emph{Princípio da~Casa~dos~Pombos}: se
colocamos~\(n+1\) pombos em~\(n\) casas, então alguma casa vai receber
mais de um pombo. Embora simples, este princípio é uma poderosa
ferramenta para obter resultados de existência. O Teorema de~Ramsey
pode, a grosso modo, ser visto como uma generalização do princípio da
casa dos pombos.

  \begin{teorema}[Teorema de~Ramsey, versão para grafos]\label{ramseygrafos}
  Dados inteiros positivos~\(n\) e~\(m\), existe um inteiro
  positivo~\(N\) tal que, se~\(G\) é um grafo com pelo menos~\(N\)
  vértices, então~\(G\) contém uma cópia do grafo completo~\(K_n\) ou
  contém uma cópia do grafo vazio~\(\overline{K_m}\).
  \end{teorema}

Uma afirmação que descreve bem o Teorema de~Ramsey é a seguinte: Dados
inteiros positivos~\(n\) e~\(m\) existe um inteiro positivo~\(N\) tal
que, em qualquer conjunto de~\(N\) pessoas, sempre existem~\(n\)
pessoas que se conhecem mutuamente ou~\(m\) pessoas que se desconhecem
mutuamente.


O~\defi{número de~Ramsey}, denotado por~\(r(n,m)\), é o menor~\(N\)
que satisfaz a condição acima. Não é difícil ver que as seguintes
propriedades valem para~\(r(n,m)\).
\begin{enumerate}
\item Para todo inteiro positivo~\(n\), valem~\(r(n,1)=1\) e~\(r(n,2)=n\);
\item Para todo inteiro positivo~\(n\) e~\(m\), vale~\(r(n,m)=r(m,n)\).
\end{enumerate}

Uma outra forma de definir o número de~Ramsey é utilizando
colorações. Dado um inteiro positivo~\(k\), uma~\(k\)-coloração de um
grafo~\(G\) é uma função~\(c\: E(G)\to X\), onde~\(X\) é um conjunto
de cardinalidade~\(k\). Muitas vezes, vemos a coloração~\(c\) como a
partição~\(\{c^{-1}(x) : x \in X\}\) de~\(E(G)\) induzida pela
pré-imagem de~\(c\). Os elementos de~\(X\) são chamados~\defi{cores}
da coloração~\(c\).

Dada uma coloração~\(c\) de um grafo~\(G\), um subgrafo~\(H \subseteq G\) e uma
cor~\(i\), dizemos que~\(H\) é monocromático de cor~\(i\) se toda aresta de~\(H\)
possui cor~\(i\) em~\(G\).

Portanto~\(r(n,m)\) é o o menor inteiro positivo~\(p\) tal que para
toda \hbox{\(2\)-coloração} de~\(K_p\), digamos, em verde e azul, existe um
subgrafo~\(K_n\) monocromático de cor verde ou um subgrafo~\(K_m\)
monocromático de cor azul.

\begin{exercicio}
 Mostre que todo grafo com 6~vértices contém uma cópia de~\(K_3\) ou uma cópia
 de~\(\overline{K_3}\). Mostre que essa afirmação não vale para grafos
 com menos do que 6~vértices. Conclua que~\(r(3,3)=6\).
\end{exercicio}

O resultado a seguir  afirma que o número~\(r(n,m)\) está bem definido para todos inteiros
positivos~\(n\) e~\(m\).


\begin{teorema}
  Para todos inteiros positivos~\(m \geq 2\) e~\(n \geq 2\), existe~\(r(n,m)\) e
  \[
  r(n,m) \leq r(n-1,m) + r(n,m-1).
  \]
\end{teorema}

\begin{prova}
  Vamos provar por indução em~\(m+n\).

  Note que, para~\(n=1\) ou~\(n=2\), temos~\(r(1,m)=1\) e~\(r(2,m)=m\), para
  qualquer~\(m\).

  Suponha que~\(n \geq 3\) e~\(m \geq 3\), e que, para todos~\(n'\) e~\(m'\) tais
  que~\(n'+m' < n+m\), exista~\(r(n',m')\).

  Se~\(G\) é um grafo com~\(r(n-1,m) + r(n,m-1)\) vértices, então
  vamos mostrar que~\(G\) contém uma cópia do grafo completo~\(K_n\)
  ou contém uma cópia do grafo vazio~\(\overline{K_m}\). Pelo
  princípio da casa dos pombos, existe um vértice~\(v\) em~\(G\) tal
  que~\(d_G(v)\geq r(n-1,m)\) ou~\(d_{\overline{G}}(v)\geq r(n,m-1)\).

  Suponha que exista~\(v\) em~\(G\) tal que~\(d_G(v) \geq r(n-1,m)\). Seja~\(H=
  G[\adj(v)]\). Pela hipótese de indução,~\(H \supseteq K_{n-1}\) ou~\(H \supseteq
  \overline{K_m}\). No primeiro caso, basta tomar o~\(K_{n-1}\) em~\(H\) e
  a estrela com centro~\(v\). O segundo caso é óbvio.

  A demonstração para o caso em que existe~\(v\) tal que~\(d_{\overline{G}}(v) \geq
  r(n,m-1)\) é análoga, tomando~\(H=G[\adj_{\overline{G}}(v)]\).
\end{prova}

%DONE verificar referência do teorema abaixo. (parece ser~\cite{ErdosSzekeres35})
%De fato, é desse paper.
\begin{teorema}[\Erdos~\&~\Szekeres, 35~\cite{ErdosSzekeres35}]\label{es35}
  Para todos inteiros positivos~\(m\) e~\(n\), vale que
  \[
    r(n,m) \leq \binom{n+m-2}{n-1}.
  \]
\end{teorema}

\begin{prova}
  Vamos provar por indução em~\(n+m\).

  Já vimos que, para~\(n=1\) ou~\(n=2\), temos~\(r(1,m)=1\) e~\(r(2,m)=m\), para
  qualquer~\(m\).

  Suponha que~\(n \geq 3\) e~\(m \geq 3\), e que, para todos~\(n'\) e~\(m'\) tais
  que~\(n'+m' < n+m\), vale a desigualdade do enunciado.

  Em particular, vale que
  \[
    r(n-1,m) \leq \binom{n+m-3}{n-2}
    \quad\text{ e }\quad
    r(n,m-1) \leq \binom{n+m-3}{n-1}.
  \]

  É fácil ver que
  \[
    \binom{k}{p} = \binom{k-1}{p-1} + \binom{k-1}{p}.
  \]
  Logo,
  \begin{align*}
    r(n-1,m) + r(n,m-1)
    & \ligual
    \binom{n+m-3}{n-2} + \binom{n+m-3}{n-1}\\
    &\igual
    \binom{n+m-2}{n-1}.
  \end{align*}
\end{prova}

Um~\((n,m)\)-grafo de~Ramsey é um grafo com~\(r(n,m)-1\) vértices que não
contém~\(K_n\) e nem~\(\overline{K_m}\).

Não é difícil ver que~\(C_5\) é um~\((3,3)\)-grafo de~Ramsey; e o
Wagner~graph, que é o grafo~\(C_8\) com cordas ligando vértices
opostos é um~\((3,4)\)-grafo de~Ramsey. Observe que ambos os grafos
provam que~\(r(3,3)> 5\) e~\(r(3,4)> 8\).

Em geral, para mostrar o valor exato de~\(r(n,m)\), basta exibir um~\((n,m)\)-grafo
de~Ramsey com~\(r(n,m)-1\) vértices. Porém, é difícil explicitar um~\((n,m)\)-grafo
de~Ramsey para quaisquer~\(n\) e~\(m\).

Poucos valores de~\(r(n,m)\) são conhecidos. A tabela abaixo mostra
alguns deles ou os intervalos que os contêm. (Possivelmente alguns 
dados podem estar desatualizados.)


\begin{center}
\tiny{
\begin{tabular}{|c|c|c|c|c|c|c|c|c|c|c|}
\hline
  & 3& 4 & 5     & 6       & 7       & 8        & 9        & 10        \\ \hline
 3& 6& 9 & 14    & 18      & 23      & 28       & 36       & 40--42    \\ \hline
 4&  & 18& 25    & 36--41  & 49--61  & 58--84   & 73--115  & 92--149   \\ \hline
 5&  &   & 43--49& 58--87  & 80--143 & 101--216 & 126--316 & 144-442   \\ \hline
 6&  &   &       & 102--165& 113--298& 132--495 & 169--780 & 179--1171 \\ \hline
 7&  &   &       &         & 205--540& 217--1031& 241--1713& 289--2826 \\ \hline
 8&  &   &       &         &         & 282--1870& 317--3583& 331--6090 \\ \hline
 9&  &   &       &         &         &          & 565--6588& 581--12677\\ \hline
10&  &   &       &         &         &          &          & 798--23556\\ \hline
\end{tabular}
}

Valores de~\(r(n,m)\)
\end{center}
%DONE tabela de Ramsey mais completa
%DONE a fonte da tabela de Ramsey é a wiki, então requer checagem...

Um problema famoso relacionado ao Teorema de~Ramsey é determinar ou estimar o
número~\(r(n)=r(n,n)\). Já sabemos que~\(r(1)=1\),~\(r(2)=2\) e~\(r(3)=6\). É um bom
exercício provar que~\(r(4)=18\). Sabe-se somente que~\(43\le r(5)\le 49\).

Pelo Teorema~\ref{es35}, obtemos
\[
r(n)\le \binom{2n-2}{n-1}\le \frac{c}{\sqrt{n}}4^n
\]
para alguma constante~\(c>0\). [Para prova a última desigualdade, usar
o seguinte fato: $n! = \sqrt{2\pi}\,n^{n+1/2} e^{-n} e^{r_n}$, onde
$\frac{1}{12n +1} < r_n < \frac{1}{12n}$.]

\smallskip

A melhor estimativa inferior é devida a~Erd\H{o}s, que utilizou o que chamamos hoje de
\emph{método probabilístico}, um poderoso método que é utilizado fortemente em várias
pesquisas atuais. Já vimos o uso de tal método no capítulo anterior. 

\begin{teorema}[\Erdos, 1947]\label{teo:7.4}
  Para todo inteiro positivo~\(n \geq 3\),
  \[
    r(n,n) > \left\lfloor 2^{n/2}\right\rfloor.
  \]
\end{teorema}

\begin{prova}
  Fixe um conjunto~\(V\) com cardinalidade~\(p = \left\lfloor 2^{n/2}\right\rfloor\),
  e considere~\(\mathcal{G}_p\) a classe de todos os grafos
  sobre~\(V\). Então~\(|\mathcal{G}_p| = 2^N\), onde ~\(N :=
  \binom{p}{2}\).

  Afirmamos que existe~\(G\in \mathcal{G}_p\) tal que~\(G \not\supseteq K_n\)
  e~\(G \not \supseteq \overline{K_n}\).

  Seja~\(\mathcal{H}_p\) a classe dos grafos de~\(\mathcal{G}_p\) que contêm~\(K_n\)
  como subgrafo. Para cada~\(S \subseteq V\), com~\(|S| = n\), o
  número de grafos em~\(\mathcal{G}_p\) nos quais~\(S\) induz um subgrafo
  completo~\(K_n\) é~\(2^{N-M}\), onde \(M := \binom{n}{2}\). Então
 % ~\(M := \binom{n}{2}\).
 %  Então~\(|\mathcal{G}_p| = 2^N\). Para cada~\(S \subseteq V\), com~\(|S| = n\), o
 %  número de grafos em~\(\mathcal{G}_p\) nos quais~\(S\) induz um subgrafo
 %  completo~\(K_n\) é~\(2^{N-M}\). Então
  \[
  |\mathcal{H}_p| \leq \binom{p}{n}2^{N-M} < \frac{p^n}{n!}2^{N-M}.
  \]

  Sabemos que~\(p \leq 2^{n/2}\).
  Logo, \(p^n\leq 2^{n^2/2}\).
  Para~\(n\geq 3\), temos~\(2^{n/2} < n!/2\). Assim
\begin{align*}
  \left(2^{n/2}\right)^{1/n} & \quad < \quad \left(\frac{1}{2}n!\right)^{1/n} \\
  2^{1/2}  & \quad < \quad  \left(\frac{1}{2}n!\right)^{1/n} \\
  2^{n/2}  & \quad < \quad
             \left(\frac{1}{2}n!\right)^{1/n}2^\frac{n-1}{2} \\
  2^{n^2/2} & \quad < \quad \left(\frac{1}{2}n!\right)2^M \\
\end{align*}
  Assim, temos que
  \[
  p^n \le 2^{n^2/2} < \left(\frac{1}{2}n!\right)2^M.
  \]
  Portanto,
  \[
  |\mathcal{H}_p| <
  p^n\frac{1}{n!}2^{N-M}<\left(\frac{1}{2}n!\right)2^M\frac{1}{n!}2^{N-M}=2^{N-1}.
  \]
  Seja~\(\mathcal{H}\) o conjunto dos grafos em~\(\mathcal{H}_p\) e seus
  complementos, isto é, o conjunto dos grafos que contêm~\(K_n\)
  ou~\(\overline{K_n}\). Como~\(|\mathcal{G}_p| = 2^N\)
  e~\(|\mathcal{H}|< 2 |\mathcal{H}_p| < 2^{N}\),
  temos que~\(\mathcal{H}\) é um subconjunto próprio de~\(\mathcal{G}_p\). Ou seja,
  existe~\(G\in \mathcal{G}_p \setminus \mathcal{H}\), como queríamos.
\end{prova}

%DONE citar artigo do Thomason
Thomason, em~1988~\cite{Thomason88}, provou que existe uma constante~\(c>0\) tal que
\[
  r(n+1) \leq (n+1)^{-1/2+c/\sqrt{\log (n+1)}}\binom{2n}{n}.
\]

Em 2009, Conlon~\cite{Conlon09} provou o seguinte resultado, que melhora sensivelmente o
resultado de Thomasson. Existe uma constante~\(c>0\) tal que
\[
  r(n+1) \leq n^{-c\log n/\log\log n}\,\binom{2n}{n}.
\]





Algumas conjecturas na Teoria de~Ramsey ainda estão em aberto.

\begin{conjectura}
Existe uma constante~\(k\) (talvez~\(k=1\)) tal que~\(r(n) = 2^{(k+O(1))n}\), para
todo~\(n\).
\end{conjectura}

\begin{conjectura}\label{conj:limrn}
O limite~\(\lim\limits_{n\to \infty}r(n)^{1/n}\) existe.
\end{conjectura}

Erd\H{o}s já sabia as seguintes estimativas em~1947, que provamos anteriormente
\[
\sqrt{2}\le \liminf_{n\to \infty}r(n)^{1/n}\le \limsup_{n\to \infty}r(n)^{1/n}\le 4.
\]
Assim, ele propôs no mesmo ano o problema de encontrar o limite, caso ele
exista. Erd\H{o}s ofereceu~100 dólares para quem resolver a
Conjectura~\ref{conj:limrn} e~250 dólares para quem conseguir calcular o limite, caso
exista.

\subsection{Teorema de~Ramsey com mais cores}

O Teorema de~Ramsey garante que ao colorirmos as arestas do grafo completo de
ordem~\(r(n,m)\) com duas cores, digamos azul e verde, existe um subgrafo completo
monocromático de cor verde de ordem~\(n\) ou de cor azul de ordem~\(m\). A seguir,
veremos que o Teorema de~Ramsey continua verdadeiro no caso em que temos mais do que
duas cores.

\begin{teorema}[Teorema de~Ramsey com~\(k\) cores]
Fixe~$k$ inteiros positivos quaisquer \(p_1,\ldots,p_k\).  Existe um inteiro
positivo~\(n\) tal que para toda \(k\)-coloração das arestas do grafo completo de
ordem~\(n\) existe um subgrafo completo de ordem~\(p_i\) monocromático de cor~\(i\),
para algum~\(1 \leq i \leq k\).
\end{teorema}

\begin{prova}
Seja~\(r(p_1,\ldots,p_k)\) o menor~\(n\) que satisfaz o Teorema
de~Ramsey com~\(k\) cores. Por indução em~\(p_1+p_2+\ldots+p_k\),
mostremos que
\[
r(p_1,p_2,\ldots,p_k)\le 2+\sum_{1\le i \le k}{(r_i-1)},
\]
onde~\(r_i=r(p_1,\ldots,p_i-1,\ldots,p_k)\) para todo~\(1\le i\le k\).

Observe que, se existe~\(i\) tal que ~\(p_i=1\), então \hbox{\(r(p_1,p_2,\ldots,p_k)=1\).}

Suponha que todos os~\(p_i\)'s são maiores que~\(1\) e que a afirmação vale
para~\(p_1+p_2+\ldots+p_k-1\).

Seja~\(n=2+\sum_{1\le i \le k}{(r_i-1)}\). Fixe uma~\(k\)-coloração das arestas
de~\(K_n\) e~\(v\) um vértice de~\(K_n\). Pela escolha de~\(n\), existe~\(i\) tal que
o vértice~\(v\) possui pelo menos~\(r_i\) incidentes arestas de cor~\(i\). Considere
o grafo~\(G' = G - v\). Pela hipótese de indução, temos dois casos:
\begin{enumerate}
\item O grafo~\(G'\) contém uma cópia~\(K_{p_i-1}\) monocromática de
  cor~\(i\);
\item O grafo~\(G'\) contém uma cópia~\(K_{p_j}\) monocromática de
  cor~\(j \neq i\).
\end{enumerate}

No segundo caso, não há nada para fazer. Suponha então que não ocorreu o segundo
caso. Como~\(v\) possui pelo menos~\(r_i\) arestas de cor~\(i\) incidentes, temos
que~\(K_n\) possui um subgrafo~\(K_{p_i}\) monocromático da cor~\(i\), como
queríamos.
\end{prova}

\subsection{Teorema de~Ramsey para hipergrafos completos}

Estenderemos a versão de grafos do Teorema de~Ramsey para
hipergrafos. Um \emph{\(l\)-grafo} é um par de conjuntos~\(G=(V,E)\)
tal que~\(E\subset \binom{V}{l}=\{U\subset V:\ |U|=l\}\). Um
\emph{\(l\)-grafo completo} de ordem~\(n\), denotado
por~\(K_n^{(l)}\), é um~\(l\)-grafo de ordem~\(n\) e de
tamanho~\(\binom{n}{l}\).

\begin{lema}[Princípio da~Casa~dos~Pombos]\label{pcp}
Sejam~\(k,\ p_1,\ldots,\ p_k\) naturais. Se~\(\sum_{i=1}^k{(p_i-1)+1}\) objetos são
coloridos com~\(k\) cores, então existe pelo menos~\(p_i\)
objetos monocromáticos da cor~\(i\).
\end{lema}

\begin{prova}
Suponhamos que exista uma~\(k\)-coloração dos~\(\sum_{i=1}^k(p_i-1)+1\) objetos
tal que toda cor~\(i\) possui no máximo~\(p_i-1\) objetos coloridos com essa cor.
Neste caso, há no máximo~\(\sum_{i=1}^k{(p_i-1)}\) objetos, uma contradição.
\end{prova}

\begin{teorema}[Teorema de~Ramsey para hipergrafos]
Para todo inteiros positivos~\(p_1,\ldots,p_k\) existe um inteiro
positivo~\(n\) tal que para toda~\(k\)-coloração das arestas
do~\(l\)-grafo completo~\(K_n^{(l)}\) existe um
subgrafo~\(K_{p_i}^{(l)}\) monocromático da cor~\(i\).
\end{teorema}

\begin{prova}
Mostremos, por indução dupla em~\(l\) e~\(p_1+\ldots+p_k\), que
\begin{equation}\label{desramsey}
R^{(l)}(p_1,p_2,\ldots,p_k)\le 1+ R^{(l-1)}(R_1,R_2,\ldots,R_k),
\end{equation}
onde~\(R_i = R^{(l)}(p_1,\ldots,p_i-1,\ldots,p_k)\).

Para~\(l=1\), temos, pelo Lema~\ref{pcp}, que~\(R^{(1)}(p_1,p_2,\ldots,p_k)\)
existe e é igual a~\(\sum_{i=1}^k{(p_i-1)+1}\).

Suponhamos que o teorema vale para um~\(k\)-coloração do~\((l-1)\)-grafo completo.
Mostremos, por indução em~\(p_1+\ldots+p_k\), que o teorema vale
para uma~\(k\)-coloração do~\(l\)-grafo completo.

Se algum~\(p_i\) é menor que~\(l\), então o grafo completo~\(K^{(l)}_{p_i}\) tem
cor~\(i\). Logo
\[
R^{(l)}(p_1,p_2,\ldots,p_k)=\min\{p_1,\ldots,p_k\}.
\]

Suponhamos que a desigualdade~(\ref{desramsey}) vale para~\(p_1+\ldots+p_k-1\).
Pela hipótese de indução (tanto a interna quanto a externa),~\(R_i\) existem,
para todo~\(1\le i\le k\).

Seja~\(n=1+ R^{(l-1)}(R_1,\ldots,R_k)\). Fixemos uma~\(k\)-coloração
das arestas de~\(K_n^{(l)}\) e~\(v\) vértice de~\(K_n^{(l)}\). Pela
escolha de~\(n\), temos que existe~\(i\) tal que o vértice~\(v\)
possui pelo menos~\(R_i\) arestas de cor~\(i\) incidentes. Temos dois
casos:
\begin{enumerate}
\item Existe um subgrafo~\(K^{(l)}_{p_i-1}\) tal que suas arestas tem cor~\(i\);
\item Existe um subgrafo~\(K^{(l)}_{p_j}\) com~\(j\neq i\) tal que
suas arestas possuem a cor~\(j\).
\end{enumerate}

Se ocorrer o segundo caso, não há nada para fazer. Suponhamos que
ocorreu o primeiro caso. Como~\(v\) possui~\(R_i\) arestas de
cor~\(i\) incidentes e não ocorreu o segundo caso, temos
que~\(K_n^{(l)}\) possui um subgrafo~\(K_{p_i}^{(l)}\) monocromático
da cor~\(i\).
\end{prova}

\section{Número de~Ramsey para grafos arbitrários}

Pela definição do número de~Ramsey~\(r(n,m)\), queremos encontrar
grafos com quantidade de vértices suficientemente grande para conter
uma cópia de~\(K_n\) ou~\(\overline{K_m}\). Podemos generalizar o
número de~Ramsey substituindo~\(K_n\) e~\(\overline{K_m}\) por dois
grafos~\(G_1\) e~\(G_2\) com~\(|V(G_1)|=n\) e~\(|V(G_2)|=m\). Neste
caso, definimos o número de~Ramsey generalizado.

\begin{definicao}
Sejam~\(n\) e~\(m\) inteiros positivos e~\(G_1\) e~\(G_2\) grafos com
ordem~\(n\) e~\(m\), respectivamente. O \emph{número de~Ramsey
  generalizado}~\(r(G_1,G_2)\) é o menor inteiro positivo~\(p\) tal
que qualquer grafo~\(G\) de ordem~\(p\) ontém uma cópia de~\(G_1\) ou
seu complemento contém uma cópia de~\(G_2\).
\end{definicao}

Claramente~\(r(n,m) = r(K_m,K_n)\). Ademais, para todo grafo~\(G_1\)
e~\(G_2\) com ordem~\(n\) e~\(m\), respectivamente,
vale~\(r(G_1,G_2)\le r(n,m)\). Isto mostra que~\(r(G_1,G_2)\) está bem
definido. Entretanto, pode ocorrer que~\(r(G_1,G_2)\) seja muito
menor que~\(r(n,m)\) se~\(G_1\) e~\(G_2\) forem `esparsos', isto é, a
ordem de~\(G_1\) e~\(G_2\) forem relativamente grande em relação às
suas quantidades de arestas.

Uma outra forma de definir o número de~Ramsey generalizado é
utilizando colorações. De fato, o número~\(r(G_1,G_2)\) é o menor
inteiro positivo~\(p\) tal que para toda~\(2\)-coloração das arestas
do grafo completo~\(K_p\) existe um subgrafo monocromático $G_i$ de
cor~\(i\).

Apresentaremos alguns resultados para grafos particulares.

\begin{teorema}[\Chvatal,77]\label{teo:7.5}
  Seja~\(T_m\) uma árvore qualquer de ordem~\(m\geq 1\) e seja~\(n\)
  um natural não nulo. Então
  \[
  r(T_m,K_n) = 1 + (m-1)(n-1).
  \]
\end{teorema}

\begin{prova}
  Considere~\(m>1\) e~\(n>1\), caso contrário o resultado é trivial. Considere~\(G =
  (n-1)K_{m-1} \) uma união disjunta de~\(n-1\) cópias de~\(K_{m-1}\).
  Note que~\(G\) tem~\((m-1)(n-1)\) vértices e não contém nem~\(T_m\)
  e nem~\(\overline{K_n}\). Isso mostra que~\(r(T_m,K_n)\ge
  1+(m-1)(n-1)\).

  Seja~\(G\) um grafo com~\(1 + (m-1)(n-1)\) vértices. Suponhamos
  que~\(G \not\supseteq \overline{K_n}\). Mostremos que~\(G\supseteq
  T_m\). Notemos que ~\(\alpha(G) \leq n-1\). Como~\(\chi(G) \geq
  \frac{|V(G)|}{\alpha(G)}\), vale
  \[
    \chi(G) \geq \frac{1+(m-1)(n-1)}{n-1} >  m-1.
  \]
  Portanto~\(\chi(G) \geq m\). Seja~\(G'\) um subgrafo de~\(G\) que é
  criticamente~\(m\)-cromático, isto é,~\(\chi(G') = m\)
  e~\(\chi(G'-v) = m-1\) para todo vértice~$v$. Neste caso~\(\delta(G') \geq m-1\)
  (exercício), donde concluímos que \(G' \supseteq T_m\). Portanto~\(G
  \supseteq T_m\), como queríamos.
\end{prova}


\begin{exercicio}
  Fazer uma outra prova do Teorema~\ref{teo:7.5},
  usando a linguagem de~\(2\)-coloração de~\(K_{(m-1)(n-1)+1}\)
  e fazendo indução em~\(m+n\).
\end{exercicio}

\begin{teorema}\label{lk2kp}
Para~\(\ell\ge 1\) e~\(p\ge 2\) temos
\[
r(lK_2,K_p)=2\ell+p-2.
\]
\end{teorema}

\begin{prova}
O grafo~\(K_{2\ell-1}\cup \overline{K_{p-2}}\) não contém~\(\ell\) arestas
independentes, e seu complementar, o grafo~\(\overline{K_{2\ell-1}}+K_{p-2}\), não contém um
grafo completo de ordem~\(p\). Logo~\(r(\ell K_2,K_p)\ge2\ell+p-2\).

Por outro lado, seja~\(G\) um grafo de ordem~\(n=2\ell+p-2\). Suponhamos
que~\(G\) contém no máximo~\(s\le \ell-1\) arestas
independentes. Mostremos que~\(\overline{G}\) contém um subgrafo~\(K_p\).

Como~\(n-2s\ge 2\ell+p-2-2(\ell-1)=p\), pelo menos $p$ vértices de $G$ são
independentes. Logo, existe~\(K_p\) em~\(\overline{G}\).
\end{prova}

Notemos que se~\(H\) é um grafo qualquer de ordem~\(h\), pelo
Teorema~\ref{lk2kp}, segue que
\[
r(lK_2,H)\le r(\ell K_2,K_h)\le 2\ell+h-2.
\]

O resultado a seguir fornece uma cota inferior para o número de~Ramsey
generalizado. Seja~\(G\)
um grafo. Como usual, denotemos por~\(\chi(G)\)
o número cromático de~\(G\).
Ademais, denotamos por~\(c(G)\) o máximo das ordens dos componentes de~\(G\),
e definimos~\(u(G)\)
como sendo a cardinalidade mínima das classes de cores considerando-se
todas as colorações próprias de~\(G\) com~\(\chi(G)\) cores.

\begin{teorema}\label{cotinf}
Para quaisquer grafos~\(H_1\) e~\(H_2\) com pelo menos uma aresta temos
\[
r(H_1,H_2)\geq (\chi(H_1)-1)(c(H_2)-1)+u(H_1).
\]
\end{teorema}

\begin{prova}
Sejam~\(k=\chi(H_1)\),~\(u=u(H_1)\) e~\(c=c(H_2)\). Naturalmente,
\[
r(H_1,H_2)\ge r(H_1,K_2)=|V(H_1)|\ge \chi(H_1)u(H_1)=ku.
\] %(1)
Assim, se~\(c\le u\), então~\(r(H_1,H_2)\ge ku\ge (k-1)c+u\). Por outro lado,  %(2)
se~\(c>u\), então o grafo~\(G=(k-1)K_{c-1}\cup K_{u-1}\) não contém~\(H_2\), e  %(3)
seu complementar não contém~\(H_1\). Portanto,
\[
r(H_1,H_2)\ge |V(G)|+1=(k-1)(c-1)+u,
\]
como queríamos.
\end{prova}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%(1) pois u(H_1) é a cardinalidade mínima das classes de cores
%considerando-se todas as colorações próprias de~$G$ com~$\chi(G)$ cores.
%Como \chi(H_1) é a menor quantidade de cores
%para colorir os vértices de H_1 com as devidas restrições, temos que
%\chi(H_1)\u(H_1) =\u(H_1)+\u(H_1)+...+\u(H_1) \chi(H_1) vezes é pequeno
%em relação à quantidade de vértices de $H_1$.

%(2) u\ge c => (k-1)u\ge (k-1)c => ku\ge (k-1)c+u.

%(3) Desenhe. H_2 não está contido em K_{c-1}, pois K_{c-1} tem menos
%vértices do que o máximo das ordens dos componentes de H_2, e também
%não está contido em K_{u-1} pois c>u. E H_1 não está contido em
%\bar{G}=(k-1)E_{c-1}+E_{u-1}, pois E_{u-1} tem pouco vértices em relação
%a H_1.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{teorema}
Para~\(\ell\ge 2\) temos
\[
r(F_1,F_l)=r(K_3,F_\ell)=4\ell+1,
\]
onde~\(F_\ell\) é união de~\(\ell\) triângulos~\(K_3\) com um vértice em comum.
\end{teorema}

\begin{prova}
Pelo Teorema~\ref{cotinf}, sabemos que~\(r(K_3,F_\ell)\ge 2(|F_\ell|-1)+1=4\ell+1\).

Suponhamos por absurdo que não vale a desigualdade~\(r(K_3,F_\ell)\le4l+1\)
isto é, existe um grafo~\(G\) livre de triângulos de ordem~\(4\ell+1\)tal que
seu complementar não contém~\(F_\ell\).

Fixemos~\(v\) vértice de~\(G\) e seja~\(U=N_G (v)\). Então~\(U\) é um
conjunto de vértices independentes, e, como~\(\overline{G}\) não contém~\(F_\ell\),
temos~\(d_{G}(v)=|U|\le 2\ell\). %(1)

Por outro lado, observemos o grau de~\(v\)
em~\(\overline{G}\). Seja~\(W=N_{\overline{G}}(v)=V(G)-(U\cup
\{v\})\). Temos que~\(\overline{G}[W]\) não contém~\(\ell\) arestas
independentes, e seu complementar~\(G[W]\) não possui triângulos. %(2)
Então, pelo Teorema~\ref{lk2kp},~\(d_{\overline{G}}(v)=|W|\le 2\ell\).

Logo concluímos que~\(d_{G}(v)=d_{\overline{G}}(v)=2l\) para todo~\(v\in
G\), isto é,~\(G\) é um grafo livre de triângulos~\(2\ell\)-regular de
ordem~\(4\ell+1\). Mostremos que isso não pode ocorrer.

Suponhamos por absurdo que existe um grafo~\(G=(V,E)\) que satisfaz as
condições acima. Notemos que~\(G\) pode ser escrito como um grafo
bipartido com~\(2\ell\) vértices mais um vértice~\(w\). Temos dois casos,
ou os vizinhos de~\(w\) estão somente em um dos lados da partição,
ou~\(w\) tem vizinhos nos dois lados da partição.

Se ocorrer o primeiro caso, vamos supor, sem perda de generalidade,
que~\(w\) incide suas arestas no lado esquerdo da partição. Temos que
cada vértice da partição da esquerda incide~\(2\ell-1\) arestas na
partição da direita. Logo, existe um vértice do lado direito da
partição com no máximo~\(2\ell-1\) arestas incididas, uma contradição.

Se ocorrer o segundo caso, suponhamos que~\(w\) incide~\(a\) arestas no
lado esquerdo da partição e~\(b\) arestas no lado direito da partição,
com~\(a+b=2\ell\). Suponhamos que~\(a\le b\), isto é,~\(a\le \ell\).

Por definição de grafo bipartido, sejam os conjuntos de vértices
independentes~\(W\) e~\(X\) disjuntos tais que~\(W\cup X=V\setminus
\{w\}\). Sejam os conjuntos~\(A\subset W\) e~\(B\subset X\) tais
que~\(|A|=a\) e~\(|B|=b\) que satisfaz a condição acima.

Fixemos um vértice~\(u\) de $A$. Temos que~\(u\) não pode incidir em
algum vértice de~\(B\), pois teríamos um triângulo. Logo~\(u\) só pode
incidir em~\(2\ell-b=a\le \ell\) vértices. Mas assim~\(d(u)\le \ell+1\), uma
contradição.
\end{prova}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%(1) pois vemos que \bar{U} é um grafo completo de ordem |U|, se |U|=2l+1,
%tome x um vértice de \bar{U}, então x tem 2l arestas incidentes, e portanto
%l triângulos.

%(2) pois se \bar{G}[W] tivesse l arestas independentes, então G[W] teria l
%triângulos.
%Se G[W] tivesse triângulos, iria satisfazer a afirmação do teorema.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

A seguir, apresentaremos outros resultados, sem provas, sobre o número
de~Ramsey generalizado.

\begin{teorema}[Lawrence,73]\label{teo:7.6}
	\[
		r(C_m,K_{1,n}) =
		\begin{cases}
			2n+1	&	\text{se } m \text{ é ímpar e }m \leq 2m+1		\\
			m	&	\text{se } m \geq 2n
		\end{cases}
	\]
\end{teorema}

\begin{teorema}[\Chvatal\ \& \Harary, 72]\label{teo:7.7}
	Para qualquer grafo~\(G\) de ordem~\(m\) e sem vértices isolados,
	\[
		r(G,P_3) =
		\begin{cases}
			m+1	&	\text{se }	\overline{G}	 \text{ tem um emparelhamento perfeito},	\\
			m	&	\text{caso contrário}
		\end{cases}
	\]
\end{teorema}

\begin{teorema}[\Chvatal\ \& \Gyarfas,67]\label{teo:7.8}
	Para naturais~\(m,n\) com~\(2 \leq m \leq n\)
	\[
		r(P_m,P_n) = n + \left\lfloor\frac{m}{2} \right\rfloor -1
	\]
\end{teorema}

\begin{teorema}[\Faudree\ \& \Schelp, 74] \label{teo:7.9}
	Sejam~\(m,n\) naturais tais que~\(3 \leq m \leq n\).
	\begin{itemize}
		\item[(a)]	Se~\(m\) é ímpar e~\((m,n) \neq (3,3)\), então
					\[
						r(C_m,C_n) = 2n -1.
					\]
		\item[(b)]	Se~\(m\) e~\(n\) são pares e~\((m,n) \neq (4,4)\), então
					\[
						r(C_m,C_n) = n + \frac{m}{2} - 1
					\]
		\item[(c)]	Se~\(n\) é ímpar e~\(m\) é par, então
					\[
						r(C_m,C_n) = \max\{n+\frac{m}{2}-1,2m-1\}
					\]
		\item[(d)]	\(r(C_3,C_3) = r(C_4,C_4) = 6\).
	\end{itemize}
\end{teorema}

%% Local Variables:
%% mode: latex
%% coding: utf-8
%% fill-column: 70
%% tab-width: 2
%% End:
