%\begin{resumo-aula}{22}{10}{2012}
%Aplicações de~Ramsey.
%\end{resumo-aula}

\section{Aplicações do Teorema de~Ramsey}

A Teoria de~Ramsey é uma área extensa. Resultados dessas teoria
encontram-se, por exemplo, na teoria dos grafos, na teoria
dos conjuntos, na teoria dos números, na teoria ergódica, em sistemas
dinâmicos topológicos.

Nesta seção veremos alguns resultados nos quais é aplicado o Teorema
de~Ramsey, e também teoremas que são do tipo~Ramsey, isto é, teoremas
envolvendo estruturas que são preservadas sob partição.

Primeiramente apresentaremos o Lema de~K\H{o}nig, um resultado
importante na teoria dos grafos.

\begin{teorema}\label{teo:7.12}[Lema de~K\H{o}nig]
	Seja~\(V_0,V_1, \ldots\) uma sequência \textit{infinita} de
  \textit{conjuntos finitos disjuntos}, não vazios, e seja~\(G\) um
  grafo cujo conjunto de vértices é a união desses conjuntos. Suponha
  que todo vértice~\(v \in V_i\), \(i \geq 1\), tem um
  vizinho~\(f(v)\) em~\(V_{i-1}\). Então~\(G\) contém um
  \textit{caminho infinito}~\(v_0v_1\cdots\) com~\(v_i \in V_i\) para
  todo~\(i\).
\end{teorema}

\begin{prova}
	Seja~\(\calp\) o conjunto dos caminhos da
  forma~\((v,f(v),f(f(v)),\ldots)\) que termina num vértice
  de~\(V_0\). Como~\(\calp\) é infinito e~\(V_0\) é finito, existe um
  vértice de~\(V_0\), digamos~\(v_0\), que é término de um número
  infinito de caminhos de~\(\calp\). Dentre esses infinitos caminhos
  que terminam em~\(v_0\), há um número infinito de caminhos cujo
  penúltimo vértice é um vértice, digamos~\(v_1\)
  de~\(V_1\). Procedendo indutivamente, definimos~\(v_i \in V_i\) para
  todo~\(i \geq 1\). Portanto~\(v_0 v_1 \cdots\) é um caminho infinito
  em~\(G\).
	\begin{comment}
	A prova segue da observação de que uma vez que~\(V_i\) é finito,
  existe um vértice~\(v_i \in V_i\) que é vértice um número infinito
  de caminhos. Assim,~\(f(v_i)\) também tem essa propriedade.
  Podemos então definir uma função~\(g\) que leva~\(v_i \in V_i\)
  em~\(v_{i+1} \in V_{i+1}\) onde ambos os vértices tem essa
  propriedade. Logo, temos~\(P = v_0 g(v_0) g^2(v_0) \cdots\) é um
  caminho infinito.
	\end{comment}
\end{prova}

Agora veremos a versão infinita do Teorema de~Ramsey.

\begin{definicao}
Dados~\(X\) um conjunto de cardinalidade infinita e~\(k\in \mathbb{N}\), definimos os
seguintes conjuntos:
\[
\begin{aligned}
\calp_{\infty}(X)&=\{Y\subset X:\ |Y|=\infty\}; \\
\calp_{k}(X)&=\{Y\subset X:\ |Y|=k\}.
\end{aligned}
\]
\end{definicao}

\begin{definicao}
Sejam~\(c,\ k\) inteiros positivos e~\(X\) um conjunto infinito. Uma
\emph{\(c\)-coloração de $\calp_{k}(X)$} é uma partição
de~\(\calp_{k}(X)\) em no máximo~\(c\)
classes (cores).  Se~\((A_1,\ldots,A_c)\) é uma coloração de~\(\calp_{k}(X)\),
para cada~\(i\), dizemos que cada elemento de~\(A_i\) está
\emph{colorido} com a cor~\(i\).

Dada uma~\(c\)-coloração de~\(\calp_{k}(X)\), dizemos que um conjunto~\(Y\in
\calp_{\infty}(X)\) é \emph{monocromático} se todos os elementos
de~\(\calp_{k}(Y)\) estão coloridos com a cor~\(i\), para algum~\(i\).
\end{definicao}

\begin{teorema}\label{teo:7.13}[Versão infinita do Teorema de~Ramsey]
	Sejam~\(k,c\) inteiros positivos e~\(X\) um conjunto infinito.
	Para toda~\(c\)-coloração de~\(\calp_k(X)\)
	existe um conjunto infinito~\(Y\in \calp_{\infty}(X)\) monocromático.
\end{teorema}

\begin{prova}
	A prova segue por indução em~\(k\), com $c$ fixo.
	Se~\(k = 1\) o resultado é óbvio, pois~\(P_1(X) = X\).

	Suponhamos que $k>1$ e que a afirmação vale valores menores do que
  \(k\).
  Fixe uma~\(c\)-coloração de~\(P_k(X)\).
  Vamos construir uma sequência infinita~\(X_0,X_1,X_2,\ldots\)
  de subconjuntos \textit{infinitos} de~\(X\),
  e escolher~\(x_i \in X_i\) com as seguintes propriedades.
	\begin{enumerate}[(a)]
		\item	\(X_{i+1} \subseteq X_i \setminus \{x_i\}\);
		\item Todos os conjuntos~\(\{x_i\}\cup Z\in \calp_k(X_{i+1})\)
      com~\(Z\in \calp_{k-1}(X_{i+1})\) têm a mesma cor.
	\end{enumerate}

	Começamos com~\(X_0 = X\) e tomamos~\(x_0 \in X\) arbitrário. Por
  hipótese, sabemos que~\(X_0\) é infinito. Uma vez escolhido~\(X_i\)
  e um elemento~\(x_i \in X_i\), definimos uma~\(c\)-coloração
  de~\(\calp_{k-1}(X_i\setminus\{x_i\})\), onde cada conjunto~\(Z\in
  \calp_{k-1}(X_i\setminus\{x_i\})\) recebe a cor de~\(\{x_i\}\cup Z\)
  da~\(c\)-coloração de~\(\calp_k(X)\).
	%ou seja, definimos a função~\(c':\calp_{k-1}(X_i\setminus \{x_i\}) \rightarrow \{1,2,\ldots,c\}, c'(Z) = c(\{x_i\}\cup X)\).

	Pela hipótese de indução, temos que~\(X_i\setminus\{x_i\}\) tem um
  subconjunto~\(Y'\in \calp_{\infty}(X_i\setminus\{x_i\})\)
  monocromático, que tomamos para ser~\(X_{i+1}\).

	Notemos que tal construção satisfaz~\((a)\) e~\((b)\). Ademais,
  existe um~\(t\in \{1,\ldots,c\}\) e uma
  subsequência~\((x_{i_p})_{p\ge 0}\) de~\((x_i)_{i\ge 0}\) tais
  que~\(X_{i_p}\) é monocromático com a cor~\(t\). Pela
  Propriedade~\((a)\), todo conjunto~\(C\subset
  \{x_{i_1},x_{i_2},\ldots\}\) está colorido com a cor~\(t\).
  Portanto~\((x_{i_p})\) formam o conjunto~\(Y\) procurado.
\end{prova}

O teorema a seguir é o resultado obtido por~Ramsey em~1927.

\begin{teorema}\label{teo:7.10}[Teorema de~Ramsey, 1927]
	Dados inteiros positivos~\(c,\ t\)
  e~\(k\),
  $c\geq 2$ e $t\geq k \geq1$, existe um natural~\(p \geq k\)
  tal que para todo conjunto~\(X\)
  com~\(p\)
  elementos e uma~\(c\)-coloração
  de~\(\calp_k(X)\), existe um~\(Y\in \calp_t(X)\) monocromático.
\end{teorema}

\begin{prova}
  Suponhamos, por absurdo, que a afirmação do teorema seja falsa para
  alguma tripla~\((c,t,k)\). Assim, para todo~\(p \geq k\) existem um
  conjunto~\(X\) com~\(|X|=p\), o qual podemos supor, sem perda de
  generalidade, que~\(X = [p]\), e uma~\(c\)-coloração
  de~\(\calp_k(X)\) tal que~\(X\) não contém um~\(Y\in \calp_t(X)\)
  monocromático. Denominemos tais colorações de ``ruins''.

  Mostremos que essas colorações ruins induzem uma coloração ruim 
  de \(\calp_k(\mathbb{N}^*)\), onde \(\mathbb{N}^* = \{1,2,\ldots\}\), contrariando o
  Teorema~\ref{teo:7.13}.

	Para todo~\(p \geq k\), seja~\(V_p\) o conjunto das colorações ruins
  de~\(\calp_k([p])\). Claramente, para todo~\(p > k\) temos que
  se~\(g \in V_p\) então a restrição~\(f(g)\) de~\(g\)
  a~\(\calp_k([p-1])\) é uma coloração ruim e, portanto, pertence
  a~\(V_{p-1}\).

	Pelo Lema~\ref{teo:7.12}, existe uma sequência
  infinita~\((g_p)_{p\ge k}\) de colorações ruins~\(g_p \in V_p\) tais
  que~\(f(g_p) = g_{p-1}\) para todo~\(p > k\). Para todo~\(m \geq
  k\), todas as colorações ruins~\(g_p\), com~\(p \geq m\),  coincidem
  sobre~\(\calp_k([m])\), e portanto para cada~\(Y \in
  \calp_k(\mathbb{N}^*)\) o valor~\(g_p(Y)\) coincide para todo~\(p
  \geq \max Y\).

	Defina~\(g(Y)\) como este valor comum~\(g_p(Y)\). Então~\(g\) é uma
  $c$-coloração ruim de~\(\calp_k(\mathbb{N}^*)\). De fato, pois todo
  subconjunto~\(T\subseteq \mathbb{N^*}\) com~\(|T|=t\) está contido
  em algum~\([p]\) (basta tomar~\(p = \max T\)) e, portanto,~\(T\) não
  pode ser monocromático, pois~\(g\) coincide sobre~\(\calp_k([p])\)
  com a coloração ruim~\(g_p\). A existência dessa coloração ruim
  de~\(\calp_k(\mathbb{N}^*)\) contradiz o Teorema~\ref{teo:7.13},
  como queríamos.
	%Logo, a hipótese inicial era falsa, e com isso a prova está completa.
\end{prova}

\begin{comment} %%%%%%%%%%%%%%%%%%%%%%%%% tirado %%%%%%%%%%%%
O seguinte teorema pode ser provado com o auxílio do axioma da
escolha.

\begin{teorema}[Teorema de~Ramsey fortalecido]\label{ramseyfortalecido}
Dados inteiros positivos~\(m,\ r,\ k\), existe um inteiro
positivo~\(n\) tal que para toda~\(r\)-coloração de~\(\calp_k([n])\)
existe um conjunto~\(Y\) monocromático
com~\(|Y|\ge m\) e~\(|Y|> \min\{y:\ y\in Y\}\).
\end{teorema}

\begin{prova}
Fixe~\(m,\ r,\ k\) inteiros positivos. Seja~\(\mathcal{A}\) a família
dos conjuntos finitos~\(A\subset [m,\infty)\) tais que~\(|A|> \min
  A\).

Fixe uma~\(r\)-coloração em~\(\calp_k([m,\infty))\). Pelo
Teorema~\ref{teo:7.13}, existe um conjunto monocromático~\(T\in
\calp_{\infty}([m,\infty))\). Seja~\(Y\) o conjunto formado
pelos~\(\min(T)+1\) primeiros elementos de~\(T\). Então
\[
|Y|=\min(T)+1>\min(T)=\min(Y).
\]
Portanto~\(Y\in \mathcal{A}\). A existência de~\(n\) segue pelo princípio da compacidade.
\end{prova}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Princípio da compacidade

%\begin{teorema}[Princípio da~Compacidade]
%Seja $k$ um natural. Seja $\mathcal{A}$ uma família de subconjuntos finitos de $\mathbb{N}$.
%Suponha que para toda coloração finita de $\mathbb{N}^{(k)}$ existe um $A\in \mathcal{A}$ tal
%que $A^{(k)}$ é monocromático. Então para todo natural $r$ existe um natural $n_0(r)$ tal
%que, se $n\ge n_0(r)$ então para toda $r$-coloração de $[n]^{(k)}$ existe um $A\in \mathcal{A}$
%com $A\subset [n]$ tal que $A^{(k)}$ é monocromático.
%\end{teorema}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

O Teorema da~Incompletude de~Gödel afirma que existe uma
proposição~\(\phi\) na teoria de primeira ordem da aritmética de~Peano
AP tal que se AP for consistente, então nem~\(\phi\) nem a negação
de~\(\phi\) são demonstráveis em AP.

Em~1977, J.~Paris e~L.~Harrington exibiram um exemplo de uma asserção
do tipo~Ramsey que satisfaz o teorema da incompletude de Gödel.

\begin{teorema}[Paris -- Harrington, 1977]
O Teorema de~Ramsey fortalecido não pode ser provada na aritmética de~Peano.
\end{teorema}

\end{comment} %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


Até este momento, estamos sempre trabalhando com um número finito de
cores. Apresentaremos agora um exemplo mostrando que o
Teorema~\ref{teo:7.13} é falso quando colorimos subconjuntos de
cardinalidade infinita.

\begin{proposicao}
 Existe uma~\(2\)-coloração de~\(\calp_{\infty}(\mathbb{N})\) que não
 possui um subconjunto monocromático de cardinalidade infinita.
\end{proposicao}

\begin{prova}
 Construímos uma~\(2\)-coloração~\(c\) tal que para todo~\(M\in
 \calp_{\infty}(\mathbb{N})\) e~\(x\in M\)
 temos~\(c(M\setminus\{x\})\neq c(M)\). Isto é claramente suficiente
 para provar a proposição.

Definimos uma relação~\(\sim\) sobre~\(\calp_{\infty}(\mathbb{N})\)
definida por~\(L\sim M\) se~\(|L\Delta M|<\infty\), onde~\(L\Delta
M=(L\setminus M)\cup(M\setminus L)\). Mostremos que~\(\sim\) é uma
relação de equivalência.
\begin{enumerate}
 \item[i)] Claramente~\(L\sim L\) pois~\(|L\Delta L|=0<\infty\)
para todo~\(L\in \calp_{\infty}(\mathbb{N})\);
 \item[ii)] Claramente~\(L\sim M\) se e somente se~\(M\sim L\),
   pois~\(|L\Delta M|=|M\Delta L|\) para todo~\(L,M\in
   \calp_{\infty}(\mathbb{N})\);
 \item[iii)] Sejam~\(L,\ M,\ N\in \calp_{\infty}(\mathbb{N})\),
   mostremos que se~\(L\sim M\) e~\(M\sim N\) então~\(L\sim N\).

Como~\(|L\Delta M|<\infty\), temos~\(L\setminus M\) e~\(M\setminus L\)
finitos.

Como~\(|M\Delta N|<\infty\), temos~\(M\setminus N\) e~\(N\setminus M\)
finitos.

Assim,
\[
L\setminus N=(L\setminus(N\cup M))\cup((L\cap M)\setminus N)\subset
(L\setminus M)\cup (M\setminus N).
\]
e
\[
N\setminus L=(N\setminus(L\cup M))\cup((M\cap N)\setminus L)\subset
(N\setminus M)\cup (M\setminus L).
\]
Logo~\(L\setminus N\) e~\(N\setminus L\) são finitos. E
portanto~\(|L\Delta N|=|L\setminus N|+|N\setminus L|<\infty\).
\end{enumerate}

Denotemos as classes de equivalências por~\(\{E_i:i\in I\}\). Para
cada~\(i\) escolhemos~\(M_i\in E_i\). Observemos que dado~\(M\in
\calp_{\infty}(\mathbb{N})\) existe único~\(i\in I\) tal que~\(M\sim
M_i\).

Definimos~\(c:\calp_{\infty}(\mathbb{N})\to [2]\) dada por~\(c(M)=1\)
se~\(|M\Delta M_i|\) é par para algum~\(i\in I\) e~\(c(M)=2\)
se~\(|M\Delta M_i|\) é impar para algum~\(i\in I\).

Fixemos~\(M\in \calp_{\infty}(\mathbb{N})\) e~\(x\in
M\). Se~\(c(M)=1\), então~\(|M\Delta M_i|\) é par para algum~\(i\in
I\), logo~\(|M\setminus\{x\}\Delta M_i|\) é
ímpar. Portanto~\(c(M\setminus\{x\})=2\). Analogamente, se~\(c(M)=2\),
então~\(c(M\setminus\{x\})=1\).
\end{prova}

Apresentemos uma aplicação à geometria utilizando o Teorema de~Ramsey,
conhecida como \emph{Happy~Ending Problem}.

\begin{teorema}[Erd\H os -- Szekeres, 1935]\label{ez:35}
Dado~\(m\ge 3\), existe o menor natural~\(f(n)\) tal que, para
todo~\(f(m)\) pontos no plano três a três não-colineares existem~\(m\)
desses pontos que formam um polígono convexo.
\end{teorema}

\begin{exercicio}
Prove o Teorema~\ref{ez:35}. Utilize e prove os seguintes lemas abaixo.
\end{exercicio}

\begin{lema}
Entre cinco pontos do plano três a três não colineares, existem quatro
que formam um quadrilátero convexo.
\end{lema}

\begin{lema}
Se entre~\(m\ge 4\) pontos no plano, três a três não-colineares, cada
quatro formam vértices de um quadrilátero convexo, então os~\(m\)
pontos são os vértices de um polígono convexo.
\end{lema}

Claramente~\(f(3)=3\). É um bom exercício provar que~\(f(4)=5\)
e~\(f(5)=9\). Em~2006, Szekeres e~Peters provaram que~\(f(6)=17\). A
partir desse números, é natural conjecturarmos
que~\(f(m)=2^{m-2}+1\). Em~1961, Erd\H{o}s e~Szekeres provaram
que~\(f(m)\ge 2^{m-2}+1\). A melhor cota superior conhecida é devida a
Tóth e~Valtr, provada em~2005. Quando~\(m\ge 7\),
\[
f(m)\le \binom{2m-5}{m-2}=O\left( \frac{4^m}{\sqrt{m}} \right).
\]

\subsection{Teoremas tipo~Ramsey na combinatória aditiva}
A seguir, veremos uma aplicação do Teorema~\ref{teo:7.10} para obter
um resultado de combinatória aditiva. O resultado a seguir foi provado
por~Issai~Schur em~1916.

\begin{teorema}[Schur, 1916]\label{schur:16}
	Para todo inteiro positivo~\(n \geq 2\) existe um menor
  natural~\(\phi(n)\) tal que para qualquer partição do
  conjunto~\(\{1,\ldots,\phi(n)\}\) em~\(n\) classes, existem
  inteiros~\(x,y,z\) numa mesma classe, tais que~\(x+y = z\).
\end{teorema}

\begin{prova}
	Seja~\((c,t,k) = (n,3,2)\). Pelo Teorema~\ref{teo:7.10}, existe um
  inteiro positivo~\(p\ge 2\) tal que para toda~\(n\)-coloração
  de~\(\calp_{2}([p])\) existe um conjunto~\(Y\subset X\)
  monocromático com~\(|Y|=3\).

	Considere uma~\(n\)-coloração de~\([p]\) em~\(n\) classes~\(X_1,\ldots,X_n\),
	e defina a seguinte partição de~\(\calp_2([p])\).
	\[
		\mathcal{C}_i
		=
		\{ \{a,b\} \in \calp_2([p]) \,:\, |a-b| \in X_i\},
	\]
	para todo~\(1\le i\le n\).

	Como~\((\mathcal{C}_1\ldots,\mathcal{C}_n)\) é uma~\(n\)-coloração
  de~\(\calp_2([p])\), existe um conjunto~\(Y=\{a,b,c\}\) de~\(X\),
  tal que~\(\{a,b\},\{b,c\},\{a,c\} \in \mathcal{C}_i\) para
  algum~\(i\).

	Suponhamos que~\(a > b> c\). Sejam~\(x = b - a\), \(y = c-b\) e~\(z
  = c-a\). Temos~\(x,\ y,\ z\in X_i\) e~\(x+y = z\), como queríamos.
\end{prova}

\begin{exercicio}
	Mostre que para toda~\(2\)-coloração do
  conjunto~\(\{1,2,\ldots,325\}\) existem inteiros positivos~\(x,y,z\)
  distintos de mesma cor tais que~\(y = \frac{1}{2}(x+z)\).
\end{exercicio}
	Motivado pelo estudo do Último~Teorema de~Fermat, Schur demonstrou o
  seguinte teorema.

	\begin{teorema}[Schur, 1916]
	Para todo inteiro positivo~\(n\), existe um primo~\(p_0\) tal que,
  para todo primo~\(p\ge p_0\), a equação
	\[
	x^n+y^n\equiv z^n (\text{mod } p)
	\]
	possui uma solução não trivial em~\(\mathbb{Z}_p\).
	\end{teorema}

	\begin{prova}
	Fixe um inteiro positivo~\(n\). Pelo Teorema~\ref{schur:16}, existe
  um primo~\(p_0\) tal que, para toda~\(n\)-coloração de~\([p_0]\)
  existem~\(x_0,\ y_0,\ z_0\in [p_0]\) de mesma cor tais
  que~\(x_0+y_0=z_0\).

	Fixe um primo~\(p\ge p_0\) qualquer e considere o grupo
  multiplicativo \((\mathbb{Z}_p^*,\cdot)\). Sabemos que este grupo
  possui um gerador~\(g\), isto é, se~\(t\in \mathbb{Z}_p^*\),
  então~\(t=g^m\) para algum inteiro positivo~\(m\). Logo, todo
  elemento~\(t\in \mathbb{Z}_p^*\) pode ser escrito da
  forma~\(t=g^{kn+r}\), onde~\(k\) é inteiro positivo e~\(0\le r\le
  n-1\). Façamos uma~\(n\)-coloração em~\(\mathbb{Z}_p^*\) dada
  por~\(c(t)=r\). Pelo Teorema~\ref{schur:16},
  existem~\(x_1,\ x_2,\ x_3\in \mathbb{Z}_p^*\) tais
  que~\(c(x_1)=c(x_2)=c(x_3)\) e~\(x_1+x_2\equiv x_3 (\text{mod
  }p)\). Assim
	\[
	g^{k_1n+r}+g^{k_2n+r}\equiv g^{k_3n+r} (\text{mod }p).
	\]
	 Como~\(g^r\) é inversível módulo~\(p\), tomando~\(x=g^{k_1}\),
   \(y=g^{k_2}\) e~\(z=g^{k_3}\), obtemos
	\[
	x^n+y^n\equiv z^n (\text{mod }p),
	\]
	como queríamos.
	\end{prova}

	Segue agora um resultado clássico que foi conjecturado por~Schur e
  que foi provado por Bartel~L.~van der~Waerden em~1927.

	\begin{teorema}\label{vdw:27}
	Para todo inteiros positivos~\(r\) e~\(k\) existe um inteiro
  positivo~\(n\) tal que toda~\(r\)-coloração de~\([n]\) contém uma
  progressão aritmética monocromática de comprimento~\(k\).
	\end{teorema}

	\begin{prova}
	Definimos~\(\W(r,k)\) como o menor inteiro positivo~\(n\) que satisfaz o teorema acima.

	Mostremos que~\(\W(r,k)\) está bem definido por indução
  dupla. Claramente para~\(k\le 2\) é trivial, para todo inteiro
  positivo~\(r\). Suponhamos que~\(\W(r,k-1)\) existe para todo
  inteiro positivo~\(r\). Denotemos por~\(A_i=PA(a_i,r_i,l)\) como o
  subconjunto dos inteiros positivos que forma uma progressão
  aritmética de comprimento~\(l\), primeiro elemento~\(a_i\) e
  razão~\(r_i\). As progressões aritméticas~\(A_1,\ldots A_t\) são
  ditas \emph{focadas em }\(z\in \mathbb{Z}\) se~\(a_i+lr_i=z\) para
  todo~\(1\le i\le t\)., e são ditas \emph{focadas em cores} (e com
  foco $z$) se todas
  são monocromáticas, e cada uma está colorido diferentemente das
  outras.

	Mostremos a seguinte afirmação: Sejam~\(k\) e~\(r\) inteiros
  positivos. Para todo~\(s\le r\) existe um inteiro positivo~\(n\) tal
  que para toda~\(r\)-coloração de~\([n]\) existe ou uma progressão
  aritmética de comprimento~\(k\), ou~\(s\) progressões aritméticas
  focadas em cores de comprimento~\(k-1\), todas com o mesmo foco.

	A prova é por indução em~\(s\). Se~\(s=1\), pela hipótese de
  indução, podemos tomar~\(n=2\W(r,k-1)\), que claramente satisfaz a
  afirmação.

	Seja~\(s>1\), e suponhamos, por hipótese de indução, que existe
  um~\(n\) para~\(s-1\). Mostremos que~\(N=2*[2n\W(r^{2n},k-1)]\) é o
  inteiro que satisfará a afirmação para~\(s\). De fato,
  particionaremos~\([N]\) em blocos de comprimento~\(2n\). Pela
  definição de~\(n\), cada bloco contém ou uma progressão aritmética
  de comprimento~\(k\) (e neste caso o resultado segue), ou
  contém~\(s-1\) progressões aritméticas focadas em cores de
  comprimento~\(k-1\), cujo foco dessas progressões pertencem ao mesmo
  bloco.

	Observe que a~\(r\)-coloração em~\([N]\) induz
  uma~\(r^{2n}\)-coloração nos blocos. Pela definição de~\(N\), existe
  uma progressão aritmética
  monocromática~\(\{B(x),B(x+y)\ldots,B(x+(k-2)y)\}\),
  onde~\(B(x+iy)\) são os blocos.

	Seja~\(A_i=PA(a_i,r_i,k-1)\), com~\(1\le i\le s-1\) as~\(s-1\)
  progressões aritméticas focadas em cores em~\(B(x)\), e seja~\(z\) o
  foco. Observe que as seguintes~\(s-1\) progressões aritméticas de
  comprimento~\(k-1\) são focadas em cores no foco~\(z+2niy(k-1)\).
	\[
	\tilde{A}_i=PA(a_i,r_i+2yi,k-1),
	\]
	com~\(1\le i\le s-1\), e~\(PA(z,2yn,k-1)\), isso prova a afirmação.

	Tomando~\(r=s\), se existe uma progressão aritmética de
  comprimento~\(k\), o teorema segue. Se existem~\(s\) progressões
  aritméticas focadas em cores de comprimento~\(k-1\), então uma delas
  é uma progressão aritmética que possui a mesma cor do foco, e logo é
  uma progressão aritmética de comprimento~\(k\), como queríamos.
\end{prova}

 A \emph{hierarquia de~Ackermann} é uma família de
 funções~\(f_n:\mathbb{N}\to \mathbb{N}\) com~\(n\) inteiro positivo
 dadas por~\(f_1(k)=2k\), e~\( f_{n+1}(k)=f_n^{k}(1)\). Note
 que~\(f_2(k)=2^k\) e~\(f_3(k)\) é uma torre de~\(2\) de altura~\(k\).

 A cota superior dada na demonstração acima é muito ruim. De fato, a
 função~\(\W(2,k)\) cresce mais rápido que todas as funções da
 hierarquia de~Ackermann. Mas em~1988, Shelah mostrou uma cota muito
 melhor, a saber,
 \[
 \W(r,k)\le f_4(r+k).
 \]
 Em~2001, Gower encontrou uma cota melhorada para duas cores, a saber,
 \[
 \W(2,k)\le 2^{2^{2^{2^{2^{k+9}}}}}.
 \]
 Graham conjecturou, oferecendo~1000 dólares a quem resolver a
 conjectura, que para todo~\(k\) inteiro positivo, vale
 \[
 \W(2,k)\le 2^{k^{2}}.
 \]
 A conjectura está aberta há mais de~30 anos.

 Berlekamp apresentou uma cota inferior para o número de van~der~Werden. A saber,
 \[
 \W(2,p+1)\ge p2^p,
 \]
 para todo primo~\(p\).

 \begin{exercicio}
 Prove ou dê um contra-exemplo. Para toda~\(2\)-coloração dos naturais
 existe uma progressão aritmética monocromática de comprimento
 infinito.
 \end{exercicio}

 Apresentaremos a seguir o Teorema de van~der~Waerden fortalecido,
 onde a razão é monocromática juntamente com a progressão aritmética.

 \begin{teorema}[Teorema de~van~der~Waerden fortalecido]\label{fvdw}
 Para todo inteiros positivos~\(p\) e~\(k\) , existe um inteiro
 positivo~\(n\) tal que para toda~\(k\)-coloração de~\([n]\) existe
 uma progressão aritmética de comprimento~\(p\) tal que seus elementos
 e sua razão possuem as mesmas cores.
 \end{teorema}

 \begin{exercicio}
 Prove o Teorema~\ref{fvdw} a partir do Teorema~\ref{vdw:27}.
 \end{exercicio}
% \begin{prova}
% Fixe~\(p\) e~\(k\) inteiros positivos quaisquer.
% Mostremos por indução em~\(k\). Para~\(k=1\) o resultado segue trivialmente.
%
% Suponhamos que vale para~\(k-1\), isto é, existe um inteiro positivo~\(n\) tal que
% para toda~\((k-1)\)-coloração de~\([n]\) existe uma progressão aritmética
% de comprimento~\(p\) tal que seus elementos e sua razão possuem as mesmas cores.
%
% Seja uma~\(k\)-coloração de~\([W(n(p-1)+1,k)]\), então, pelo teorema de van der Waerden,
% existe uma progressão aritmética monocromática de tamanho~\(n(p-1)+1\). Denotemos
% os elementos dessa progressão por~\(a,a+d,\ldots,a+n(p-1)d\). Se algum elemento do
% conjunto~\(\{d,2d,\ldots,nd\}\) tiver a mesma cor da progressão, o teorema está satisfeito.
% Caso contrário, temos que~\(\{d,2d,\ldots,nd\}\) está colorido com~\(k-1\) cores, que
% concluimos por indução.
% \end{prova}

 Pode-se deduzir o Teorema de~Schur utilizando o Teorema~\ref{fvdw}
 tomando~\(p=2\), pois obtemos uma progressão aritmética de
 comprimento dois tal que seus elementos e sua razão possuem as mesmas
 cores.

 O resultado mais importante da combinatória aditiva é o Teorema
 de~Szemerédi, o qual foi conjecturado por~Erd\H{o}s e~Turán
 em~1936. A densidade superior é definida por
 \[
 \bar{d}(A)=\limsup_{n\to \infty}\frac{|A\cap[n]|}{n}.
 \]

\begin{teorema}(Szemerédi, 1975)\label{sz:75}
Todo subconjunto dos naturais com densidade superior positiva contém
uma progressão aritmética de comprimento arbitrariamente longo.
\end{teorema}
Em~2001, Gowers exibiu um uma versão quantitativa do
Teorema~\ref{sz:75}.

\begin{teorema}[Gowers, 2001]
Para todo~\(k>0\), todo subconjunto de~\([N]\) de cardinalidade no
mínimo~\(N(\log\log N)^{-c(k)}\) contém uma progressão aritmética de
comprimento~\(k\), onde~\(c(k)=2^{-2^{k+9}}\).
\end{teorema}

Em~1977, Furstenburg exibiu uma prova alternativa do
Teorema~\ref{sz:75} utilizando métodos da teoria ergódica; Nagle, Rödl
e~Schacht, em~2006~\cite{NagleRodlSchacht06}, provaram o resultado
utilizando hipergrafos. O seguinte teorema foi provado por~Green
e~Tao.

\begin{teorema}[Green -- Tao, 2008]\label{GT:08}
Existem progressões aritméticas arbitrariamente longas no conjunto dos
números primos.
\end{teorema}

Note que o resultado não segue do Teorema~\ref{sz:75}, pois
se~\(\pi(n)\) é a quantidade de primos que tem entre~\(1\) e~\(n\),
pelo Teorema dos números~primos,
\[
\pi(n)=\frac{n}{\log n}.
\]
Portanto, se~\(P\) é o conjunto dos números primos,
\[
\bar{d}(P)=\limsup_{n\to \infty}\frac{\pi(n)}{n}=0,
\]
logo~\(P\) não tem densidade superior positiva.

Antes de~Green e~Tao, já haviam resultados sobre progressões
aritméticas de números primos, como o Teorema de~van~der~Corput.

\begin{teorema}[van~der~Corput, 1939]
Existem infinitas progressões aritméticas de comprimento três nno
conjunto dos números primos.
\end{teorema}

Erd\H{o}s e~Turán conjecturaram a seguinte afirmação.

\begin{conjectura}
Se~\(A\) é um subconjunto dos naturais satisfazendo
\[
\sum_{a\in A}\frac{1}{a}=+\infty,
\]
então~\(A\) contém progressões aritméticas de comprimento
arbitrariamente longos.
\end{conjectura}

Observe que Teorema~\ref{GT:08} e o Teorema~\ref{sz:75} satisfazem a
conjectura acima.

%% Local Variables:
%% mode: latex
%% coding: utf-8
%% fill-column: 70
%% tab-width: 2
%% End:
