\section{Emparelhamentos perfeitos}

\begin{teorema}[Hall, 1935]
  \textsl{Seja \G\ um grafo \((A,B)\)-bipartido. Então \G\ tem um
  emparelhamento que cobre \(A\) se e somente se \(\bigl|\adj(X)\bigr|
  \geq \bigl|X\bigr|\) para todo \(X\subseteq A\).}
\end{teorema}

\begin{prova}
  Demonstramos apenas uma das implicações. Suponha que não exista
  emparelhamento que cobre~\(A\), e seja~\M\ um emparelhamento máximo
  de~\G. Então existe vértice~\(v\in A\) que não é coberto por~\M.
  Considere \(A' \cup B'\) (\(A' \subseteq A\), \(B' \subseteq B\)) o
  conjunto de vértices de~\G\ que são atingíveis a partir de \(v\) por
  caminhos \M-alternantes. Como~\G\ é bipartido, todo caminho
  de~\(v\) até~\(a \in A'\) tem comprimento par. Logo, ~\(a\) é
  emparelhado com o vértice que vem exatamente antes dele em todos
  esses caminhos que o atingiram. Assim, todo vizinho de~\(a\) está
  em~\(B'\) e, portanto, \(\adj(A') \subseteq B'\).  Por outro lado, 
\(B'\subseteq \adj(A')\), e portanto, \(\adj(A') =  B'\).   Por hipótese, temos
  que \(|\adj(A')| \geq |A'|\); logo, ~\(|B'| \geq |A'|\) e, assim, como~\(v\in
  A'\) não está coberto por~\M, existe pelo menos um vértice~\(b\)
  em~\(B'\) que não está coberto por~\M. O caminho \M-alternante de
  \(v\) a \(b\) tem comprimento ímpar e, portanto, é aumentador. Pelo
  Teorema~\ref{teo:berge_aumentador}, $M$ não é um emparelhamento máximo, uma contradição.
\end{prova}

\begin{corolario}\label{cor:emparelhamento_|A|-k}
 \textsl{Seja \G\ um grafo \((A,B)\)-bipartido. Se \(|\adj(X)|\geq|X|-k\)
  para todo \(X\subseteq A\) e algum inteiro fixo \(k\), então
  \G\ possui um emparelhamento de cardinalidade~\(|A|-k\).}
\end{corolario}

\begin{prova}[Demonstração (sugestão)]
Adicione~\(k\) vértices a~\(B\), conectados cada um a todos os vértices
de~\(A\).
\end{prova}

Um conjunto~\(S\) de vértices de um grafo~\G\ é dito uma
\defi{cobertura} (por vértices) se toda aresta de~\G\ é incidente a
pelo menos um vértice de~\(S\). O tamanho de uma menor cobertura
de~\G\ é denotado por~\(\cob (G)\). É fácil ver que~\(|S|\geq |M|\),
para qualquer emparelhamento~\(M\). O seguinte teorema estabelece a
célebre relação min--max entre coberturas e
emparelhamentos~\cite{diestel05,bondy2008graph}.

\begin{teorema}[\Konig, 1931]
\textsl{Seja \G\ um grafo bipartido. A cardinalidade de um emparelhamento
máximo de~\G\ é igual à cardinalidade de uma cobertura mínima de~\G.}
\end{teorema}


%\begin{prova} 
%\((\emp G\leq \cob (G))\) 
%Sejam~\M\ um emparelhamento e~\C\ uma cobertura de~\G. É fácil ver
%que~\(|M| \leq |C|\), pois as arestas do emparelhamento são
%independentes, e cada uma incide em algum vértice da cobertura. Em
%particular a desigualdade vale para o emparelhamento máximo e para a
%cardinalidade mínima.
%\end{prova}

%\begin{prova}[Prova alternativa de ~\(\emp G \leq \cob (G)\).]
%Considere o grafo~\(H=(V(G),M)\) e observe que~\(|M|\leq \sum_{v\in C}d_H(v)\leq |C|\), onde a primeira desigualdade decorre de~\(C\) ser uma  cobertura, e a segunda de~\(M\) ser um emparelhamento.
%\end{prova}

\begin{prova}
Demonstraremos que~\(\emp G \geq \cob (G)\). A outra desigualdade é um
exercício. Seja~\G\ um grafo~\((A,B)\)-bipartido e~\C\ uma cobertura
mínimade~\G. Definimos os conjuntos
\begin{equation*}
A_C=A\cap C,\quad
B_C=B\cap C,\quad
A_{\overline{C}}=A\setminus C,\quad\text{e}\quad
B_{\overline{C}}=B\setminus C.
\end{equation*}
%
Seja~\aga\ o subgrafo de~\G\ induzido por~\(A_C\cup
B_{\overline{C}}\). É claro
que~\aga\ é~\((A_C,B_{\overline{C}})\)-bipartido. Mostramos a sequir
que a minimalidade de~\C\ garante que~\aga\ satisfaz a hipótese do
Teorema de Hall.

Seja~\X\ um subconjunto qualquer de~\(A_C\). Note que o conjunto~\(C\setminus
X)\cup \adj_H(X)\) é uma cobertura de~\G (observe que toda aresta que tem uma
ponta em~\X\ tem a outra ponta em~\(\adj_H(X)\)). Como a
cardinalidade dessa cobertura é~\(|C| -|X| + |\adj_H(X)|\), a
minimalidade de~\C\ implica que~\(|\adj_H(X)|\geq |X|\).

Pelo Teorema de Hall, existe um emparelhamento~\F\ em~\aga\ que
cobre~\(A_C\). De maneira análoga, podemos concluir que o
subgrafo~\(H'\) de~\G\ induzido por~\(B_C\cup A_{\overline{C}}\)
possui um emparelhamento~\(F'\) que cobre~\(B_C\). Ademais, \(F\cup
F'\) é um emparelhamento em~\G\ e
%
\[
\emp(G)\geq |F\cup F'|=|F| + |F'| =|A_C| + |B_C| = |C|=  \cob (G). \qedhere
\]
\end{prova}

Dado um grafo~\aga, denotamos por~\defi{\(c_o(H)\)} o número de
componentes (conexas) de~\aga\ que têm um número ímpar de vértices. O
teorema a seguir fornece uma condição para a existência de
emparelhamentos perfeitos em grafos arbitrários em termos do número de
tais componentes.

\begin{exercicio}
  Demonstre o Teorema de Hall de duas formas:
  \begin{enumerate}[i)]
  \item Por indução em $|A|$, dividindo o estudo em dois casos:
    (1).~existe conjunto de vértices~\(S\subseteq A\) com
    \(\bigl|\adj(S)\bigr|=\bigl|S\bigr|\), e (2).~para todo \(S
    \subseteq A\)
    vale \(\bigl|\adj(S)\bigr|>\bigl|S\bigr|\).
  \item Usando o Teorema de \Konig.
  \end{enumerate}
\end{exercicio}

\begin{teorema}[Tutte, 1947]\label{teo:tutte-emp}
 \textsl{ Um grafo \(G=(V,E)\) tem um emparelhamento perfeito se e só se
  \(c_o(G-S)\leq|S|\) para todo \(S\subseteq V\).}
\end{teorema}

\begin{prova}
Em aula. [A ser incluído aqui.]
\end{prova}


\begin{exercicio}
  Deduza o Teorema de Hall do Teorema de Tutte.
\end{exercicio}

\begin{exercicio}
Um \defi{grafo cúbico} é um grafo em que todo vértice possui grau
\(3\). Prove que todo grafo cúbico sem arestas-de-corte tem um
emparelhamento perfeito (\Petersen, 1891). \hint{mostre que tal grafo
  satisfaz a condição do Teorema~\ref{teo:tutte-emp}.}
\end{exercicio}



%% Local Variables:
%% mode: latex
%% coding: utf-8
%% fill-column: 70
%% tab-width: 2
%% End:
