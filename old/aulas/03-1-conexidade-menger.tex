%%Dizemos que dois conjuntos de vértices estão \defi{conectados} quando se
%interceptam ou caso exista um caminho com um extremo em cada. Um
%grafo~\gve\ é conexo se qualquer par de~subconjuntos de~\vdg\ está
%conectado (essa definição é equivalente à que demos na
%seção~\ref{subsec:conectividade}). Um grafo conexo com~\numn\ vértices tem
%ao menos~\(n - 1\) arestas (um caminho é um exemplo). Grafos conexos com
%esse número de arestas são chamados de árvores. Qualquer aresta que
%removemos de uma árvore desconecta o grafo. Isso sugere duas generalizações
%da noção de conexidade.
%


%Comentário: existe prova do teorema de Menger por fluxo-máximo (\emph{max-flow}). 

Seja~\G\ um grafo. Dizemos que~\G\ é~\defi{\(k\)-conexo} se, para
todo~\(S\subseteq V(G)\) e~\(|S|< k\), temos que o grafo~\(G -S\) é
conexo e possui ao menos dois vértices.

Analogamente, dizemos que~\G\ é~\defi{\(k\)-aresta-conexo} se, para
todo~\(F\subseteq E(G)\), com~\(|F| < k\), temos que~\(G-F\) é conexo
e possui ao menos dois vértices.

Além disso, convencionamos que todo grafo é~\(0\)-conexo
e~\(0\)-aresta-conexo.

Finalmente, definimos a \defi{conexidade} de~\G\ (\defi{connectivity}
of~\G), denotada por~\(K(G)\), como o maior natural~\(k\) tal
que~\G\ é~\(k\)-conexo e definimos a \defi{aresta-conexidade}
de~\G\ (\defi{edge connectivity} of~\G), denotada por~\(K'(G)\), como
o maior natural~\(k\) tal que~\G\ é~\(k\)-aresta-conexo.

%DONE discutir se conexidade é K ou kappa

\begin{exercicio}\label{ex:sanduiche-conexidade}
 Mostre que~\(K(G) \leq K'(G) \leq \delta(G)\).
\end{exercicio}

Para provar as várias versões to teorema de Menger, vamos agora
considerar digrafos (grafos orientados), em particular, permitimos
aqui arestas múltiplas. Note que isso é diferente de uma
\defi{orientação} de um grafo, quando partimos de um grafo e
orientamos suas arestas.

Para facilitar a notação, se~\G\ é um grafo (ou digrafo) e~\(s\)
e~\(t\) são dois de seus vértices, então chamamos
de~\defi{\st-caminho} qualquer caminho (orientado, no caso de
digrafos) de~\(s\) a~\(t\).

Um digrafo~\(D\) é dito \defi{fortemente conexo} se para quaisquer
dois vértices~\(u,v\) existe em~\(D\) um~\(uv\)-caminho (Isto
significa que existe também um~\(vu\)-caminho).


\begin{figure}[ht]
\begin{center}
\PoeFigura{exemplo-st-caminho}
\caption{Um~\(st\)-caminho, \ie, um caminho orientado
  de~\(s\) para~\(t\). Os arcos em um tal caminho devem ter ``mesma''
  orientação.}\label{fig:exemplo_st-caminho}
\end{center}
\end{figure}

Além disso, definimos~\(k\)-conexidade forte,~\(k\)-aresta-conexidade
forte,~\(K(D)\) e~\(K'(D)\) analogamente, trocando ``conexo'' por
``fortemente conexo''.

Finalmente, se~\G\ é um grafo conexo (ou digrafo fortemente conexo)
e~\(S \subseteq V(G)\) é tal que~\(G - S\) é desconexo, então dizemos
que~\(S\) é um \defi{conjunto separador} de~\G.

\section{Teorema de Menger}

O teorema de Menger caracteriza a noção de conexidade através de
caminhos disjuntos.

\begin{teorema}[Menger, 1927]
\label{teo:3.1}\label{teo:MengerDigrafosArestas}%
 Seja~\(D=(V,E)\) um digrafo,~\(s,t\in V\), e~\(s\neq t\). O número
 \emph{máximo} de~\st-caminhos em~\(D\) disjuntos nos arcos é igual ao
 número \emph{mínimo} de arcos cuja remoção destroi todos
 os~\st-caminhos.
\end{teorema}

A prova tradicional é por \mencao{maxflow}. Faremos
outra. Se~\(X\subseteq V\), denotamos por~\(\delta^+(X)\) o conjunto
dos arcos de~\(D\) que saem de~\X\ (\ie, têm a cauda em~\X\ e a cabeça
em~\(\compl{X}=V \setminus X\)). Chamamos~\(\delta^+(X)\)
de~\defi{corte de~\X}; dizemos que~\(X\) é
um~\defi{corte~\(st\)-separador} se~\(s\in X\) e~\(t\in \compl{X}\).

\begin{prova}
 A seguinte prova é devido a \AndrasFrank, e usa funções submodulares
 definidas abaixo.

Primeiramente, uma função~\(f:2^X\mapsto \RR\) das partes de~\X\ nos
reais é dita \defi{submodular} se~\(f(U\cup V) + f(U\cap V) \leq
f(U)+f(V)\). Se há igualdade em vez de ``\(\leq\)'' a função é
\defi{modular}, e se há ``\(\geq\)'' a função é \defi{supermodular}.

O teorema~\ref{teo:3.1} é um corolário do seguinte.

\begin{teorema}\label{teo:menger-k-conexo-geral}
 Seja~\(D=(V,E)\) um digrafo,~\(s,t\in V\), e~\(s\neq t\). Em~\(D\)
 existem pelo menos~\(k\)~\st-caminhos disjuntos nos arcos se e só
 se~\(\delta^+(X)\geq k\) para todo~\(X\subseteq V\) que é corte
 \st-separador (\ie,~\(s\in X\) e~\(t\notin X\)).
\end{teorema}

Vamos provar inicialmente que o
teorema~\ref{teo:menger-k-conexo-geral} de fato implica o
teorema~\ref{teo:3.1}.

Seja~\(k\) o número máximo de~\st-caminhos disjuntos nos arcos, então,
pelo teorema~\ref{teo:menger-k-conexo-geral}, existe um~\(X\subseteq
V\) corte \st-separador tal que~\(\delta^+(X) < k + 1\). A remoção
de~\(\delta^+(X)\) certamente desconecta~\(s\) de~\(t\). Por outro
lado a remoção de~\(k-1\) arestas certamente não desconecta~\(s\)
de~\(t\), pois todo corte~\(X\)~\st-separador passa a ter ao menos um
arco em~\(\delta^+(X)\).

Vamos agora provar o teorema~\ref{teo:menger-k-conexo-geral}.

Suponha que~\(D\) é~\(k\)-conexo, então cada corte intercepta ao menos
uma aresta de cada caminho, e os caminhos são disjuntos,
donde~\(|X|\geq k\).

\medskip

\begin{exercicio}
 Mostre que a função~\(f(X)=|\delta^+(X)|\) é submodular.
\end{exercicio}

A prova da recíproca é por indução no número de arcos. Dizemos que um
corte \st-separador é \defi{justo} se~\(|\delta^+(X)|=k\). Suponha
que~\(|\delta^+(X)|\geq k\) para todo corte~\st-separador.

Observe que
\begin{equation*}
 |\delta^+(X)|+|\delta^+(Y)| \geq |\delta^+(X\cup Y)| +
 |\delta^+(X\cap Y)|\geq k+k.
\end{equation*}
Donde segue que se~\X\ e~\(Y\) são justos, então~\(X\cup Y\) e~\(X\cap
Y\) são justos.

Vamos supor que todo arco de~\(D\) pertence a um conjunto justo (senão
podemos deletá-lo e usar a hipótese indutiva).

Tome~\(v\neq s\) e~\X\ tais que~\(vt\in \delta^+(X)\) e~\X\ é justo
(Figura~\ref{fig:teo-menger-1}). Observe que se não existisse tal~\vv,
então todos os arcos de~\(D\) seriam da forma~\(st\) e o resultado
seguiria trivialmente.

Seja~\(S\) a interseção de todos os conjuntos justos~\X\ tais
que~\(\delta^+(X)\) contém~\(vt\) (note que~\(S\) é justo). Vamos
provar que existe~\(u \in S\) com~\(uv \in E(D)\).

\begin{figure}[ht]
\begin{center}
\PoeFigura{teo-menger-1}
\caption{Exemplo de escolha do vértice~\vv\ e do conjunto justo~\X\ da
  demonstração do Teorema~\ref{teo:menger-k-conexo-geral}.}
%DONE legenda e label da imagem 1 do teorema de Menger
\label{fig:teo-menger-1}
\end{center}
\end{figure}

Suponha que não há tal~\(u\) e observe que~\(\delta^+(S\setminus
\{v\})\) deve possuir ao menos~\(k\) arcos pois~\(S \setminus \{v\}\)
é corte~\st-separador, mas isso significa que~\(\delta^+(S)\) possui
ao menos~\(k+1\) arcos, o que contradiz sua definição.

Tomemos então~\(u \in S\) com~\(uv \in E(D)\) e~\(D'\) o digrafo
definido a partir de~\(D\) removendo os arcos~\(uv\) e~\(vt\) e
incluindo um novo arco~\(ut\)
(Figura~\ref{fig:teo-menger-2-splitting}), note que essa operação
poderá gerar arcos múltiplos.

\begin{figure}[ht]
\begin{center}
\PoeFigura{teo-menger-2-splitting}
\caption{À esquerda, um exemplo de digrafo~\(D\) da demonstração do
  Teorema~\ref{teo:menger-k-conexo-geral}. À direita, o digrafo~\(D'\)
  construído.}
%DONE legenda e label da imagem 2 do teorema de Menger
\label{fig:teo-menger-2-splitting}
\end{center}
\end{figure}

Certamente~\(|E(D')| = |E(D)| - 1\). Vamos mostrar que~\(D'\) satisfaz
a hipótese do teorema.

Tome~\(X\) um corte~\st-separador em~\(D'\) arbitrário e suponha por
absurdo que~\(|\delta^+_{D'}(X)| < k\).

Observe que, se~\(u,v \notin X\), então~\(\delta^+_{D'}(X) =
\delta^+_D(X)\), logo~\(|\delta^+_{D'}(X)| \geq k\).

Por outro lado, se~\(u \in X\) e~\(v \notin X\), então também
temos~\(|\delta^+_{D'}(X)| = |\delta^+_D(X)| \geq k\), já que a
remoção do arco~\(uv\) foi compensada pela adição do arco~\(ut\).

O mesmo vale para o caso~\(u, v \in X\), pois a remoção do arco~\(vt\)
foi compensada pela adição do arco~\(ut\).

Resta apenas o caso~\(u \notin X\) e~\(v \in X\). Mas então~\(k >
|\delta^+_{D'}(X)| = |\delta^+_D(X)| - 1 \geq k\),
logo~\(|\delta^+_D(X)| = k\), i.e.~\(X\) é justo em~\(D\). Observe
que~\(vt \in \delta^+_D(X)\), mas~\(X\) não contém~\(u\), o que
contradiz a escolha de~\(S\), pois~\(u \in S\).

Portanto podemos aplicar a hipótese indutiva para~\(D'\), então
temos~\(k\)~\st-caminhos em~\(D'\) disjuntos nos arcos. Para obter
os~\st-caminhos em~\(D\), basta substituir o arco adicionado~\(ut\)
pelos arcos removidos~\(uv\) e~\(vt\).
\end{prova}

%DONE decidir sobre a observação abaixo (sobre referência da prova do teo de Menger). Aparentemente não vem de lá essa prova...
%\begin{observacao}
% É possível que a prova acima venha do \emph{Handbook of Combinatorics}. Lá há também uma prova interessante do teorema de Hall, usando submodularidade.
%\end{observacao}

A partir do teorema~\ref{teo:3.1}, podemos deduzir as seguintes outras
versões.

\begin{teorema}\label{teo:MengerDigrafosVertices}
Seja~\(D=(V,E)\) um digrafo,~\(s,t\in V\),~\(s\neq t\) e~\(st \notin
E\). O número \emph{máximo} de \st-caminhos em~\(D\) internamente
disjuntos nos vértices é igual ao número \emph{mínimo} de vértices
em~\(V \setminus \{s,t\}\) cuja remoção destroi todos os~\st-caminhos.
\end{teorema}

\begin{prova}
Certamente não há como destruir todos os~\st-caminhos removendo menos
vértices do que o número máximo de~\st-caminhos.

Considere~\(D'\) o digrafo tal que~\(V(D') = \{v^+ : v \in V(D)\} \cup
\{v^- : v \in V(D)\} \text{ e } E(D') = \{x^+y^- : xy \in E(D)\} \cup
\{v^-v^+ : v \in V(D)\}\).

Observe que caminhos disjuntos nos arcos em~\(D'\) correspondem a
caminhos internamente disjuntos nos vértices em~\(D\).

Além disso, remover um vértice~\vv\ de~\(D\) corresponde a remover o
arco~\vmenos\vmais\ de~\(D'\).

Logo o teorema~\ref{teo:3.1} aplicado a~\(D'\) nos dá o resultado
para~\(D\).
\end{prova}

\begin{teorema}\label{teo:MengerGrafosArestas}
Seja~\(G=(V,E)\) um grafo,~\(s,t\in V\), e~\(s\neq t\). O número
\emph{máximo} de \st-caminhos disjuntos nas arestas em~\(D\) é igual
ao número \emph{mínimo} de arcos cuja remoção destroi todos os
\st-caminhos.
\end{teorema}

\begin{prova}
Seja~\(D\) o digrafo obtido a partir de~\(G\) trocando cada
aresta~\(uv\) pelos arcos~\((u,v)\) e~\((v,u)\). Porém, não basta
apenas aplicar o teorema para o digrafo e remover a orientação dos
arcos, pois nada impede que ambos os arcos do par sejam usados, cada
uma por um caminho diferente.

Para resolver esse problema, consideramos o digrafo induzido pelos
arcos contidos nos caminhos e adicionamos~\(k\) arcos da forma~\(ts\),
observe que todo vértice desse digrafo possui grau de entrada igual ao
grau de saída e que essa propriedade é mantida se eliminarmos todos os
pares de arcos~\((u,v)\) e~\((v,u)\) sucessivamente correspondentes a
orientações opostas de uma mesma aresta.

%DONE colocar teoremas sobre grafos e digrafos eulerianos na introdução
%DONE colocar referência do teorema de digrafos eulerianos no parágrafo abaixo
%DONE definir seções de caminhos na introdução, há (mais) um uso abaixo

Ao final desse procedimento, obtemos um digrafo~\(D'\) euleriano pelo
teorema~\ref{teo:digrafoeuleriano}. Cada seção da trilha euleriana
entre duas ocorrências sucessivas de arcos da forma~\(ts\) corresponde
a um dos~\(k\)~\st-caminhos.
\end{prova}

\begin{teorema}\label{teo:MengerGrafosVertices}
Seja~\(G=(V,E)\) um grafo,~\(s,t\in V\),~\(s\neq t\) e~\(st \notin
E\). O número \emph{máximo} de \st-caminhos\ em~\(G\) internamente
disjuntos nos vértices é igual ao número \emph{mínimo} de vértices
em~\(V \setminus \{s,t\}\) cuja remoção destroi todos os~\st-caminhos.
\end{teorema}

\begin{prova}
Análoga à prova do teorema~\ref{teo:MengerDigrafosVertices}, mas
usando o teorema~\ref{teo:MengerGrafosArestas} ao invés do
teorema~\ref{teo:3.1}.
\end{prova}

%DONE decidir se é necessário colocar o teorema que relaciona
%k-conexidade com existência de k caminhos entre todos os pares de
%vértices ou se a versão entre um par fixo já é suficiente. (E a mesma
%coisa para arestas e para versão orientada.)

A partir dos teoremas~\ref{teo:MengerDigrafosArestas},
\ref{teo:MengerDigrafosVertices}, \ref{teo:MengerGrafosArestas}
e~\ref{teo:MengerGrafosVertices}, seguem os seguintes corolários.

\begin{corolario}\label{teo:menger-k-arco-fort-conexo}
Um digrafo~\(D\) é~\(k\)-fortemente-arco-conexo se e somente se para
todo par de vértices distintos~\(v\) e~\(w\) existem~\(k\) caminhos
orientados disjuntos nos arcos de~\(v\) a~\(w\).
\end{corolario}

\begin{corolario}\label{teo:menger-k-fort-conexo}
Um digrafo~\(D\) é~\(k\)-fortemente-conexo se e somente se para todo
par de vértices distintos~\(v\) e~\(w\) existem~\(k\) caminhos
orientados internamente disjuntos nos vértices de~\(v\) a~\(w\).
\end{corolario}

\begin{corolario}\label{teo:menger-k-aresta-conexo}
Um grafo~\(G\) é~\(k\)-aresta-conexo se e somente se para todo par de
vértices distintos~\(v\) e~\(w\) existem~\(k\) caminhos disjuntos nas
arestas de~\(v\) a~\(w\).
\end{corolario}

\begin{corolario}\label{teo:menger-k-conexo}
Um grafo~\(G\) é~\(k\)-conexo se e somente se para todo par de
vértices distintos~\(v\) e~\(w\) existem~\(k\) caminhos internamente
disjuntos nos vértices de~\(v\) a~\(w\).
\end{corolario}

%% Local Variables:
%% mode: latex
%% coding: utf-8
%% fill-column: 70
%% tab-width: 2
%% End:
