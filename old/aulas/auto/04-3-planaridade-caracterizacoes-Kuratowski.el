(TeX-add-style-hook
 "04-3-planaridade-caracterizacoes-Kuratowski"
 (lambda ()
   (LaTeX-add-labels
    "teo:5.16"
    "teorema de Kuratowski"
    "fig:grafo_com_6_blocos"
    "fig:dem Kuratowski caso planar"
    "fig:dem Kuratowski caso ((a))"
    "it:kur-3viz"
    "it:kur-vizcam"
    "it:kur-2viz"
    "fig:dem Kuratowski caso (a)"
    "fig:dem Kuratowski caso (b)"
    "fig:dem Kuratowski caso (c)"
    "fig:prova 1 kuratowski"
    "teo:5.17"
    "teorema de Wagner"))
 :latex)

