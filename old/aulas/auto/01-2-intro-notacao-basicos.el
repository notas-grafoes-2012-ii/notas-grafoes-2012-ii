(TeX-add-style-hook
 "01-2-intro-notacao-basicos"
 (lambda ()
   (LaTeX-add-labels
    "fig:estrela"
    "fig:exemplo-estrela"
    "fig:exemplo-K5"
    "fig:exemplo-vazio5"
    "fig:exemplo-complementar1"
    "fig:exemplo-complementar2"
    "fig:exemplo-3-regular"
    "fig:exemplo-bipartido"
    "fig:exemplo-subgrafo"
    "fig:exemplo-subgrafo-induzido"
    "fig:exemplo-trilha-aberta"
    "fig:exemplo-trilha-fechada"
    "fig:exemplo-trilha-caminho"
    "fig:exemplo-trilha-circuito"
    "subsec:conectividade"
    "teo:digrafoeuleriano"
    "fig:exemplo-remocao1"
    "fig:exemplo-remocao2"))
 :latex)

