Uma \defi{decomposição arbórea} (d.a.) de um grafo~\(G\) é um
par~\((T,\mathcal{W})\), onde
\begin{itemize}
\item \(T\) é uma árvore e
\item \(\mathcal{W} = \{W_t \subseteq V(G)\,:\, t \in V(T)\}\) é uma
  família de conjuntos de vértices de~\(G\) indexada pelos vértices
  de~\(T\);
\end{itemize}
E são tais que
\begin{enumerate}[(i)]
\item\label{it:decarb_i} O conjunto~\(\mathcal{W}\) cobre~\(V(G)\)
  (i.e., temos~\(\bigcup_{t\in V(T)} W_t = V(G)\)), e toda aresta
  de~\(G\) tem ambos os extremos em algum~\(W_t\) (i.e.,
  temos~\(\bigcup_{t\in V(T)} E(G[W_t]) = E(G)\)); e
\item\label{it:decarb_ii} se~\(t,t',t'' \in V(T)\) e~\(t'\) está no
  (único) caminho de~\(t\) para~\(t''\) em~\(T\) então~\(W_t \cap
  W_{t''} \subseteq W_{t'}\).
\end{enumerate}

A \defi{largura} (width) da decomposição arbórea~\((T,\mathcal{W})\) é
definida como
\[
\max\{|W_t| -1 \,:\, t \in V(T)\}
\]
e sua \defi{ordem} como
\[
\begin{cases}
  0 & \text{se } E(T) = \vazio; \text{ e } \\
  \max\{|W_t \cap W_{t'}| \,:\, t,t' \text{ adjacentes em } T\} &
  \text{caso contrário}
\end{cases}
\]

\emph{Exemplos.}  Considere~\(G = C_5\) com~\(V(G) = \{a,b,c,d,e\}\)
e~\(E(G) = \{ab, bc, cd, de, ea\}\).
\begin{enumerate}[(a)]
\item\label{ex:decarb1} Uma decomposição arbórea de~\(C_5: (T,\{W_t :
  t\in V(T)\})\), onde~\(V(T) = \{t_1,t_2,t_3,t_4\}\), \(E(T) =
  \{t_it_{i+1} : i\in\{1,2,3\}\}\) e~\(W_{t_1} = \{a,b\}, W_{t_2} =
  \{a,b,c\}, W_{t_3} = \{a,b,c,d\}, W_{t_4} = \{a,b,c,d,e\}\).

%DONE: Caminho de comprimento 4 horizontal com vértices t_1,t_2,t_3,t_4

\begin{figure}[ht]
\begin{center}
\PoeFigura{decarb-ex1}
\caption{Exemplo~\ref{ex:decarb1} de decomposição arbórea do~\(C_5\).}
\end{center}
\end{figure}

  Essa decomposição tem largura~\(4\) e ordem~\(4\).
			
\item\label{ex:decarb2} Uma outra decomposição arbórea
  de~\(C_5\):~\((T,\{W_t : t\in V(T)\})\), onde~\(V(T) =
  \{t_1,t_2,t_3,t_4,t_5\}\), \(E(T) = \{t_it_{i+1} :
  i\in\{1,2,3,4\}\}\) e~\(W_{t_1} = \{a,b\}, W_{t_2} = \{a,b,c\},
  W_{t_3} = \{a,c,d\}, W_{t_4} = \{a,d,e\}, W_{t_5} = \{a,e\}\).

%DONE: Caminho de comprimento 5 horizontal com vértices t_1,...,t_5

\begin{figure}[ht]
\begin{center}
\PoeFigura{decarb-ex2}
\caption{Exemplo~\ref{ex:decarb2} de decomposição arbórea do~\(C_5\).}
\end{center}
\end{figure}
			
  Essa decomposição tem largura~\(2\) e ordem~\(2\).
			
\item\label{ex:decarb3} Outra decomposição arbórea
  de~\(C_5\):~\((T,\{W_t : t\in V(T)\})\), onde~\(V(T) =
  \{t_1,t_2,t_3\}\), \(E(T) = \{t_it_{i+1} : i\in\{1,2\}\}\)
  e~\(W_{t_1} = \{a,b,c\}, W_{t_2} = \{a,c,d\}, W_{t_3} = \{a,d,e\}\).

%DONE: Caminho de comprimento 3 horizontal com vértices t_1,t_2,t_3.

\begin{figure}[ht]
\begin{center}
\PoeFigura{decarb-ex3}
\caption{Exemplo~\ref{ex:decarb3} de decomposição arbórea do~\(C_5\).}
\end{center}
\end{figure}
  
  Essa decomposição tem largura~\(2\) e ordem~\(2\).
			
\item\label{ex:decarb4} Outra decomposição arbórea
  de~\(C_5\):~\((T,\{W_t : t\in V(T)\})\), onde~\(V(T)=\{t_1\}\),
  \(E(T) = \vazio\) e~\(W_{t_1} = V(G)\).

%DONE: Vértice t isolado

\begin{figure}[ht]
\begin{center}
\PoeFigura{decarb-ex4}
\caption{Exemplo~\ref{ex:decarb4} de decomposição arbórea do~\(C_5\).}
\end{center}
\end{figure}
			
  Essa decomposição é chamada de \defi{Decomposição Arbórea Trivial} e
  tem largura~\(|V(G)|-1\) e ordem~\(0\). Todos os grafos possuem tal
  decomposição, elas não são de nenhum interesse.
\end{enumerate}

Os exemplos acima foram todos dados com~\(T\) um caminho, mas a
estrutura de árvore pode ser útil para construirmos uma decomposição
de largura pequena, como sugerido pelo exemplo a seguir.

\begin{exemplo}\label{ex:8.2}
Considere o grafo~\(G\) definido por~\(V(G) = \{a,b,c,d,e,f\}\)
e~\(E(G) = \{ab,ad,bc,bd,be,ce,df,ef\}\).

Uma decomposição arbórea de~\(G\) é~\((T,\{W_t : t\in V(T)\})\),
onde~\(V(T) = \{t_1,t_2,t_3,t_4,t_5\}\), \(E(T) = \{t_it_4 :
i\in\{1,2,3\}\}\) e~\(W_{t_1} = \{a,b,d\}, W_{t_2} = \{b,c,e\},
W_{t_3} = \{d,e,f\}, W_{t_4} = \{b,d,e\}\).

\begin{figure}[ht]
\begin{center}
\PoeFigura{decarb-ex8.2}
\caption{Exemplo~\ref{ex:8.2} de decomposição arbórea.}
\end{center}
\end{figure}

%DONE: EX2. página 8.2, desenhar~\(G\) e~\(T\).

Esta decomposição tem largura~\(2\) e ordem~\(2\).
\end{exemplo}

As decomposições arbóreas interessantes são as de largura pequena,
pois muitos algoritmos exploram tal decomposição e levam tempo
exponencial em sua largura.

Claramente é mais difícil conseguirmos uma decomposição arbórea em que
a árvore em questão é um caminho, quando o fazemos, a decomposição
obtida é chamada de \defi{decomposição em caminho}.

Dada uma decomposição arbórea~\((T,\{W_t : t\in V(T)\})\) de~\(G\),
para cada~\(t \in T\) podemos escolher um subgrafo~\(X_t\) de~\(G\)
com conjunto de vértices~\(W_t\) tal que cada aresta de~\(G\) está em
precisamente um desses subgrafos. Tal escolha determina uma partição
de~\(E(G)\) (note que~\(X_t\) não precisa ser necessariamente conexo.

No caso do Exemplo~\ref{ex:8.2}, podemos ter~\(E(X_{t_1}) = \{ab,ad\},
E(X_{t_2}) = \{bc,be,ce\}, E(X_{t_3}) = \{df,ef\}, E(X_{t_4}) =
\{bd,de\}\).

\begin{figure}[ht]
\begin{center}
\PoeFigura{decarb-ex8.2-Xt}
\caption{Partição das arestas do Exemplo~\ref{ex:8.2}.}
\end{center}
\end{figure}

%DONE: Desenhar grafos da página 8.3

Dada uma decomposição arbórea~\((T,\{W_t : t\in V(T)\})\) de~\(G\),
para cada~\(v \in V(G)\) definimos~\(T_v\) como o subgrafo de~\(T\)
gerado por todos os vértices~\(t\) de~\(T\) tais que~\(v \in W_t\).

Note que, das propriedades~\ref{it:decarb_i} e~\ref{it:decarb_ii}
segue que~\(T_v\) é uma árvore. No Exemplo~\ref{ex:8.2},
temos~\(V(T_a) = \{t_1\}, V(T_b) = \{t_1,t_2,t_4\}, V(T_c) = \{t_2\},
V(T_d) = \{t_1,t_3,t_4\}, V(T_e) = \{t_2,t_3,t_4\}, V(T_f) =
\{t_3\}\).

\begin{figure}[ht]
\begin{center}
\PoeFigura{decarb-ex8.2-Tv}
\caption{Subárvores~\(T_v\) relativas ao Exemplo~\ref{ex:8.2}.}
\end{center}
\end{figure}

%DONE: desenhos da pagina 8.3


Dizemos que~\((T,\{W_t : t\in V(T)\})\) é uma \defi{decomposição
  arbórea em cliques} se todo~\(W_t\) induz uma clique em~\(G\). No
Exemplo~\ref{ex:8.2}, a decomposição arbórea que exibimos é uma
decomposição arbórea em cliques.



A \defi{largura arbórea} de um grafo~\(G\) (denotada por~\(\la(G)\)) é
o menor inteiro~\(k\) tal que~\(G\) possui uma decomposição arbórea de
largura~\(k\).

Analogamente, a \defi{largura de caminho} (`path width') de um
grafo~\(G\) (denotada por~\(\pw(G)\)) é o menor inteiro~\(k\) tal
que~\(G\) possui uma decomposição em caminho de largura~\(k\).


\begin{exercicio}
  Seja~\(G\) um grafo simples. Prove que~\(\la(G) \leq 1\) se e
  somente se~\(G\) é uma floresta.
\end{exercicio}

%DONE: definição de grafo série-paralelo (vide 09-3-menores-e-qbo-dec-arb-men-proib.tex)
Um grafo é dito \defi{série-paralelo} se não possui~\(K_4\) como menor.

\begin{exercicio}
  Prove que se~\(G\) é série-paralelo, então~\(\la(G) \leq 2\).
\end{exercicio}

%\begin{observacao}
%  Se~\((T,\mathcal{W})\) é uma decomposição arbórea de um grafo~\(G\), da definição
%  de decomposição arbórea segue que~\((T,\mathcal{W})\) induz uma decomposição sobre
%  cada subgrafo de~\(G\).
%\end{observacao}
% A observação acima é a proposição abaixo.

\begin{proposicao}\label{prop:8.0}
  Seja~\((T,\{W_t : t\in V(T)\})\) uma decomposição arbórea de um
  grafo~\(G\), e~\(H \subseteq G\) (\(H\) um subgrafo de~\(G\)). Então
  tomando~\(\mathcal{W}_H = \{W_t \cap V(H)\,\:\,t \in V(T)\}\), temos
  que~\((T,\mathcal{W}_H)\) é uma decomposição arbórea de~\(H\).
\end{proposicao}

\begin{prova}
  Se~\(t'\in V(T)\) está no caminho entre~\(t\) e~\(t''\),
  então~\(W_{t}\cap W_{t''} \subseteq W_{t'}\), logo~\((W_{t}\cap
  V(T))\cap(W_{t''}\cap V(T)) \subseteq W_{t'}\cap V(T)\).
\end{prova}

%DONE: prova da Proposição~\ref{prop:8.0}.

\emph{Convenção}: Para simplificar, diremos ``\(K \subseteq V(G)\) é
uma clique'', devendo ficar implícito que ``\(G[K]\) é uma clique''.

As proposições abaixo estudam o comportamento dos cliques em
decomposições arbóreas de um grafo.

\begin{proposicao}\label{prop:8.1}
  Seja~\((T,\{W_t : t\in V(T)\})\) uma decomposição arbórea de um
  grafo~\(G\), e seja~\(K \subseteq V(G)\) uma clique. Então
  existe~\(t \in V(T)\) tal que~\(K \subseteq W_t\).
\end{proposicao}

\begin{prova}
  Sejam~\(u,v \in K\) e considere~\(T_u\) e~\(T_v\).

  Claramente~\(V(T_u) \cap V(T_v) \neq \vazio\) (pois existe~\(x\in
  V(T)\) tal que~\(W_x \supseteq \{u,v\}\)).

  Portanto, para todos~\(u,v\in K\), temos~\(V(T_u)\cap
  V(T_v)\neq\vazio\).

  Se~\(|K| \leq 2\), apenas essa propriedade já é suficiente para
  provar a proposição. Então suponha~\(|K| \geq 3\).

  Seja~\(x\in V(T)\) com~\(|W_x\cap K|\) maior possível e suponha por
  absurdo que~\(|W_x \cap K| < |K|\). Então existe~\(v\in K\setminus
  W_x\).

  Seja~\(y\in V(T_v)\) com~\(|W_y\cap W_x\cap K|\) maior possível e
  note que~\(x\neq y\) pois \(x\notin V(T_v)\).

  Pela escolha de~\(x\), temos que~\(|W_y\cap K| \leq |W_x\cap K|\),
  como~\(v\in K\cap W_y\setminus W_x\), então existe~\(u\in K\cap
  W_x\setminus W_y\).

  Seja~\(z\in V(T_u)\cap V(T_v)\) (já vimos que esse conjunto não é
  vazio).

  Se~\(x\) estiver no caminho de~\(y\) a~\(z\) em~\(T\),
  teremos~\(v\in W_y\cap W_z \subset W_x \not\ni v\), um absurdo.

  Se~\(y\) estiver no caminho de~\(x\) a~\(z\) em~\(T\),
  teremos~\(u\in W_x\cap W_z \subset W_y \not\ni u\), também um
  absurdo.

  Seja então~\(z_0\) o vértice mais próximo de~\(z\) do caminho
  de~\(x\) a~\(y\) em~\(T\) (possivelmente teremos~\(z_0 =
  z\)). Observe que~\(z_0\) está também no caminho de~\(x\) a~\(z\),
  logo~\(u\in W_{z_0}\).

  Por outro lado, como~\(z_0\) está no caminho de~\(x\) a~\(y\),
  temos~\(W_x\cap W_y \subset z_0\), logo~\(|W_{z_0}\cap W_x\cap K|
  \geq |W_y\cap W_x\cap K| + 1\), o que contradiz a escolha de~\(y\).
\end{prova}


\begin{proposicao}\label{prop:8.2}
  Se~\(G\) tem uma decomposição arbórea em cliques então~\(G\) tem uma
  decomposição arbórea em cliques~\((T,\{W_t : t\in V(T)\})\) onde
  cada~\(W_t\) é uma clique maximal.
\end{proposicao}

\begin{prova}
  Seja~\((T, \mathcal{W})\), com~\(\mathcal{W} = \{W_t : t\in V(T)\}\)
  uma decomposição arbórea de~\(G\) em cliques com~\(|V(T)|\)
  mínimo. Suponha que existe~\(t_1 \in V(T)\) tal que~\(W_{t_1}\) não
  é uma clique maximal. Seja~\(K\) uma clique maximal tal que~\(K
  \supseteq W_{t_1}\). Pela Proposição~\ref{prop:8.1}, existe~\(t_2\in
  V(T)\) tal que~\(W_{t_2} \supseteq K\).
	
  Seja~\(t_3\) um vértice adjacente a~\(t_1\) no caminho em~\(T\)
  de~\(t_1\) a~\(t_2\) (possivelmente temos~\(t_3 = t_1\)) e observe
  que~\(W_{t_1}\subset W_{t_3}\).

  Tome~\(T' = T/\{t_1,t_3\}\) e~\(\mathcal{W}'= \{W_t\,:\, t\in V(T)
  \setminus \{t_1,t_3\}\} \cup \{W_{t_0}\}\), onde~\(W_{t_0} =
  W_{t_3}\) e~\(t_0\) é o vértice obtido da contração
  de~\(\{t_1,t_3\}\).

  Observe que~\((T',\mathcal{W}')\) é uma decomposição arbórea
  de~\(G\) em cliques com~\(|V(T')| < |V(T)|\), o que contradiz a
  escolha de~\((T,\mathcal{W})\).
\end{prova}


A proposição abaixo mostra que a árvore de uma decomposição arbórea
transfere a propriedade de separação para o grafo que ela decompõe.

\begin{proposicao}\label{prop:8.3}
  Seja~\((T,\{W_t : t\in V(T)\})\) uma decomposição arbórea de um
  grafo~\(G\). Seja~\(e = t_1t_2\) uma aresta de~\(T\) e sejam~\(T_1\)
  e~\(T_2\) as componentes conexas de~\(T-e\) com~\(t_1 \in V(T_1)\)
  e~\(t_2 \in V(T_2)\) (\(T_1\) e~\(T_2\) são árvores). Sejam também
  \[
  U_1 = \bigcup_{t \in V(T_1)} W_t \qquad U_2 = \bigcup_{t \in V(T_2)}
  W_t.
  \]
  Então~\(W_{t_1} \cap W_{t_2} = U_1 \cap U_2\) e~\(W_{t_1} \cap
  W_{t_2}\) separa~\(U_1\) de~\(U_2\) em~\(G\).
\end{proposicao}

\begin{prova}
  Sejam~\(t \in V(T_1), t' \in V(T_2)\) e observe que todo caminho
  de~\(t\) para~\(t'\) em~\(T\) contém~\(t_1\) e~\(t_2\), logo temos
  \begin{align*}
  W_t \cap W_{t'} & \subseteq W_{t_1}, \text{ e} \\
  W_t \cap W_{t'} & \subseteq W_{t_2}; \\
  \end{align*}
  o que significa que
  \[
  W_t \cap W_{t'} \subseteq W_{t_1}\cap W_{t_2}.
  \]

  Portanto, para todos~\(t \in T_1\) e~\(t' \in T_2\), temos~\(W_t
  \cap W_{t'} \subseteq W_{t_1}\cap W_{t_2}\).

  Segue então que
  \[
  U_1 \cap U_2 = \left(\bigcup_{t \in V(T_1)} W_t \right) \cap
  \left(\bigcup_{t' \in V(T_2)} W_{t'}\right) \subseteq W_{t_1} \cap
  W_{t_2}.
  \]
  
  Por outro lado, como~\(W_{t_1} \subseteq U_1\) e~\(W_{t_2}\subseteq
  U_2\), temos~\(W_{t_1} \cap W_{t_2} \subseteq U_1 \cap U_2\).

  Portanto~\(U_1\cap U_2 = W_{t_1}\cap W_{t_2}\).
  
  Para mostrar que~\(W_{t_1} \cap W_{t_2}\) separa~\(U_1\) de~\(U_2\),
  vamos mostrar que não existe aresta~\(u_1u_2\) em~\(G\) com~\(u_1
  \in U_1 \setminus U_2\) e~\(u_2 \in U_2 \setminus U_1\).
  
  Se houvesse tal aresta~\(u_1u_2\) em~\(G\), então existiria um~\(t
  \in V(T)\) tal que~\(W_t \supseteq \{u_1,u_2\}\). Mas~\(u_1 \in U_1
  \setminus U_2\) e~\(u_1 \in W_t\) implicam que~\(t \in V(T_1)
  \setminus V(T_2)\) e~\(u_2 \in U_2 \setminus U_1\) e~\(u_2 \in W_t\)
  implicam que~\(t \in V(T_2) \setminus V(T_1)\), o que seria uma
  contradição.
\end{prova}

\begin{proposicao}\label{prop:8.4}
  Seja~\((T,\{W_t : t\in V(T)\})\) uma decomposição arbórea de um
  grafo~\(G\), e seja~\(Y \subseteq V(G)\). Então
  \begin{enumerate}[(a)]
  \item ou existe~\(t \in V(T)\) tal que~\(Y \subseteq W_t\),
  \item ou existem vértices~\(y_1,y_2 \in Y\) e uma aresta~\(t_1t_2\)
    de~\(T\) tais que~\(y_1,y_2 \notin W_{t_1}\cap W_{t_2}\) e
    \(y_1,y_2\) são separados por~\(W_{t_1}\cap W_{t_2}\) em~\(G\).
  \end{enumerate}
\end{proposicao}

%TODO: prova da Proposicao~\ref{prop:8.4}.

%% Local Variables:
%% mode: latex
%% coding: utf-8
%% fill-column: 70
%% tab-width: 2
%% End:
