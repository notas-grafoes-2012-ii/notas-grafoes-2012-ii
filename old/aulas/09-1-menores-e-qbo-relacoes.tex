%DONE: decidir se vamos manter o parágrafo de objetivo de capítulo
%\emph{Objetivo}: ``Provar'' (dar uma idéia da prova) o ``Minor
%Theorem'' [Robsertson \& Seymour]: ``Em todo conjunto infinito de
%grafos existem dois tais que um é menor do outro.''

\section{Relações}
Uma relação~\(R\) sobre um conjunto~\(X\) é um subconjunto
de~\(X\times X\). Para enfatizar que estamos tratando de relações e
não simplesmente de conjuntos, usamos a notação infixa~\(xRy\) para
denotar~\((x,y)\in R\).

Fixada uma relação~\(R\) sobre um conjunto~\(X\), dizemos que~\(R\) é
\begin{itemize}
\item \defi{Reflexiva}, se~\(xRx\) para todo~\(x\in X\);
\item \defi{Transitiva}, se~\(xRy\) e~\(yRz\) implicam~\(xRz\) para
  todos~\(x,y,z\in X\);
\item \defi{Antissimétrica}, se~\(xRy\) e~\(yRx\) implicam~\(x = y\),
  para todos~\(x,y\in X\);
\item \defi{Total}, se para todos~\(x,y\in X\), temos~\(xRy\)
  ou~\(yRx\);
\item \defi{Bem-fundada} (well-founded), se para todo subconjunto não-vazio~\(Y\)
  de~\(X\), existe um~\(y\in Y\) tal que nenhum~\(z\in Y\)
  satisfaz~\(zRy\) (\(Y\) tem um elemento minimal).
\end{itemize}

Em particular, uma relação é chamada de
\begin{itemize}
\item \defi{Quase-ordem}, se for reflexiva e transitiva;
\item \defi{Ordem parcial}, se for reflexiva, transitiva e
  antissimétrica;
\item \defi{Ordem total} (ou \defi{ordem}), se for reflexiva,
  transitiva, antissimétrica e total;
\item \defi{Quase-boa-ordem}, se for reflexiva, transitiva e
  bem-fundada;
\item \defi{Boa-ordem parcial}, se for reflexiva, transitiva,
  antissimétrica e bem-fundada;
\item \defi{Boa-ordem total} (ou \defi{boa-ordem}), se for reflexiva,
  transitiva, antissimétrica, total e bem-fundada.
\end{itemize}

Estamos particularmente interessados em estudar quase-boas-ordens e,
nesse contexto, é mais simples trabalhar com uma noção equivalente
(sob o axioma da escolha) de relações bem-fundadas:

\begin{proposicao}
Assumindo o axioma da escolha, uma relação~\(R\) sobre um
conjunto~\(X\) é bem-fundada se e somente se para toda sequência
infinita enumerável~\((x_i)_{i\in\NN}\), existem índices~\(i < j\)
tais que~\(x_i R x_j\).
\end{proposicao}

A partir deste ponto, assumiremos verdadeiro o axioma da escolha
implicitamente.

Dada uma relação~\(R\) sobre um conjunto~\(X\), um
subconjunto~\(A\subset X\) é dito uma~\defi{anticadeia} se não
existem~\(x,y\in A\) distintos tais que~\(xRy\).

Usaremos frequentemente o símbolo~\(\preceq\) no estudo de relações
(tipicamente quase-ordens) e assumiremos sempre que as
relações~\(\prec\), \(\succeq\) e~\(\succ\) estão definidas como:
\begin{itemize}
\item Para todos~\(x,y\), temos~\(x \prec y\) se e somente
  se~\(x\preceq y\) e~\(\neg(y\preceq x)\);
\item Para todos~\(x,y\), temos~\(x \succeq y\) se e somente
  se~\(y\preceq x\);
\item Para todos~\(x,y\), temos~\(x \succ y\) se e somente
  se~\(x\succeq y\) e~\(\neg(y\succeq x)\);
\end{itemize}

Supondo que~\(\preceq\) é uma quase-ordem, uma sequência~\((x_i)_{i\in
  U}\) indexada por~\(U\subset\NN\) é dita
\begin{itemize}
\item \defi{Crescente}, se para todos índices~\(i < j\), temos~\(x_i
  \preceq x_j\);
\item \defi{Decrescente}, se para todos índices~\(i < j\), temos~\(x_j
  \succeq x_i\);
\item \defi{Estritamente crescente}, se para todos índices~\(i < j\),
  temos~\(x_i \prec x_j\);
\item \defi{Estritamente decrescente}, se para todos índices~\(i <
  j\), temos~\(x_j \succ x_i\).
\end{itemize}



\begin{proposicao}\label{prop:9.1}
  Seja~\(\preceq\) uma quase-ordem sobre~\(X\). Então~\(\preceq\) é
  quase-boa-ordem se e somente se~\(X\) não contém nem uma anticadeia
  infinita nem uma sequência infinita estritamente decrescente~\(x_0
  \succ x_1 \succ \cdots\).
\end{proposicao}

\begin{prova}
  Observe que se~\(A\) é uma anticadeia infinita, então qualquer
  sequência~\((x_i)_{i\in\NN}\) em~\(A\) de elementos distintos é tal
  que não existem~\(i < j\) com~\(x_i \preceq x_j\).

  Por outro lado, se~\((x_i)_{i\in\NN}\) é uma sequência infinita
  estritamente decrescente, então também temos que não existem~\(i <
  j\) com~\(x_i \preceq x_j\).

  Portanto, se~\(\preceq\) é quase-boa-ordem, então~\(X\) não contém
  nem uma anticadeia infinita nem uma sequência infinita estritamente
  decrescente.

  \medskip
	
  Seja~\(x_0, x_1, \ldots, \) uma sequência qualquer de~\(X\) indexada
  pelos naturais. Considere o grafo completo (infinito)~\(K_{\NN}\)
	
  Faça uma coloração das arestas~\(ij\) de~\(K_{\NN}\), com~\(i < j\)
  com~\(3\) cores:
  \begin{itemize}
  \item Atribua a cor \emph{verde}, se~\(x_i \preceq x_j\);
  \item Atribua a cor \emph{amarela}, se~\(x_i\) e~\(x_j\) são
    incomparáveis;
  \item Atribua a cor \emph{vermelha}, se~\(x_i \succ x_j\).
  \end{itemize}
  Pelo Teorema de Ramsey, sabemos que~\(K_{\NN}\) tem um subgrafo
  completo~\(H\) infinito cujas arestas são todas da mesma cor. Pela
  hipótese da proposição, tais arestas não podem ser nem amarelas e
  nem vermelhas.

  Portanto, são verdes. Logo, quaisquer dois vértices~\(i,j\) (com~\(i
  < j\)) de~\(H\) são tais que~\(x_i \preceq x_j\) (bastava uma tal
  aresta).

  Concluímos que~\(\preceq\) é uma quase-boa-ordem.
\end{prova}
%DONE prova nas notas

\begin{corolario}\label{cor:9.2}
  Se~\(\preceq\) é quase-boa-ordem sobre $X$, então toda sequência infinita
  em~\(X\) tem uma subsequência infinita crescente.
\end{corolario}

Uma quase-ordem~\(\preceq\) sobre~\(X\) induz uma quase-ordem natural
sobre o conjunto dos subconjuntos finitos de~\(X\) (denotado
por~\([X]^{<\omega}\)). Essa quase-ordem é definida abaixo.

Para conjuntos finitos~\(A,B \subseteq X\), fazemos~\(A \preceq B\) se
existe uma função injetora~\(f\:A\rightarrow B\) tal que~\(a \preceq
f(a)\) para todo~\(a \in A\).

O seguinte lema e a ideia de sua prova têm um papel importante na
teoria da quase-boa-ordem.

%DONE escrever página 9.1,9.2

\begin{lema}\label{lem:9.3}
  Se~\(\preceq\) é quase-boa-ordem sobre~\(X\), então~\(\preceq\) é
  quase-boa-ordem sobre~\([X]^{<\omega}\).
\end{lema}

%DONE prova nas notas

\begin{prova}
  Suponha que~\(\preceq\) seja uma quase-boa-ordem sobre~\(X\) e
  que~\(\preceq\) não seja quase-boa-ordem sobre~\([X]^{<\omega}\).
	
  Chamaremos de sequências \emph{ruins} aquelas que violam a
  propriedade de serem bem-fundadas, isto é,
  sequências infinitas~\((x_i)_{i\in\NN}\) que são ou estritamente
  decrescentes ou tais que~\(\{x_i : i\in\NN\}\) é uma anticadeia.
  Naturalmente, chamaremos de sequências \emph{boas} aquelas que não
  são ruins.

  Vamos construir uma sequência ruim~\((A_n)_{n\in\NN}\) especial
  em~\([X]^{< \omega}\) recursivamente.
	
  Dado~\(n \in \NN\), suponha, indutivamente, que~\(A_i\) foi definido
  para todo natural~\(i < n\), e que exista uma sequência ruim
  em~\([X]^{<\omega}\) começando com~\(A_0, A_1, \ldots, A_{n-1}\)
  (para~\(n = 0\), temos que uma sequência ruim existe por~\(\preceq\)
  não ser quase-boa-ordem sobre~\([X]^{<\omega}\))

  Escolha então~\(A_n \in [X]^{< \omega}\) com~\(|A_n|\) mínimo e de
  forma que exista uma sequência ruim em~\([X]^{<\omega}\) começando
  com~\(A_0, A_1, \ldots, A_n\).
	
  Claramente, temos que ~\(A_n \neq \vazio\) para todo~\(n\in\NN\) (caso
  contrário teríamos~\(A_n \preceq A_{n+1}\)). Escolha então, para cada
  \(n \in \NN\), um elemento~\(a_n \in A_n\) e tome \(B_n =
  A_n\setminus\{a_n\}\).

  Pelo Corolário~\ref{cor:9.2}, a sequência \((a_n)_{n\in\NN}\) possui
  uma subsequência infinita crescente \((a_{n_i})_{i\in\NN}\).

  Considere a sequência de conjuntos
  \[
  (C_i)_{i\in\NN} = (A_0, A_1, \ldots A_{n_0 -1}, B_{n_0}, B_{n_1}, \ldots)
  \]
  Note que esta sequência é boa por construção (se a sequência fosse
  ruim, como~\(|B_{n_0}| < |A_{n_0}|\), teríamos uma contradição à escolha
  de~\(A_{n_0}\)).

  Portanto, existe um par de índices~\(i_0 < j_0\) tal que~\(C_{i_0} \preceq
  C_{j_0}\).

  Observe que não podemos ter~\(j_0 < n_0\), caso contrário,
  teríamos~\(A_{i_0} \preceq A_{j_0}\).

  Também não podemos ter~\(i_0 < n_0 \leq j_0\), caso contrário,
  teríamos~\(A_{i_0} \preceq B_{j_0} \preceq A_{j_0}\).

  Finalmente, se tivermos~\(n_0 \leq i_0\), então, como~\(a_{i_0}
  \preceq a_{j_0}\), temos que~\(B_{i_0}\preceq B_{j_0}\), o que 
  implica que \(A_{i_0}\preceq A_{j_0}\) (basta estender a função
  de~\(B_{i_0}\) para~\(B_{j_0}\) fazendo a imagem de~\(a_{i_0}\)
  ser~\(a_{j_0}\)). Isso é uma contradição.

  Portanto~\(\preceq\) é quase-boa-ordem sobre~\([X]^{<\omega}\).
\end{prova}

% pagina 9.3

Seja~\(\mathcal{G}\) a classe de todos os grafos \emph{finitos} a
menos de isomorfismo e~\(\preceq\) a relação de `menor (de)' definida
sobre~\(\mathcal{G}\). É fácil ver que~\(\preceq\) é quase-ordem
sobre~\(\mathcal{G}\) (é inclusive uma ordem parcial).

O `Minor Theorem' afirma que
\begin{center}
	``A relação~\(\preceq\) é quase-boa-ordem sobre~\(\mathcal{G}\).''
\end{center}

Como obviamente uma sequência estritamente decrescente de menores não
pode ser infinita, a Proposição~\ref{prop:9.1} nos dá que o `Minor
Theorem' é equivalente a sequinte afirmação:
%DONE verificar se a referência acima está correta, foi corrigida de prop:91

\begin{center}
  ``Não existe uma anticadeia infinita em~\(\mathcal{G}\).''
\end{center}
Isto é, não existe um conjunto infinito de grafos em~\(\mathcal{G}\),
dois-a-dois incomparáveis quanto à relação menor~\(\preceq\).

Restringindo-nos à classe das árvores, existe uma versão mais forte do
`Minor Theorem':

\begin{teorema}\label{teo:minorarv}
  Seja~\(\mathcal{T}\) a classe das árvores \emph{finitas} a menos de
  isomorfismo e~\(\preceq\) a relação de menor topológico (\(\preceq
  \equiv \preceq_t\)). Então~\(\preceq\) é quase-boa-ordem
  sobre~\(\mathcal{T}\).
\end{teorema}

A prova será baseada numa relação mais forte definida sobre árvores
enraizadas.

Se~\(T\) é uma árvore e~\(r\) um vértice qualquer fixo, chamaremos o
par~\((T,r)\) de \emph{árvore enraizada}, e~\(r\) sua \emph{raiz}
(algumas vezes escrevemos simplesmente~\(T\), em vez de~\((T,r)\).

Dadas árvores enraizadas~\((T,r)\) e~\((T',r)\), escrevemos
\[
(T,r) \preceq (T',r')
\]
se existe um isomorfismo~\(\varphi\) entre alguma subdivisão de~\(T\)
e uma subárvore~\(T''\) de~\(T'\) tal que o caminho de~\(r'\)
a~\(\varphi(r)\) em~\(T'\) não contém nenhum outro vértice de~\(T''\)
a não ser~\(\varphi(r)\). Dizemos que um tal isomorfismo
\emph{respeita}~\(r'\).

Seja~\(\mathcal{T}^*\) o conjunto das árvores enraizadas finitas.

Provaremos então o teorema abaixo, do qual o
Teorema~\ref{teo:minorarv} segue como corolário.

%DONE: o teorema abaixo parece ser de Kruskal (verificar)
\begin{teorema}[Kruskal, 1960~\cite{Kruskal60}]\label{teo:minorarvenr}
  A relação~\(\preceq\) é quase-boa-ordem sobre~\(\mathcal{T}^*\).
\end{teorema}

\begin{prova}
  Novamente adotaremos a terminologia de sequências \emph{ruins} para
  as que violam a propriedade de boa-fundação das relações e de
  sequências \emph{boas} para as que não são ruins.

  Suponha que~\(\preceq\) não seja quase-boa-ordem
  sobre~\(\mathcal{T}^*\) e vamos construir uma sequência
  ruim~\((T_n,r_n)_{n\in\NN}\) especial em~\(\mathcal{T}^*\)
  recursivamente.

  Dado~\(n \in \NN\), suponha indutivamente que~\((T_i,r_i)\) já foi
  construído para todo~\(i < n\), e que exista uma sequência ruim
  começando com~\((T_0,r_0), (T_1,r_1), \ldots, (T_{n-1},r_{n-1})\).

  Escolha então~\((T_n,r_n)\) com~\(|V(T_n)|\) mínimo e de forma que
  exista uma sequência ruim começando com~\((T_0,r_0), (T_1,r_1),
  \ldots, (T_n,r_n)\) (para~\(n = 0\), temos que uma sequência ruim
  existe por~\(\preceq\) não ser quase-boa-ordem).

  Observe que~\(|V(T_n)| > 1\) para todo \(n\in\NN\)).

  Para cada~\(n\in\NN\), seja~\(A_n\) o conjunto das árvores
  enraizadas~\((T,r)\) tal que~\(T\) é um componente conexo
  de~\(T_n-r_n\) e~\(r\) é adjacente a~\(r_n\) em~\(T_n\).

  Seja~\(A = \bigcup_{n \in \NN} A_n\).

  Vamos provar que~\(\preceq\) é quase-boa-ordem sobre~\(A\).

  Seja~\(((U_k,s_k))_{k\in\NN}\) uma sequência qualquer de árvores
  enraizadas do conjunto~\(A\).

  Para cada~\(k\in \NN\) escolha~\(n = n(k)\) tal que \(U_k \in A_n\)
  e seja~\(k^* = \argmin\{n(k) : k\in \NN\}\) (\(n(k^*) \leq n(k)\)
  para todo~\(k \in \NN\)).

  Então a sequência
  \[
  (W_n,t_n)_{n\in\NN} = ((T_0,r_0),\ldots,
  (T_{n(k^*)-1},r_{n(k^*)-1}), (U_{k^*},s_{k^*}),
  (U_{k^*+1},s_{k^*+1}), \ldots)
  \]
  é boa, pois \(U_{k^*} \subsetneq T_{n(k^*)}\) (caso
  contrário, teríamos uma contradição com a minimalidade
  de~\(|V(T_{n(k^*)})|\)).

  Ou seja, existem índices~\(i < j\) tais que~\((W_i,t_i) \preceq
  (W_j,t_j)\).

  Observe que não podemos ter~\(j \leq n(k^*) -1\), caso contrário,
  teríamos~\((T_i,r_i)\preceq (T_j,r_j)\).

  Também não podemos ter~\(i < n(k^*) \leq j\), caso contrário,
  teríamos~\((T_i,r_i)\preceq (U_j,s_j)\preceq (T_{n(j)},r_{n(j)})\)
  (pois~\(U_j\in A_{n(j)}\)).

  Logo, temos~\(n(k^*) \leq i\), ou seja,
  temos~\((U_i,s_i)\preceq (U_j,s_j)\), o que significa que a
  sequência~\((U_n,s_n)_{n\in\NN}\) é boa.

  Portanto~\(\preceq\) é quase-boa-ordem sobre~\(A\).

  Pelo Lema~\ref{lem:9.3}, temos que~\(\preceq\) é quase-boa-ordem
  sobre~\([A]^{<\omega}\).

  Em particular, isso significa que existem índices~\(i_0 < j_0\) tais
  que~\(A_{i_0} \preceq A_{j_0}\).

  A partir da função~\(f\: A_{i_0}\to A_{j_0}\), das subdivisões e
  isomorfismos que respeitam raiz que testemunham~\(A_{i_0} \preceq
  A_{j_0}\), segue que~\((T_{i_0},r_{i_0})\preceq (T_{j_0},r_{j_0})\).

  Portanto~\(\preceq\) é quase-boa-ordem sobre~\(\mathcal{T}^*\).
\end{prova}

%% Local Variables:
%% mode: latex
%% coding: utf-8
%% fill-column: 70
%% tab-width: 2
%% End:
