\section{Grafos Perfeitos}

Seja~\(\omega (G) = \) cardinalidade de uma clique máxima
em~\(G\). Claramente~\(\chi (G) \geq \omega (G) \). Mas esse limitante não
é justo, pois há grafos~\G\ que apresentam~\(\chi (G) \gg \omega
(G)\). A existência de tais grafos foi provada por Erdös em 1959, 
usando argumentos probabilísticos. 


\begin{teorema}[\Erdos, 1959]\label{teo:girth_and_chromatic_number}
Dados~\(k,\ell\in\bbn\), existe um  grafo~\G\ tal que~\(\chi(G)>k\)
e~\(\cintura(G)> \ell\).
\end{teorema}

A prova a seguir usa a desigualdade de \Markov, que fornece um limitante
para a probabilidade de uma variável aleatória não-negativa~\(X\) assumir
valores grandes relativamente ao seu valor esperado~\(\esp[X]\). Lembramos
que o \defi{valor esperado}, ou~\defi{esperança} de uma variável
aleatória~\(X\), que assume valores em~\(\calx\), é uma média dos
valores~\(x\in\calx\) ponderada pela probabilidade~\(\Prob[X=x]\) dos
eventos~``\(X\) assume o valor~\(x\)''. Em símbolos,
\[
\esp[X] =\sum_{x\in \calx} x\Prob[X=x].
\]

\begin{lema}[Desigualdade de \Markov]
  Seja~\(X\) uma variável aleatória não-negativa e~\(t>0\). Temos
\[
\Prob[X\geq t]\leq \frac{\esp[X]}{t}.
\]
\end{lema}

\begin{prova}
\[
\esp[X]=\sum_a a\Prob[X=a]\geq \sum_{a\geq t}t\Prob[X=a]=t\Prob[X\geq t].
\qedhere
\]

\end{prova}

\begin{prova}[Demonstração do Teorema~\ref{teo:girth_and_chromatic_number}]
\[
\esp[X] \leq \sum_{j=3}^{\ell} n^jp^j
=\sum_{j=3}^{\ell} n^{\lambda j}
\leq \frac{n^{\lambda\ell}}{1-n^{-\lambda}}. 
\]

Como~\(\lambda\ell<1\), a esperança é menor do que~\(n/4\) para~\(n\)
suficientemente grande. Pela desigualdade de~\Markov, temos~\(\Prob[X\geq
n/2] < 1/2\). Note que ainda é possível que haja circuitos de comprimento
menor do que~\(\ell\) em~\G (que chamaremos de circuitos curtos). 

Consideramos agora o número cromático de~\G. Faremos isso por meio do
número de independência~\(\alpha(G)\), isto é, o tamanho do maior conjunto
independente em~\G.  Como toda classe de cor forma um conjunto
independente, temos que~\(\chi(G)\geq
|V(G)|/\alpha(G)\). Tomamos~\(a=\lceil 3p^{-1}\ln n\rceil\) e consideramos
o evento representando o evento ``existe um conjunto independente de
tamanho~\(a\) em~\G''. Como a probabilidade da união de eventos é limitada
pela soma da probabilidade dos eventos (desigualdade da união), temos
\[
\Prob[\alpha(G)\geq a]
\leq \binom{n}{a}(1 -p)^{\binom{a}{2}}
\leq n^ae^{-pa(a -1)/2}
\leq n^an^{-3(a -1)/2}\to 0.
\]
Para~\(n\) suficientemente grande, esta probabilidade é menor do
que~\(1/2\). Assim, mais uma vez pela desigualdade da união, temos
\[
\Prob[X\geq n/2\text{ ou }\alpha(G)\geq a] <1.
\]
Logo, existe um grafo $G$ (com $n$ vértices) em que o número de circuitos curtos é menor do
que $n/2$ e cujo número de independência é menor do que $a$). 
Isso implica que podemos remover um vértice (arbitrário) de cada
circuito curto, obtendo um grafo~\(G'\)
com ao menos~\(n/2\)
vértices. Ademais,~\(G'\)
não possui circuito de comprimento menor ou igual a~\(\ell\), ou seja, 
$G'$ tem cintura maior do que~$\ell$, 
e~\(\alpha(G')<a\). Logo, 
\[
\chi(G')\geq \frac{|V(G')|}{\alpha(G')}\geq \frac{n/2}{3n^{1-\lambda}\ln n}
=\frac{n^\lambda}{6\ln n}.
\]

Assim, se garantirmos que $\frac{n^\lambda}{6\ln n} > k$, temos o
resultado desejado. Claramente, para $n$ suficientemente grande essa
desigualdade é satisfeita. Com isso, concluímos a prova do teorema. 

\end{prova}

\begin{corolario}
Seja~\aga\ um grafo, que não é uma floresta. Para todo~\(k\in\bbn\),
existem grafos~\G\ com~\(\chi(G)\geq k\) tais que~\(H\not\subseteq G\).
\end{corolario}

Um resultado (assintótico) mais recente nessa direção foi apresentado por
Osthus, Prömel and Taraz~\cite{OsthusPromelTaraz01}. Fixados~\(\ell\geq 3\)
e~\(k\in \bbn\), existem constantes~\(C_1\) e~\(C_2\) tais que quase todo
grafo com~\(n\) vértices,~\(m\) arestas e cintura maior ou igual a~\(\ell\)
possui número cromático maior ou igual a~\(k\), dado que~\(C_1n\leq m\leq
C_2n^{\ell/(\ell -1)}\). (A expressão ``quase todo'' aqui é usada para
dizer que a fração dos grafos com~\(n\) vértices e~\(m\) arestas para os
quais vale a propriedade tende a~\(1\) quando~\(n\) tende a infinito.)

 
Grafos com \mencao{cintura} (\mencao{girth}) grande ``parecem árvores''
localmente, e não parecem ter uma estrutura local que os obrigue a ter
número cromático alto. Esses grafos comprovam que número cromático alto
pode ocorrer por razões de ``natureza global'' e não ``local''.  O
interesse por grafos onde essa dependência global não ocorre, isto é,
grafos cujo número cromático alto é apenas por causa de alguma razão local
(por exemplo, existência de uma clique grande) ocupa assim o outro extremo
no estudo da relação entre~\(\chi (G)\) e~\(\omega (G)\).
 
Uma pergunta natural é se existem grafos não-completos~\G\ com número
cromático~\(\chi (G) = \omega (G)\), e a resposta é afirmativa. Um exemplo é
o grafo~\gve, com~\(V=\{1,2,3,4,5,6\}\) e
arestas~\(E=\{12,23,34,45,56,13\}\). Outro exemplo é o circuito~\(C_4\) com
quatro vértices. A menos de sua existência, não são conhecidas muitas
propriedades desses grafos.  Em 1960, Claude Berge introduziu uma outra
classe relacionada, porém com mais estrutura e mais interessante. Trata-se
da classe dos \mencao{grafos perfeitos}. Dizemos que um grafo~\(G\) é
\defi{perfeito} se cada um de seus subgrafos induzidos~\(H\)
satisfaz~\(\chi (H) = \omega (H)\). Os grafos perfeitos constituem uma
classe onde a exigência~\(\chi (G) = \omega (G)\) é levada ao extremo: um
grafo é dito~\defi{perfeito} se todo subgrafo~\(H\subseteq G\)
satisfaz~\(\chi(H)=\omega(H)\).  Ao contrário do que pode parecer, a classe
dos grafos perfeitos é grande, e seu estudo envolve uma série de problemas
interessantes. Em particular a \emph{Conjectura Forte dos Grafos Perfeitos}
(CFGP), de Berge, permaneceu em aberto por~\(40\)~anos. Ela foi demonstrada
por Chudnovsky, Robertson, Seymour e Thomas \cite{Chudnovsky06thestrong}
%[The strong perfect graph theorem, Ann Math 164 (2006) 51-229]
 
\begin{teorema}[Chudnovsky, Robertson, Seymour, Thomas, 2006]\label{teo:TFGP}
Um grafo é perfeito se e somente se nem~\(G\) nem~\(\overline{G}\) contém
um circuito ímpar de comprimento maior ou igual a~\(5\) como subgrafo
induzido.
\end{teorema}


Um importante resultado, anterior à prova dessa conjectura, é o
Teorema fraco dos grafos perfeitos (Teorema~\ref{teo:carac_perf_Lovasz}), provado por~\Lovasz\ em~\(1972\),
conhecido como o Teorema dos Grafos Perfeitos, cuja prova exibimos nestas
notas.

\begin{teorema}[\Lovasz, 1972]\label{teo:4.11}\label{teo:carac_perf_Lovasz}
Um grafo~\(G\) é perfeito se e somente se~\(\overline{G}\) é perfeito.
\end{teorema}

%DONE definir substituição de vértice v do grafo G por grafo H
Dado um vértice~\(v\) de um grafo~\(G\) e um grafo~\(H\), definimos o
\textbf{grafo da substituição de~\(v\) por~\(H\) (em~\(G\))} como o
grafo~\(G'\) tal que~\(V(G') = V(H) \cup V(G-v)\) e~\(E(G') = E(H)
\cup \{xy : x\in V(H), y\in N_G(v)\} \cup E(G-v)\) (assumindo
que~\(V(G)\cap V(H) = \vazio\).

\begin{teorema} \label{teo:4.10}
Se~\(G\) é perfeito, substituindo-se um vértice de~\(G\) por um grafo
perfeito obtém-se um grafo perfeito.
\end{teorema}

\begin{prova}
Seja~\(x\) um vértice qualquer de~\(G\) e~\(H\) um grafo perfeito e~\(G'\)
o grafo que resulta da substituição de~\(x\) por~\(H\).  É suficiente
mostrar que~\(\chi (G') = \omega (G')\), já que para os subgrafos induzidos
de~\(G'\) que são construídos da mesma forma que~\(G'\) o resultado segue
analogamente. Faremos a prova por indução em~\(k\deq \omega (G')\).
Suponha~\(k > 1\) (se~\(k = 1\) o resultado é óbvio).

É suficiente encontrar um conjunto independente~\(I\) de~\(G'\) que
intercepta todos as cliques com~\(k\) elementos.  (Note que podemos colorir
esses vértices com a mesma cor e os vértices restantes com outras~\(k-1\)
cores,  o que pode ser feito por hipótese, obtendo assim
uma~\(k\)-coloração de~\(G'\).)

Sejam~\(m\deq \omega(G)\), \(l\deq \omega(H)\) e ainda~\(p\) a
cardinalidade máxima de uma clique em~\(G\) que contém~\(x\). Note que~\(k =
\max\{m,l+p-1\}\).  Considere uma~\(m\)-coloração de~\(G\) e chame de~\(X\)
o conjunto dos vértices de~\(G\) que possuem a mesma cor que~\(x\).
Por fim, seja~\(L\) o conjunto dos vértices independentes de~\(H\) que intercepta
toda clique de~\(H\) com \(l\) elementos.  

O conjunto~\(I \deq L \cup (X - \{x\})\) é independente em~\(G'\), e~\(I\)
intercepta toda clique de~\(G'\) com~\(k\) elementos.  De~fato, se~\(K\) é
uma clique com~\(k\) elementos de \(G'\) que intercepta~\(H\), então~\(K\)
contém uma clique com~\(l\) elementos de~\(H\). Em particular, isso implica
que~\(K\) contém um vértice de~\(L\). Por outro lado, se~\(K\) não
intercepta~\(H\), então~\(K\) deve ser uma clique com~\(m\) elementos
de~\(G -x \), e assim~\(K\) contém um vértice de~\(X - \{x\}\).
\end{prova}


\begin{prova}[Demonstração do Teorema~\ref{teo:carac_perf_Lovasz}]
  A prova é por indução no número de vértices do grafo. Para
  $|V(G)|\leq 3$ o resultado é óbvio. Seja $G$ um grafo perfeito com
  pelo menos 4~vértices.  Para provar que~\(\overline{G}\)
  é perfeito é suficiente provar que \(\overline{G}\)
  contém um conjunto independente que intercepta todas as cliques
  máximas de~\(\overline{G}\).
  (Esse fato vale, usando a hipótese de indução para subgrafos de
  $G$.) Ou seja, é suficiente provar que existe clique~\(K\)
  em~\(G\) tal que
\[
\alpha (G-K) < \alpha (G).
\]
(Lembramos que $\alpha (G)$ denota a cardinalidade de um conjunto
independente máximo de~$G$.)
Suponha que isto não ocorra: Para toda clique~\(K\) de $G$ existe um conjunto
independente máximo que é disjunto de~\(K\).  Seja~\(K_1, K_2, \ldots,
K_r\) uma enumeração de todas as cliques de~\(G\). Para cada~\(i = 1, \ldots, r\),
denote por~\(I_i\) um conjunto independente máximo que é disjunto de
\(K_i\). Para cada vértice~\(x\) de~\(G\), seja~\(\theta (x)\) o número de
conjuntos independentes~\(I_i\) que contêm~\(x\).

Seja~\(G'\) o grafo obtido de $G$ substituindo~\(x\) por~\(K_{\theta
  (x)}\), para todo~\(x \in V(G)\). (Observamos que um grafo completo
é um grafo perfeito, e portanto $G'$ é perfeito.) Note que uma clique máxima de~\(G'\)
vem de uma clique, digamos~\(K_i\), obtida de um \textit{join} de~\(K_{\theta
  (x)}\) para cada~\(x \in V(K_i)\).

\[
\omega (G') 
= 
\sum_{x\in V(K_i)} \theta (x) 
=
\sum_{j = 1}^r |I_j \cap K_i| 
\leq
r - 1,
\]
ou seja, temos~\(r \geq \omega (G') + 1\). Por outro lado,
\begin{equation}\label{**}
|V(G')| 
=
\sum_{x \in V(G)} \theta (x)
=
\sum_{i = 1}^r |I_i|
=
r \cdot \alpha (G).
\end{equation}

Como~\( \chi (G') \geq |V(G')|/\alpha(G')\),
usando~\ref{**} e o fato de que $\alpha(G')= \alpha(G)$, temos que
\[
\chi (G') 
\geq
\frac{|V(G')|}{\alpha (G')}
=
\frac{|V(G')|}{\alpha (G)}
=
r
\geq
\omega (G') + 1,
\]
isto é, \(\chi (G') > \omega (G')\), uma contradição pois~\(G'\) é
perfeito.
\end{prova}

\begin{observacao}
Para todo grafo~\(G\), sabemos que
\[
\chi (G) 
\geq
\max\left\{\frac{|V(G)|}{\alpha (G)},~ \omega (G) \right\}.
\]
Se~\(G\) é perfeito, então para todo subgrafo induzido~\(H
\subind G\) vale~\(\chi (H) \geq |V(H)|/\alpha (H)\). Como
\(H\) é perfeito, temos~\(\omega (H)=\chi (H)\), e portanto~\(\omega
(H)\geq|V(H)|/\alpha (H)\), ou seja,
\[
|V(H)| \leq \alpha (H) \cdot \omega (H).
\]

Assim, temos que~\(G\) perfeito implica que para todo subgrafo induzido~\(H
\subind G\), vale~\(|V(H)| \leq \alpha (H) \omega (H)\).
\Hajnal~\&~\Simonovits\ conjecturaram que vale a recíproca da afirmação acima.
Em 1972, \Lovasz\ provou esse fato.
\end{observacao}

%DONE: comando para subgrafo e subgrafo induzido
\begin{teorema} [Lovász, 72] \label{teo:4.12}
Seja~\(G\) um grafo. Se para todo subgrafo induzido~\(H
\subind G\) vale~\(|V(H)| \leq \alpha (H) \omega (H)\), então $G$ é
perfeito. 
\end{teorema}

%TODO: verificar a afirmação abaixo (independência e cliques vs grafos perfeitos)
Note que o Teorema~\ref{teo:4.11} segue como corolário do Teorema~\ref{teo:4.12}. De fato, suponha que $G$ é perfeito. Tomando~\(\overline{H} \subind
\overline{G}\), temos que 
\[
|V(\overline{H})|
=
|V(H)|
\leq
\alpha (H) \omega(H)
=
\omega(\overline{H}) \alpha (\overline{H}).
\]
Portanto, $\overline{G}$ é perfeito. 

%% Local Variables:
%% mode: latex
%% coding: utf-8
%% fill-column: 70
%% tab-width: 2
%% End:
