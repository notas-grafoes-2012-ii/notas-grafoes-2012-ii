\section{Um algoritmo para achar um emparelhamento máximo}

\begin{figure}[ht]
\begin{center}
\PoeFigura{emparelhamento-exemplo-de-flor}
\caption{Um
  emparelhamento~\(M=\bigl\{\{v_1,v_2\},\{v_3,v_4\},\{v_5,v_6\}\bigr\}\)
  e uma flor~\(v_0v_1\cdots v_7\).}\label{fig:passeio_com_circuito_impar}
\end{center}
\end{figure}

O Teorema~\ref{teo:berge_aumentador} sugere que podemos construir um
emparelhamento máximo iterativamente. Partimos de um emparelhamento
qualquer e enquanto houver um caminho~\M-aumentador~\P, substituímos o
emparelhamento corrente~\M\ por~\(M\sdiff P\). Descrevemos nessa seção
algumas estratégias que podem ser usadas para encontrar um tal caminho.

Seja~\(G=(V,E)\) um grafo, \M\ um emparelhamento em~\G, e~\X\ o
conjunto dos vértices livres (de~\M). Um~\emph{passeio
  \hbox{\M-alternante}}~\( v_0,v_1,\ldots,v_t \) é chamado
de~\defi{\M-flor} se satisfaz
(na Figura~\ref{fig:passeio_com_circuito_impar}
considere o passeio $v_0,v_1,\ldots,v_7$):
\begin{enumerate}[a)]
\item \(v_0\in X\);
\item \(v_0,\ldots, v_{t -1}\) são distintos;
\item \(t\) é ímpar, e \(v_t=v_i\) para algum \(i\) par, $i<t$.
\end{enumerate}

%Podemos construir o
%digrafo~\(\Gchapeu=(V,A)\) onde~\( A = \{ uw \,:\, \exists\, v \in V
%\text{ tal que } uv \in E \setminus M \text{, e } vw \in M \}\). Um
%caminho em~\Gchapeu\ é um passeio \M-alternante em~\G. 


Seja \(T=v_0v_1\cdots v_k\) um passeio~\M-alternante em~\G\ entre
vértices distintos de~\X. Se~\T\ é um caminho, então \T\ é um
caminho \hbox{\M-aumentador.} Caso contrário, seja~\(j\) o menor inteiro tal
que~\(v_i=v_j\), para~\(i < j\). É fácil ver que~\(v_0v_1\cdots v_j\)
é uma~\M-flor. A parte da~\M-flor de~\vindice0\ a~\vindice i é 
chamada~\defi{caule} (``\defi{stem}'') e a parte de~\vindice i
a~\vindice t é chamada de \defi{botão} (``\defi{blossom}'') ou
\defi{$M$-botão}. Dizemos
que~\vindice 0 é a~\defi{raiz} da flor ou do botão. Se~\B\ é
um~\M-botão, definimos o grafo~\defi{\(G/B\)}
(chamado~\G\ \defi{contraído} de~\B), com
emparelhamento~\mencao{\(M/B\)}, como o grafo que resulta da
substituição do botão~$B$ por um vértice. Mais formalmente, temos
\begin{itemize}
\item \(V(G/B)=(V\setminus B)\cup\{b\}\), onde~\(b\notin V\) é um
  vértice novo;
\item 
  \(E(G/B)= \bigl(E\setminus\{e\in E: \text{\(e\) incide em
    \B}\}\bigr) \,\cup\, \bigl\{vb:v\in V(G/B), vz \in E(G), z\in B
  \bigr\}\);
\item \(M/B = \bigl(M \setminus \{e\in E:\text{\(e\) incide em
  \B}\}\bigr) \,\cup\, \bigl\{vb:v\in V(G/B), vz \in M, z\in B
  \bigr\}\).
\end{itemize}
Note que~\(\bigl\{vb:v\in V(G/B), vz \in M, z\in B \bigr\}\) possui
apenas uma aresta.

\medskip

O Teorema~\ref{teo:berge_aumentador} diz que se não houver caminho
\M-aumentador, então~\M\ é um emparelhamento máximo, e todo passeio
\M-alternante entre vértices distintos de~\X\ possui uma flor. A
utilidade  da operação de \defi{contração}, definida acima, fica
aparente pelo enunciado do teorema a seguir. 





%% Se existe um caminho \M-aumentador~\(P=v_0v_1\cdots v_k\) para~\G, então podemos
%% construir um caminho orientado \(\hat P=v_0v_3\cdots
%% v_{k -1}\) em~\Gchapeu, entre~\(v_0\in X\) e~\(v_{k -1} \in N(X
%% -v_0)\). A recíproca, contudo, não é verdade --- tome, por exemplo, um
%% caminho em~\Gchapeu\ correspondente a um passeio em~\G\ que contenha
%% um circuito ímpar, como na 
%% figura~\ref{fig:passeio_com_circuito_impar}.


\begin{teorema}[\Edmonds, 1965]\label{teo:2.11}
Seja~\gve\ um grafo, \M\ um emparelhamento em~\G\ e~\B\ um
\M-botão. Então~\M\ é um emparelhamento máximo em~\G\ se e somente se
\(M/B\) é um emparelhamento máximo em~\(G/B\).
\end{teorema}

\begin{prova}
  Vamos provar que existe um caminho~\M-aumentador em~\G\ se e somente
  se existe um caminho~\(M/B\)-aumentador em~\(G/B\).
	
  Seja~\F\ a flor de botão~\B. Para todo vértice~\(v\) de~\B, denote
  por~\(P_v\) o caminho de comprimento par em~\F\ que vai de~\(v\) até
  a raiz de~\B.
  
  Note que se existe um caminho~\M-aumentador~\P\ em~\G\ que não
  possui aresta de~\B, então~\P\ também é um caminho aumentador
  em~\(G/B\).

  Por outro lado, se existe um
  caminho~\M-aumentador~\P\ em~\G\ contendo arestas de~\B, então
  existe um caminho~\M-aumentador com extremo na raiz de~\B. De fato,
  tome~\vu\ um extremo de~\(P\) diferente da raiz de~\B\ e~\vv\ o
  primeiro vértice do botão em~\(P\) quando seguimos~\(P\) partindo
  de~\vu. Seja~\(Q\) o subcaminho de~\(P\) de~\vu\ até~\vv. O
  caminho procurado é dado pela união de~\(Q\) com~\(P_v\).
  
  Observe que a aresta de~\vv\ em~\(Q\) não é aresta de~\M\ e,
  portanto,~\((Q\cup P)/B\) é um caminho~\(M/B\)-aumentador
  em~\(G/B\).
  
  \medskip

  Suponha então que exista um caminho~\(M/B\)-aumentador~\(P\)
  em~\(G/B\). Se~\(P\) não contém~\(b\) então~\(P\) é um
  caminho~\M-aumentador em~\G. Se~\(P\) contém~\(b\) então
  seja~\vu\ um extremo de~\(P\) diferente da raiz de~\B\ e~\(v \in
  V(G)\) o primeiro vértice do botão em~\(P\). Como antes, tome~\(Q\)
  o caminho em~\(P\) de~\vu\ a~\vv\ e então~\(Q \cup P_v\) é um
  caminho~\M-aumentador em~\G.
\end{prova}
  
%TODO encontrar uma boa prova da outra implicação
%DONE revisar o formalismo
\begin{prova}[(Prova alternativa de uma das implicações.)] 
  Suponhamos que~\(M/ B\) não seja um emparelhamento máximo em~\(G/
  B\). Seja~\(N\) um emparelhamento máximo em~\(G/ B\),
  temos~\(|N|>|M/ B|\).

  Considere o emparelhamento~\(N^{+}=\tilde N\cup \tilde M\),
  onde~\(\tilde N\) é um emparelhamento em~\(G\) que não possui
  arestas de~\(B\) e tal que~\(N/ B = \tilde N\) e~\(\tilde M\) é um
  emparelhamento quase-perfeito em~\(B\) compatível com~\(\tilde N\),
  isto é, existe exatamente um vértice em~\(B\) livre de~\(\tilde M\)
  e~\(N^{+}\) é um emparelhamento em~\(G\). Temos

  \[ |N^{+}|=|N|+|\tilde M|>|M/ B|+ |\tilde M|=|M|. \]

  Portanto~\(|N^{+}|>|M|\), uma contradição. 
\end{prova}

%%   \medskip

%%   Mostremos agora a recíproca. Seja~\(M'=M\Delta P\), onde~\(P\) é o 
%%   caule do botão~\(B\). Claramente~\(M'\) é um emparelhamento 
%%   e~\(|M'|=|M|\). Notemos que~\(M'/ B\) é um emparelhamento 
%%   máximo em~\(G/ B\).

%%   Mostremos que~\(M'\) é um emparelhamento máximo em~\(G/ B\).

%%   Suponha que não seja, então, pelo teorema de Berge, existe um
%%   caminho~\(M'\)-aumentador, digamos~\(Q\) com ambos os extremos
%%   livres, digamos~\(u\) e~\(v\).

%%   Claramente~\(Q\) intersecta~\(B\), de fato, se~\(Q\) não
%%   intersecta~\(B\), então~\(Q\subset (G/ B)\). Se trocar as arestas
%%   de~\(Q\), isto é, \(Q\Delta (M'/ B)\), então~\(Q\Delta M'/ B\) é um
%%   emparelhamento e~\(|Q\Delta M'/ B|> |M'/ B|\), o que contradiz a
%%   maximalidade de~\(M'/B\).

%%   Notemos que um dos vértices livres de~\(Q\) não pertence a~\(B\),
%%   pois~\(B\) é um botão. Suponhamos~\(u\notin B\), onde~\(u\) é um dos
%%   vértices livres de~\(Q\).

%%   Seja~\(w\) o primeiro vértice de~\(Q\) que intersecta~\(B\), e
%%   seja~\(Q'\) a secção de~\(Q\) que vai de~\(u\) a~\(w\). Então~\(Q'\)
%%   é um caminho~\(M'/ B\)-aumentador em~\(G/ B\), uma contradição.
  
%%   Portanto~\(M'\) é um emparelhamento máximo em~\(G/ B\), donde segue
%%   que~\(M\) é um emparelhamento em~\(G\).
%\end{prova}


\begin{observacao}
  Note que nem todo circuito ímpar hipoemparelhado é um botão. Além
  disso, se~\(C\) é um circuito ímpar, e~\(M/C\) é um emparelhamento
  máximo em~\(G/C\), então \emph{não}
  necessariamente~\M\ emparelhamento máximo em~\G; onde a contração é
  definida analogamente.
\end{observacao}

%O teorema~\ref{} sugere que uma possível estratégia para a construção de um algoritmo para encontrar emparelhamento máximos é
%Um algoritmo para encontrar um emparelhamento máximo pode buscar 

O Teorema~\ref{teo:2.11} motiva o seguinte algoritmo para encontrar um
emparelhamento máximo partindo de um grafo~\G\ com emparelhamento~\M.
Buscamos passeios \M-alternantes entre vértices distintos de~\X
(conjunto dos vértices livres). Se
não existe tal passeio, o emparelhamento é máximo, pelo
Teorema~\ref{teo:berge_aumentador}.
%
Se encontramos um tal passeio~\P\ \emph{sem} flor, aplicamos o algoritmo
a~\G\ com emparelhamento~\(M'=M\sdiff P\).
%
Se encontramos um passeio \emph{com} uma flor de botão~\B, aplicamos o
algoritmo a~\(G/B\) com emparelhamento~\(M'=M/B\).

O procedimento acima atinge um grafo~\G\ com emparelhamento máximo.
%Note que~\G\ pode conter vértices que resultaram de contrações. O
%teorema~\ref{teo:2.11} garante que podemos desfazer as contrações,
%mantendo a maximalidade do emparelhamento.
Uma vez que \G\ pode conter vértices resultantes de contrações, podemos
usar o Teorema~\ref{teo:2.11} para desfazê-las preservando a maximalidade do
emparelhamento. Na próxima seção descrevemos o algoritmo que esboçamos.

\section{Algoritmo de~\Edmonds--\Gallai}\label{sec:AlgoritmoEdmonds}

Dado um emparelhamento~\M\ em~\G, desejamos encontrar um emparelhameno
maior do que~\M, ou constatar que~\M\ é máximo. No processo, rotulamos
os vértices do grafo, de modo a obter a decomposição de
\Edmonds--\Gallai.

Para encontrar o emparelhamento, fazemos uso de caminhos
alternantes. A grosso modo, partimos de um emparelhamento~\M, e
construímos uma floresta \M-alternante, a partir de algum vértice não
coberto (raiz). A árvore~``cresce'' por meio da adição de arestas do
emparelhamento. Nesse processo, rotulamos os vértices da
árvore. Quando não pudermos prosseguir, o algoritmo termina, e as
classes de vértices definidas pelos rótulos (ou sua ausência) definem
as componentes da~\mencao{decomposição
  de~\Edmonds--\Gallai}~\ref{teo:decomposicao-edmonds-gallai}.


Existem outros algoritmos para encontrar emparelhamentos
máximos.\pagenote{\input{notas/algoritmos_para_emparelhamento}}


Nosso objetivo é construir uma floresta \M-alternante \F, e rotulamos
seus vértices \vpar\ ou \vimpar. Seja \X\ o conjunto de vértices não
cobertos por \M. Inicialmente rotulamos de \vpar\ os vértices de
\X. Cada vértice de \X\ é raiz de uma das árvores de~\F. O crescimento
de \F\ é sempre feito a partir de um vértice \vpar, digamos~\vu. Temos
os seguintes casos.

\emph{Caso 1.}\quad Existe uma aresta \(uv\) onde \vv\ não está
rotulado. Rotulamos \vv\ de \vimpar\ e o seu companheiro \vw\ (\(vw\in
M\)) de~\vpar.

\emph{Caso 2.}\quad Existe uma aresta \(uv\) com \vv\ rotulado
\vpar\ tal que \vv\ pertence a uma árvore distinta da que
\vu\ pertence. Neste caso, encontramos um caminho \M-aumentador
\(R\deq P(uv)Q\), onde \(P\) é o caminho em \F\ de~\(r_u\), raiz da
árvore que contém \vu, até \vu, e \(Q\) é o caminho em \F\ de~\vv\ 
até~\(r_v\), raiz da árvore que contém \vv. Fazemos \(M\deq M\sdiff R\) e
repetimos o processo da construção de \F\ (do início!).

\emph{Caso 3.}\quad Existe uma aresta~\(uv\)
com~\vv\ rotulado~\vpar\ e~\vv\ pertencente à mesma árvore à qual
pertence \vu. Neste caso, temos uma uma \M-flor em~\G, com
um~\M-botão, digamos~\B\ (circuito ímpar que existe em
\(F+uv\)). Rotulamos de~\vpar\ os vértices do botão~\B,
contraímos~\B\ e consideramos o grafo~\(G/B\) com o emparelhamento
\(M/B\) nesse grafo. Continuamos o processo de expansão da floresta
\F\ resultante. (Usamos então o teorema provado anteriormente.)
Recursivamente, continuamos\ldots

\begin{fato}
  Se nenhum dos três casos ocorre, então afirmamos que encontramos um
  emparelhamento máximo~\(M'\) no grafo corrente \(G'=(V',E')\) que
  foi obtido do grafo original após zero ou mais contrações.
\end{fato}

\begin{prova}
  Considere a rotulação \vpar/\vimpar\ feita conforme a
  \hbox{\(M'\)-floresta} foi construída. Seja \(X'\deq\{\mbox{\(x\in
    V'\):\ \(x\) não é coberto por \(M'\)}\}\), e sejam
\begin{align*}
  \vpar &\digual\{v\in V':\text{ rótulo de \vv\ é \vpar}\},\\ \vimpar
  &\digual\{v\in V':\text{ rótulo de \vv\ é \vimpar}\}.
\end{align*}

Note que não há flores em~\glinha\negthinspace, e
portanto~\(|X'|=|\vpar| - |\vimpar|\). Para para todo
subconjunto~\(S\subseteq V'\) vale \(\deficiencia(G')\geq c_o (G -S)-
|S|\) e em particular, tomando \(S=\vimpar\), temos que
\[
\deficiencia(G')\geq c_o (G -\vimpar)-|\vimpar| =|\vpar|-|\vimpar|
=|X'|.
\] 
Como \(M'\) \emph{não} cobre exatamente \(|X|\) vértices, segue que
\(M'\) é máximo.
\end{prova}

\begin{prova}[(Prova alternativa.)] Sabemos, pela Fórmula de Tutte--Berge, 
\[
\emp(G')\le \frac{1}{2}\bigl(|V'|+|S|-c_o (G'-S)\bigr)
\]
para todo \(S\subset V'\). Tomando \(S=\vimpar\), temos
\[
\emp(G')\le \frac{1}{2}(|V'|+|\vimpar|-|\vpar|).
\]

O algoritmo descrito constrói uma floresta, composta pelo conjunto de
vértices rotulados. Como na outra demonstração, considere a rotulação
\vpar/\vimpar\ feita conforme a \(M'\)-floresta foi construída. Seja
\(X'\deq\{\mbox{\(x\in V'\):\ \(x\) não é coberto por \(M'\)}\}\), e
sejam
\begin{align*}
  \vpar &\digual\{v\in V':\text{ rótulo de \vv\ é \vpar}\},\\ \vimpar
  &\digual\{v\in V':\text{ rótulo de \vv\ é \vimpar}\}.
\end{align*}
Pela rotulação feita pelo algoritmo, o número de arestas do
emparelhamento~\(M'\) que estão fora da floresta
é~\((|V'|-(|\vimpar|+|\vpar|))/2 \), e o das que estão na floresta
é~\(|\vimpar|\). De fato, o conjunto de vértices \(X'\subseteq V'\)
expostos por \(M'\) está na floresta, e assim os vértices não
rotulados (isto é, fora da floresta) devem estar cobertos pelo
emparelhamento~\(M'\).

A quantidade de vértices fora da floresta
é~\(|V'|-(|\vimpar|+|\vpar|)\), e o número de arestas do
emparelhamento \(M'\) na floresta é igual ao número de vértices
rotulados \(\vimpar\), pois cada \(\vimpar\) é ponta de exatamente uma
aresta de \(M'\). Portanto
\[
\begin{aligned}
|M'|&\igual
\frac{1}{2}\bigl(|V'|-(|\vimpar|+|\vpar|)\bigr)+|\vimpar| \\
&\igual
\frac{1}{2}(|V'|+|\vimpar|-|\vpar|).
\end{aligned}
\]
Logo, o emparelhamento \(M'\) é máximo.
\end{prova}

Já vimos que o emparelhamento máximo em~\glinha\ corresponde a um
emparelhamento máximo no grafo~\G\ inicial, obtido segundo o
Teorema~\ref{teo:2.11} (os botões são descontraídos na ordem inversa
de sua contração).

Observamos agora como identificar a decomposição de
\Edmonds-\Gallai\ do Teorema~\ref{teo:decomposicao-edmonds-gallai}
partindo da rotulação de~\G\ fornecida pelo algoritmo. Lembramos
que~\(D_G\) é o conjunto dos vértices que são descobertos por algum
emparelhamento máximo, e~\(A_G\) é o conjunto de vizinhos de~\(D_G\)
em~\(V\setminus D_G\). Note que~\vpar\ é o conjunto dos
vértices~\vv\ tais que existe em~\G\ um caminho~\M-alternante de
comprimento par de~\X\ até~\vv. Analogamente, \vimpar\ é o conjunto de
vértices~\(v\in V(G)\setminus\vpar\) alcançáveis por um
caminho~\M-alternante de comprimento ímpar.

\begin{proposicao}
Seja~\M\ um emparelhamento máximo em~\gve, \X\ o conjunto dos vértices
não cobertos por~\M\ e sejam~\vpar\ e~\vimpar\ como acima.
Então~\(\vpar = D_G\) e~\(\vimpar=A_G\).
\end{proposicao}

Consideramos que um caminho de comprimento zero é um
caminho \M-alternante. Ou seja, \vpar\ contém o conjunto~\X.

\begin{prova}
Demonstraremos a igualdade~\(\vpar=D_G\); o fato~\(\vimpar=A_G\) é um
exercício.

\emph{(Prova de \(\vpar \subseteq D_G\).)}\quad Seja~\vv\ um vértice
em~\vpar, e seja~\P\ um caminho \M-alternante
de~\X\ para~\vv. Considere~\(M'\deq M \sdiff P\). Então~\(M'\) é um emparelhamento máximo que não
cobre~\vv. Portanto, \vv\ pertence a~\(D_G\).

\emph{(Prova de \(D_G \subseteq \vpar\).)}\quad Seja~\vv\ um vértice
em~\(D_G\). Se~\(v\) pertence a~\X\ então~\vv\ pertence
a~\vpar. Suponha que~\vv\ é coberto por~\M. Seja~\(M'\) um
emparelhamento máximo que não cobre~\vv\ (\(M'\) existe
pois~\vv\ pertence a~\(D_G\)). Os componentes de~\(M' \sdiff M\) são
circuitos ou caminhos pares com arestas alternadamente em~\(M'\) e
em~\M. Como~\vv\ é coberto por~\M, existe um caminho alternante par
que começa em~\vv, com uma aresta de~\M, e termina num vértice~\vw,
chegando por uma aresta de~\(M'\). Como~\vw\ não é coberto por~\M,
então~\vw\ pertence a~\X. Neste caso, temos um caminho \M-alternante
par que começa em~\X e termina em~\vv. Logo,~\vv\ pertence a~\vpar.
\end{prova}

Um corolário que segue imediatamente do Algoritmo
de~\Edmonds--\Gallai\ é o seguinte.

\begin{corolario}
Se~\(M\) é um emparelhamento máximo em um grafo~\(G\), então para todo
vértice~\(w\) em~\(D_G\), existe um vértice~\(v\) não-coberto por~\(M\)
e um caminho~\(M\)-alternante par de~\(v\) a~\(w\).
\end{corolario}

%% Local Variables:
%% mode: latex
%% coding: utf-8
%% fill-column: 70
%% tab-width: 2
%% End:
