#! /bin/bash
FILE=$1
SUB=`cat $2`
sed '/<!-- log lines begin -->/q' "$FILE" > "$FILE.before"
sed -n '/<!-- log lines end -->/,$ p' "$FILE" > "$FILE.after"
#awk '1,/<!-- log lines begin -->/p' "$FILE" > "$FILE.before"
#awk '/<!-- log lines end -->/,0p' "$FILE" > "$FILE.after"
cat "$FILE.before" $2 "$FILE.after" > "$FILE.updated"
# awk -v sb="$SUB" '/<!-- log lines begin-->/,/<!-- log lines end -->/ { if ( $0 ~ /<!-- log lines begin -->/ ) print "<!-- log lines begin-->" ;if ( $0 ~ /<!-- log lines end -->/ ) { print sb; print "<!-- log lines end -->" }; next } 1' "$FILE" > "$FILE.updated"


