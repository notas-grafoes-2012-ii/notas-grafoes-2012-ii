Versão        Descrição
-----------------------------------------------------------------------------------
0.5           todos capítulos revisados por uma pessoa
0.5.X         capítulos revisados por uma pessoa + X capítulos revisados por alguma
              dupla

0.6           todos capítulos revisados por uma dupla (= 0.5.9)
0.6.1         criado arquivo de formato de teoremas, demonstrações, citações, etc.
0.6.1.X       X capítulos revisados para o padrão de formatos
0.6.2         todos capítulos revisados para o padrão de formatos (= 0.6.1.9)
0.6.3         criados arquivos de termos permitidos e termos proibidos
0.6.3.X       X capítulos revisados para o padrão de termos
0.6.4         todos capítulos revisados para o padrão de termos (= 0.6.3.9)
0.6.5         criado arquivo de padrão de símbolos, imagens e legendas
0.6.5.X       X capítulos revisados para o padrão de símbolos, imagens e legendas
0.6.6         todos capítulos revisados para o padrão de símbolos, imagens e
              legendas (= 0.6.5.9)
0.6.6.X       X capítulos com todas as referências presentes e corretas
0.6.7         todos capítulos com todas as referências presentes e
              corretas (= 0.6.6.9)
0.6.8         criado arquivo de padrão de entradas do .bib
0.6.9         revisado .bib

0.7           uniformização concluída (= 0.6.9)
0.7.1         discutidos possíveis apêndices
0.7.1.X       X apêndices foram feitos
0.7.2         todos apêndices foram feitos
0.7.2.X       X exercícios extras foram adicionados
0.7.3         declarado fim da temporada de exercícios extras
0.7.3.X       X exercícios extras foram revisados
0.7.4         todos exercícios extras foram revisados
0.7.5         pedido de "olhada" por professor feito
(0.7.6)       pedido de "olhada" por segundo professor feito
(0.7.7)       pedido de "olhada" por terceiro professor feito

0.8           algum professor terminou de olhar
(0.8.1)       dois professores terminaram de olhar
(0.8.2)       três professores terminaram de olhar
0.8.5         criado/recebido arquivo de sugestões de professores
0.8.5.X       X capítulos revisados de acordo com sugestões de professores

0.9           revisão para *gulp* publicação concluída
0.9.1         ideia de publicação apresentada
0.9.2         *WTF* publicado
0.9.3         descobertos/recebidos primeiros erros
0.9.5         definido plano de errata
0.9.5.X       errata referente a X capítulos concluída

1.0           primeira errata concluída

