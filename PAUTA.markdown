# 4 março
- como escrever: mapa que leva vértices de G a vértices de H
(coloração, prova da operação de Hajós). Opções? Ex:
  - leva vértices de G a vértices em H
  - mapa entre vértices de G e em H

# 21 janeiro
- sugestão: deixar a definição de `\deq` diferente de `=`, , para a gente
  ter certeza de onde estamos usando \deq e onde estamos usando =. Eu
  sugiro definir `\deq` como `\ensuremath{\doteq}`

# 3 dezembro
- Tá chegando mesmo!
- exercícios sobre número de arestas num subgrafo bipartido
- tâmo no google! (dupla mesóclise, entre aspas)

# 25 junho
- natal tá chegando gente!
- definir na introdução:
  - número de independência, logo após grafo induzido:
  
  Um conjunto~\(A\subseteq V\) de vértices é dito independente se seu grafo
  induzido~\(G[A]\) não contém arestas. O tamanho~\(\alpha(G)\) do maior
  conjunto independente de~\G\ é o~\defi{número de independência} de~\G,

  - clique, logo após falar de subgrafo

  Uma clique é um subgrafo completo. O número de vértices na maior clique
  de~\G\ é denotado por~\(\omega(G)\).
  
# 18 junho

Breve resumo e desdobramentos da reunião ontem.

- Discutimos alguns dos TODOs do fb 

  Uma dificuldade com os todos é quando eles não são "autocontidos".  Fica
  difícil só vendo o arquivo e a linha com o todo de saber a que se referem
  coisas como "%TODO pra que serve isso?"

- Sugestão do tn (depois da reunão): 

  Que tal uma rodada de revisões assim que terminarmos estes capítulos?
  Assim aproveitamos que os assuntos estão frescos na cabeça e já
  aprendemos um pouco como é que é revisar as coisas.  
  
  
- lc disse que C-componente aparece em alguma demonstração

Tarefas postas em TODO
- definir hipergrafos, seção de caminho, isomorfismo
- acrescentar um blá de approx meia página sobre complexidade
- fb, está ok falar de planaridade antes de coloração?
- lc escreveu duas provas em conexidade que acha que precisam ser lidas,
  devem estar difíceis de entender: 
  - implicação a => d da caracterização de grafo 2-aresta-conexo, e 
  - teorema de Lovász sobre emparelhamentos 
  
Não entendemos alguns TODOs (mesmo com contexto
- o que é o todo sobre rótulos de subfiguras?


- resultados que pode ser interessante acrescentar
  - grafo com nro cromático não enumerável tem cintura limitada
  - algum resultado sobre grafos infinitos
  - toda classe de grafos fechada por <operação x> é forbiddent de alguém

- todos lerem demonstração dos teoremas
  - a => d da caracterização de grafo 2-aresta-conexo
  - Lovász sobre emparelhamentos

- exercícios de planaridade relacionados a coloração:

    \begin{exercicio}
    Dizemos que um mapa (cartográfico) é \defi{padrão} se exatamente~\(3\)
    linhas de fronteira encontram-se em algum ponto.  Isto é, se o grafo 
    formado pelas fronteiras é~\(3\)-regular. Prove que todo mapa padrão
    possui uma face (país) com até~\(5\) arestas.
    \begin{exercicio}

    \begin{exercicio}\label{exc:planar_padrao} 
    Mostre que podemos colorir as faces de um mapa padrão com até~\(6\)
    cores.\hint{Note que todo mapa padrão é \(2\)-conexo.}
    \end{exercicio}

    \begin{exercicio}    
    \begin{exercicio}\label{exc:6_cores}
    Prove que todo grafo planar pode ser colorido com até 6 cores.
    \end{exercicio}

- colocar discussão sobre complexidade na introdução?

# 11 junho
- colocar discussão sobre complexidade na introdução
- Notação N_G(v) para indicar o grafo onde a vizinhança ocorre

#  4 junho
- vamos pedir fomento do IME?

  Modelo de carta:
  
  São Paulo, 04 de mar:co de 2013
  
  À comissão coordenadora de programa do PPG em matemática,
  
  Eu, \emph{Arnaldo Bernardo}, número USP XXXXXX, aluno regular do curso de
  \emph{Doutorado} do Programa de Pós-Graduação em Matemática do IME/USP,
  sob orientação do \emmph{Prof.\ Dr.\ Carlos Dimitri}, gostaria de pedir
  auxílio financeiro para desenvolvimento de uma apostila sobre Teoria dos
  Grafos, baseado em curso de pós-graduação, que foi realizado em 2012. O
  material abrange tópicos como.
  
  Emparelhamentos, Conectividade,...
  
  Já existe um documento preliminar (com 70 páginas) cuja revisão foi
  iniciada em fins de 2012. A previsão é de que a confecção do material
  leve 6 meses, e que o produto do trabalho seja publicado pela Sociedade
  Brasileira de Computação.

  Gasto previsto
  R$: 10 exemplares da apostila, 3 destinados à biblioteca do IME.

  Atenciosamente
  
  Assintaturas do Orientador e do Aluno  
- eo: Ramsey

# 28 maio
- vamos experimentar trabalhar em paralelo em capítulos diferentes:
  - tn: terminar emparelhamentos
  - lc: conectividade
  - fb: planaridade

# 21 maio
- discussão sobre a rotulação

# 14 maio
- fix em trilha
- gente nova!

#  7 maio
- 
# 29 abril
- Discutir as definições na introdução
- Continuar a leitura conjunta do cap de emparelhamentos

- Blah antes da definição de defeito m grafos arbitrários:

O teorema~\ref{teo:Ore_deficiencia} conclui que a deficiência de um
grafo bipartido~\G\ é o número de vértices descobertos por um
emparelhamento máximo, isto é, \(\deficiencia G=|V|-2\emp (G)\). Esta
última igualdade motiva uma definição de defeito para grafos
arbitrários.

Fica:

O teorema~\ref{teo:Ore_deficiencia} conclui que a deficiência de um
grafo bipartido~\G\ é o número de vértices descobertos por um
emparelhamento máximo, isto é, % \(\deficiencia_A (G)=|V|-2\emp
(G)=\deficiencia_B (G)\).  % Ou seja, a deficiência independe da
partição considerada. Isto motiva uma definição de defeito para grafos
arbitrários.

# 23 de abril
- continuamos a revisão final, adicionamos referências e 
  encontramos alguns conceitos que precisavam ser definidos.
  
## Decisões
- que notação usar para a distância entre dois vértices? Ideias: d,
  dist. Deixamos \(\mathrm{d}\), deixando claro no contexto.
- vamos usar um símbolo especial, digamos ":=", para designar
  definição? Não.
- notação Emp(G), Cob(G), Adj(V) usando sempre parênteses.

## História do estudo de emparelhamentos

(tn) minhas notas dizem que a teoria de emparelhamentos vêm da década
de 30 (Wikipedia diz que o min-max do König é de 31). Alguém confirma?
Também, por volta dos anos 60, Edmonds trouxe uma abordagem poliédrica
para o problema

- o lema para encolher os botões é de Edmonds, 1965? (Paths, Trees and
  Flowers, Canadian J. Math, 17, 1965)
- história dos algoritmos para matching (estava em folhinha que a profa deu):
  confiável? interessante por?
  - 57 - Berge - augmenting path theorem
  - 65 - n^4 - Edmonds - blossom shrinking
  - 65 - mn^2 - Witzgall and Zahn - modified Edmond's algorithm
  - 67 - n^3 - Balinski - 
  - 74 - mn - Kameda and Munro - better blossom handling
  - 76 - n^3 - Gabow and Lawler - better blossom handling -  no explicit shrinking/expansion
  - 80 - n^3 - Pape and Conradt - simple blossom handling -  Fortran code
  - 80 - Micalli and Vazirani - theoretically fastest but not practical
  - 83 - mn - Gabow and Tarjan

# 16 de abril
- Presentes: Eric, Josefran, Fábio, Tássio
- continuamos as revisões do cap. 2 (emparelhamentos)
- acrescentamos algumas referências, uniformizamos nomes de conceitos
- mudamos a ordem de algumas definições e os nomes de algumas seções
- discutimos um pouco a formatação de teoremas, exercícios

# 9 de abril
- Presentes: Eric, Fábio, Tássio
- comentários sobre o bla do cap. de emparelhamentos
- texto que deve ir para notas ao fim do cap, referências bib.

# 2 de abril
- Presentes: Eric e Tássio.
- Eric apresentou a prova da equivalência entre a rotulação e os
conjuntos da decomposição de Edmonds--Gallai. Com isso essencialmente
concluímos o material do cap. 2.
- Iniciamos a revisão do cap. 3, olhamos por cima o que tem que ser 
feito. Há bastante material que, ao que parece, não está nas notas 
de aula (provavelmente está nas folhinhas). 
- ficamos de verificar de fato o que falta; comparando com nossas 
anotações e com o material recebido



