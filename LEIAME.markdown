Teoria dos Grafos - Notas de aula
=================================

As notas são compiladas com `make notas-grafoes-print.pdf`.

O projeto está atualmente hospedado no [Gitlab][1].

[1]: https://gitlab.com/notas-grafoes-2012-ii/notas-grafoes-2012-ii


Pacotes necessários
-------------------

Alguns pacotes que você precisa para compilar o documento são

- nag;
- cite;
- xifthen;
- subfig;
- caption; e
- tikz.

Sobre os scripts na pasta do projeto
------------------------------------

Usamos o pacote chapterbib para gerar bibliografias a cada capítulo, o
que requer que bibtex seja rodado para cada capítulo. Fazemos isso
usando o script `call-bibtex.sh`.

Os scripts `all-labels.sh` e `labels-in-file.sh` são para identificar
`\labels' definidos múltiplas vezes, o que acabou acontecendo (somos
vários escribas...)

Por fim, o script `insert.sh` serve para atualizar uma página com
informações do projeto.

Erros
-----

Os escribas ficam muito gratos se você puder nos comunicar os erros
que encontrar. O material é longo, e certamente contém erros. Podemos
ser contactados pelo email `um@dois.tres.quatro` (trocando os números
por "tnaia", "member", "fsf" e "org", respectivamente, mas mantendo os
pontos).
