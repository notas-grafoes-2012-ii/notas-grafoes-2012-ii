# Anotações para o uso do LaTeX

** Sumário **
- Nomes de Matemáticos
- Corpo do texto
- Matemática
- Organização dos arquivos

## NUNCA
- label antes de caption

## Nomes de Matemáticos

Teremos um índice remissivo, com as ocorrências dos nomes pessoas. Para
isso, criamos macros para os nomes de pessoas que sejam usados no
texto. Esses nomes estão no arquivo `ferramentas-para-o-texto.tex`. São
dois comandos por nome: um para o sobrenome usado em citações, e outro com
o nome completo. Assim, Para _Martin Luther King_ teríamos os comandos
`\King` e `\MartinLutherKing`.

## Corpo do texto
## Matemática
- módulo entre parêntese: \pmod 
## Organização dos arquivos

Existem dois conjuntos de arquivos. Para cada arquivo `foo.tex`, quando
houver `foo-print.tex` significa que eles diferem em uns poucos detalhes. A
versão `foo.tex` está associada à versão digital do texto, formatado para
ser lido no computador. A versão `-print` é para impressão. Assim, quando
for alterar qualquer arquivo que tenha um par `-print`, certifique-se de
fazer as alterações espelhadas nos dois arquivos.

Alguns arquivos têm funções específicas:
- `notas-grafoes.tex` e `notas-grafoes-print.tex`

  São o arquivo "raiz" do projeto, incluem os demais.

- `head.tex` 

  Inclui pacotes, e faz a maior parte das definições de comandos está
  definida ali.
  
- `Makefile`

  Contém as instruções para construção do projeto. É um arquivo de
  configuração do programa `make`, que permite, por exemplo, usar os
  comandos `make notas-grafoes.pdf` para gerar as notas de aula.

- `insert.sh` é um script que gera um pedaço da tabela com os últimos
  commits do projeto.
  
- `call-bibtex.sh` script para geração das bibliografias dos vários
  capítulos.

- `labels-in-file.sh` script que recebe um arquivo e exibe todos 
  os comandos `\label` nele
  
- `all-labels.sh` script que mostra todos os comandos `\label`
  em qualquer arquivo `tex` na sub-árvore atual.
  
- `layout-grafoes.tex`, `layout-grafoes-print.tex` e
  `layout-grafoes-common.tex`

  Definem formatação do documento: dimensão das margens e do papel, cores,
  formatação dos capítulos e seções.

- `macros-matematica.tex` definição de macros para matemática

- `ferramentas-para-o-texto.tex`

- `capa.tex` capa do documento e informações sobre o documento

- `color-definitions.tex` define as cores para o documento todo

- `notas-de-fim-de-capitulo.tex` contém comandos que habilitam as notas ao
  fim de cada capítulo
