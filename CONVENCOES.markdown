- abolimos o ambiente observação
- usaremos termos em inglês e em português
- dizemos _mapa plano_ em vez de somente _mapa_
- componentes ímpares de um grafo \G\ é \(c_o(G)\)
- Teorema de Tutte, e similares: a palavra _teorema_ fica em minusculo
- que notação usar para a distância entre dois vértices? Ideias: d,
  dist. Deixamos \(\mathrm{d}\), deixando claro no contexto.
- vamos usar um símbolo especial, digamos ":=", para designar
  definição? Não.
- notação Emp(G), Cob(G), Adj(V) usando sempre parênteses.
- óbvio   : é óbvia
  trivial : 1 passo de lógica, em metros
  note que: basta rever a definição para entender
  é fácil ver que: quando é preciso muito mais palavras para descrever
   a situação do que para concluir

GRAFIA (novas excessivas exceções da língua portuguesa)
- hipoemparelhável é sem hífen mesmo, porque hipo termina com uma vogal
diferente da vogal inicial de emparelhável. e.g. hipopótamo wtf

LAYOUT

- entre a ida e volta de um teorema, colocar um \medskip

CENSURA

- ademais, tampouco, tão pouco, 
- paulatinamente é um termo muito culto que não fomos capazes de
  compreender e, portanto, abster-nos-emos de usar.
- Não empregaremos dupla mesóclise.
  Exemplo 1: Suceder-se-lhe-iam outras.
  Exemplo 2: hi-po-pó-tamo...

SUBSTITUTOS "RAZOÁVEIS" (respectivamente)

- além disso, 


COMO FAZER UMA DEFINIÇÃO 

- Um conjunto de arestas M de um grafo G é chamado de \defi{emparelhamento}
se para cada vértice v ∈ V (G) existe no máximo uma aresta de M incidente a
v. 

CONJUGAÇÕES DE VERBOS EM DEMONSTRAÇÕES

- Primeiramente, demonstramos que Emp(G) ≤ 2 minS⊆V (G) |V |+ |S| − co (G − S) . 
Observe que se existe S ⊆ V tal que co (G − S) > |S|, então
pelo menos co (G − S) − |S| vértices devem ficar desemparelhados em qualquer
emparelhamento. Assim, temos no máximo 1
2 |V | − co (G − S) + |S| arestas em
um emparelhamento, e segue que Emp(G) ≤ 1
2 minS⊆V |V | + |S| − co (G − S) .
13 em 89
Versão: 6 de agosto de 2013, às 16 04A demonstração de Emp(G) ≥ 1
2 minS⊆V (G) |V | + |S| − co (G − S) segue
por indução no número de vértices |V (G)|. Se |V (G)| = 1 a fórmula é óbvia, e
ambos os lados resultam 0. Suponha então que |V (G)| > 1 e que o teorema
vale para todo grafo G com |V (G )| < |V (G)|. Consideramos dois casos.

SUBTRAÇÕES

Usamos "-" como operação entre números ou quando um dos objetos é um grafo.
Isto é, no caso em que os objetos envolvidos na subtração são ambos conjuntos usamos "\setminus".
Exemplos: "G-v", "G-E(H)", "E(G) \setminus E(H)", "H_i - P_ó \setminus T_a - m_o", etc...


CORES

Abolimos o uso de cores cujo masculino e feminino se escrevem diferentemente
e.g. vermelha, amarela, hipopótama.

CASOS

Abolimos o uso de lista de casos, exceto no caso 3 de hipopótamos.
Isto é, vire-se para usar "suponha" ou "se [...] então".

TODO
decidir sobre vírgulas em i.e. e e.g.