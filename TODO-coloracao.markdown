- reler, verificando organização do capítulo
- verificar as demonstrações

# Demonstrações a adicionar
- L-coloração de disco triangulado
- Thomassen: G planar tem `chi_L ≤ 5`
- Galvin:  G bipartido tem  `chi_L' = chi_L`

# Pilha
- verificar se operações entre grafos estão definidas:
 - H + xy -xz
 - H - H'
 - H \cap H'
 - x \in G
 - isomorfismo entre grafos
- Teorema de Grötzsch ?
- referência da versão assintótica da conjectura `chi_L' = chi_L`
- Lovász: grafos perfeitos têm complemento perfeito
