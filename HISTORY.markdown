# História da revisão

Abaixo estão anotados alguns pontos marcantes da
revisão destas notas.


2023-02-15  criada tag b4-notes-yoshiko, que marca uma tentativa 
            (de Gabriel Morete e Tássio Naia para incorporar os
            comentários da Yoshiko nas notas).
