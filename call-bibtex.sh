#!/bin/bash
QUIET=false
BOPT=
while getopts ":qb:" opt; do
	case $opt in
		q)
			QUIET=true
			;;
		b)
			BOPT=$OPTARG
			;;
		:)
			#b without parameter (silently ignored)
			;;
		\?)
			echo "Invalid option: -$OPTARG" >&2
			exit 1
			;;
	esac
done

for auxfile in [0-9][0-9]*.aux
do
	if ! $QUIET; then
		echo `basename $auxfile .aux` >&2
	fi
    bibtex $BOPT `basename $auxfile .aux`
done
